﻿namespace LoggerSoftware
{
    partial class GaugeGraph_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.set_button = new System.Windows.Forms.Button();
            this.maxvalue_label = new System.Windows.Forms.Label();
            this.minvalue_label = new System.Windows.Forms.Label();
            this.maxvalue_textbox = new System.Windows.Forms.TextBox();
            this.minvalue_textbox = new System.Windows.Forms.TextBox();
            this.currentreading_label = new System.Windows.Forms.Label();
            this.readingLabel = new System.Windows.Forms.Label();
            this.gaugeGraph = new AGaugeApp.AGauge();
            this.SuspendLayout();
            // 
            // set_button
            // 
            this.set_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.set_button.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.set_button.Location = new System.Drawing.Point(269, 179);
            this.set_button.Name = "set_button";
            this.set_button.Size = new System.Drawing.Size(112, 30);
            this.set_button.TabIndex = 12;
            this.set_button.Text = "Set";
            this.set_button.UseVisualStyleBackColor = true;
            this.set_button.Click += new System.EventHandler(this.Setbutton_Click);
            // 
            // maxvalue_label
            // 
            this.maxvalue_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.maxvalue_label.AutoSize = true;
            this.maxvalue_label.BackColor = System.Drawing.Color.White;
            this.maxvalue_label.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxvalue_label.Location = new System.Drawing.Point(291, 131);
            this.maxvalue_label.Name = "maxvalue_label";
            this.maxvalue_label.Size = new System.Drawing.Size(73, 17);
            this.maxvalue_label.TabIndex = 11;
            this.maxvalue_label.Text = "Max Value";
            // 
            // minvalue_label
            // 
            this.minvalue_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.minvalue_label.AutoSize = true;
            this.minvalue_label.BackColor = System.Drawing.Color.White;
            this.minvalue_label.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minvalue_label.Location = new System.Drawing.Point(291, 83);
            this.minvalue_label.Name = "minvalue_label";
            this.minvalue_label.Size = new System.Drawing.Size(69, 17);
            this.minvalue_label.TabIndex = 10;
            this.minvalue_label.Text = "Min Value";
            // 
            // maxvalue_textbox
            // 
            this.maxvalue_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.maxvalue_textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxvalue_textbox.Location = new System.Drawing.Point(269, 151);
            this.maxvalue_textbox.Name = "maxvalue_textbox";
            this.maxvalue_textbox.Size = new System.Drawing.Size(112, 22);
            this.maxvalue_textbox.TabIndex = 9;
            // 
            // minvalue_textbox
            // 
            this.minvalue_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.minvalue_textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minvalue_textbox.Location = new System.Drawing.Point(269, 103);
            this.minvalue_textbox.Name = "minvalue_textbox";
            this.minvalue_textbox.Size = new System.Drawing.Size(112, 22);
            this.minvalue_textbox.TabIndex = 8;
            // 
            // currentreading_label
            // 
            this.currentreading_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.currentreading_label.AutoSize = true;
            this.currentreading_label.BackColor = System.Drawing.Color.White;
            this.currentreading_label.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentreading_label.Location = new System.Drawing.Point(266, 9);
            this.currentreading_label.Name = "currentreading_label";
            this.currentreading_label.Size = new System.Drawing.Size(115, 17);
            this.currentreading_label.TabIndex = 13;
            this.currentreading_label.Text = "Current Reading";
            // 
            // readingLabel
            // 
            this.readingLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.readingLabel.AutoSize = true;
            this.readingLabel.BackColor = System.Drawing.Color.White;
            this.readingLabel.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.readingLabel.ForeColor = System.Drawing.Color.Blue;
            this.readingLabel.Location = new System.Drawing.Point(269, 37);
            this.readingLabel.Name = "readingLabel";
            this.readingLabel.Size = new System.Drawing.Size(15, 22);
            this.readingLabel.TabIndex = 14;
            this.readingLabel.Text = " ";
            // 
            // gaugeGraph
            // 
            this.gaugeGraph.BaseArcColor = System.Drawing.Color.Gray;
            this.gaugeGraph.BaseArcRadius = 80;
            this.gaugeGraph.BaseArcStart = 120;
            this.gaugeGraph.BaseArcSweep = 300;
            this.gaugeGraph.BaseArcWidth = 2;
            this.gaugeGraph.Cap_Idx = ((byte)(1));
            this.gaugeGraph.CapColors = new System.Drawing.Color[] {
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black};
            this.gaugeGraph.CapPosition = new System.Drawing.Point(10, 10);
            this.gaugeGraph.CapsPosition = new System.Drawing.Point[] {
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10)};
            this.gaugeGraph.CapsText = new string[] {
        "",
        "",
        "",
        "",
        ""};
            this.gaugeGraph.CapText = "";
            this.gaugeGraph.Center = new System.Drawing.Point(100, 100);
            this.gaugeGraph.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gaugeGraph.Location = new System.Drawing.Point(12, 12);
            this.gaugeGraph.MaxValue = 5F;
            this.gaugeGraph.MinValue = 0F;
            this.gaugeGraph.Name = "gaugeGraph";
            this.gaugeGraph.NeedleColor1 = AGaugeApp.AGauge.NeedleColorEnum.Gray;
            this.gaugeGraph.NeedleColor2 = System.Drawing.Color.DimGray;
            this.gaugeGraph.NeedleRadius = 80;
            this.gaugeGraph.NeedleType = 0;
            this.gaugeGraph.NeedleWidth = 2;
            this.gaugeGraph.Range_Idx = ((byte)(0));
            this.gaugeGraph.RangeColor = System.Drawing.Color.LightGreen;
            this.gaugeGraph.RangeEnabled = false;
            this.gaugeGraph.RangeEndValue = 5F;
            this.gaugeGraph.RangeInnerRadius = 70;
            this.gaugeGraph.RangeOuterRadius = 80;
            this.gaugeGraph.RangesColor = new System.Drawing.Color[] {
        System.Drawing.Color.LightGreen,
        System.Drawing.Color.Red,
        System.Drawing.SystemColors.Control,
        System.Drawing.SystemColors.Control,
        System.Drawing.SystemColors.Control};
            this.gaugeGraph.RangesEnabled = new bool[] {
        false,
        false,
        false,
        false,
        false};
            this.gaugeGraph.RangesEndValue = new float[] {
        5F,
        5F,
        0F,
        0F,
        0F};
            this.gaugeGraph.RangesInnerRadius = new int[] {
        70,
        70,
        70,
        70,
        70};
            this.gaugeGraph.RangesOuterRadius = new int[] {
        80,
        80,
        80,
        80,
        80};
            this.gaugeGraph.RangesStartValue = new float[] {
        0F,
        0F,
        0F,
        0F,
        0F};
            this.gaugeGraph.RangeStartValue = 0F;
            this.gaugeGraph.ScaleLinesInterColor = System.Drawing.Color.Black;
            this.gaugeGraph.ScaleLinesInterInnerRadius = 73;
            this.gaugeGraph.ScaleLinesInterOuterRadius = 80;
            this.gaugeGraph.ScaleLinesInterWidth = 1;
            this.gaugeGraph.ScaleLinesMajorColor = System.Drawing.Color.Black;
            this.gaugeGraph.ScaleLinesMajorInnerRadius = 70;
            this.gaugeGraph.ScaleLinesMajorOuterRadius = 80;
            this.gaugeGraph.ScaleLinesMajorStepValue = 0.5F;
            this.gaugeGraph.ScaleLinesMajorWidth = 2;
            this.gaugeGraph.ScaleLinesMinorColor = System.Drawing.Color.Gray;
            this.gaugeGraph.ScaleLinesMinorInnerRadius = 75;
            this.gaugeGraph.ScaleLinesMinorNumOf = 9;
            this.gaugeGraph.ScaleLinesMinorOuterRadius = 80;
            this.gaugeGraph.ScaleLinesMinorWidth = 1;
            this.gaugeGraph.ScaleNumbersColor = System.Drawing.Color.Black;
            this.gaugeGraph.ScaleNumbersFormat = null;
            this.gaugeGraph.ScaleNumbersRadius = 95;
            this.gaugeGraph.ScaleNumbersRotation = 0;
            this.gaugeGraph.ScaleNumbersStartScaleLine = 0;
            this.gaugeGraph.ScaleNumbersStepScaleLines = 1;
            this.gaugeGraph.Size = new System.Drawing.Size(209, 196);
            this.gaugeGraph.TabIndex = 0;
            this.gaugeGraph.Text = "aGauge1";
            this.gaugeGraph.Value = 0F;
            // 
            // GaugeGraph_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(393, 214);
            this.Controls.Add(this.readingLabel);
            this.Controls.Add(this.currentreading_label);
            this.Controls.Add(this.set_button);
            this.Controls.Add(this.maxvalue_label);
            this.Controls.Add(this.minvalue_label);
            this.Controls.Add(this.maxvalue_textbox);
            this.Controls.Add(this.minvalue_textbox);
            this.Controls.Add(this.gaugeGraph);
            this.Name = "GaugeGraph_form";
            this.Text = "GaugeGraph_form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private AGaugeApp.AGauge gaugeGraph;
        private System.Windows.Forms.Button set_button;
        private System.Windows.Forms.Label maxvalue_label;
        private System.Windows.Forms.Label minvalue_label;
        private System.Windows.Forms.TextBox maxvalue_textbox;
        private System.Windows.Forms.TextBox minvalue_textbox;
        private System.Windows.Forms.Label currentreading_label;
        private System.Windows.Forms.Label readingLabel;
    }
}