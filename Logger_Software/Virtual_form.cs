﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO.Ports;

namespace LoggerSoftware
{
    public partial class Virtual_form : Form
    {
        public Virtual_form()
        {
            InitializeComponent();

            foreach (String portName in SerialPort.GetPortNames())
            {
                comDropdown.Items.Remove(portName);
            }
            comDropdown.SelectedIndex = 0;
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            Software.mainForm.virtualProcess = Process.Start(Environment.CurrentDirectory + "/COM2TCP/COM2TCP.exe");

            Thread.Sleep(500);

            for (int i = 0; i < 15; i++)
            {
                SendKeys.Send("{DEL}");
            }

            foreach (char word in addressTextbox.Text)
            {
                SendKeys.Send("{" + word + "}");
            }

            SendKeys.Send("{TAB}");

            for (int i = 0; i < 10; i++)
            {
                SendKeys.Send("{DEL}");
            }

            foreach (char word in tcpPortTextbox.Text)
            {
                SendKeys.Send("{" + word + "}");
            }


            SendKeys.Send("{TAB}");

            for (int i = 0; i < 5; i++)
            {
                SendKeys.Send("{DEL}");
            }

            foreach (char word in comDropdown.SelectedItem.ToString())
            {
                SendKeys.Send("{" + word + "}");
            }

            SendKeys.Send("{TAB}");
            SendKeys.Send("{TAB}");
            SendKeys.Send("{ENTER}");

            Thread.Sleep(5000);

            Cursor.Current = Cursors.Default;

            this.Close();
        }
    }
}
