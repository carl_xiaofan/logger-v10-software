﻿namespace LoggerSoftware
{
    partial class Main_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private System.Windows.Forms.MenuStrip Main_Menu;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectionToolStripMenuItem;
        private System.Windows.Forms.TabControl mainTab;
        private System.Windows.Forms.TabPage infoTab;
        private System.Windows.Forms.TabPage settingTab;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label fixLabel3;
        private System.Windows.Forms.Label fixLabel4;
        private System.Windows.Forms.Label fixLabel2;
        private System.Windows.Forms.Label fixLabel6;
        private System.Windows.Forms.Label fixLabel5;
        public System.Windows.Forms.Label connectioLable;
        private System.Windows.Forms.Label fixLabel1;
        public System.Windows.Forms.TextBox builddateTextbox;
        public System.Windows.Forms.TextBox setdateTextbox;
        public System.Windows.Forms.TextBox checkTextbox;
        public System.Windows.Forms.TextBox firmwareTextbox;
        public System.Windows.Forms.TextBox serialTextbox;
        private System.Windows.Forms.TextBox logConsole;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button sensorButton;
        private System.Windows.Forms.Button systemButton;
        private System.Windows.Forms.Button clockButton;
        private System.Windows.Forms.Label fixLabel9;
        private System.Windows.Forms.Button recordButton;
        private System.Windows.Forms.Button roleButton;
        private System.Windows.Forms.TabPage dataTab;
        private System.Windows.Forms.Button downloaddataButton;
        private System.Windows.Forms.Label fixaLabel11;
        private System.Windows.Forms.Button realtimeButton;
        public System.Windows.Forms.MaskedTextBox lbDateTextbox;
        private System.Windows.Forms.CheckBox hideShowCheckbox;
        private System.Windows.Forms.ProgressBar mainProgressbar;
        private System.Windows.Forms.ToolStripMenuItem disconnectToolStripMenuItem;
        private System.Windows.Forms.Button viewButton;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem softwareLocationToolStripMenuItem;

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Main_Menu = new System.Windows.Forms.MenuStrip();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disconnectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.softwareLocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downloadDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.realTimeDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainTab = new System.Windows.Forms.TabControl();
            this.infoTab = new System.Windows.Forms.TabPage();
            this.builddateTextbox = new System.Windows.Forms.TextBox();
            this.setdateTextbox = new System.Windows.Forms.TextBox();
            this.checkTextbox = new System.Windows.Forms.TextBox();
            this.firmwareTextbox = new System.Windows.Forms.TextBox();
            this.serialTextbox = new System.Windows.Forms.TextBox();
            this.fixLabel6 = new System.Windows.Forms.Label();
            this.fixLabel5 = new System.Windows.Forms.Label();
            this.fixLabel4 = new System.Windows.Forms.Label();
            this.fixLabel3 = new System.Windows.Forms.Label();
            this.fixLabel2 = new System.Windows.Forms.Label();
            this.connectioLable = new System.Windows.Forms.Label();
            this.fixLabel1 = new System.Windows.Forms.Label();
            this.settingTab = new System.Windows.Forms.TabPage();
            this.radioButton = new System.Windows.Forms.Button();
            this.roleButton = new System.Windows.Forms.Button();
            this.fixLabel9 = new System.Windows.Forms.Label();
            this.recordButton = new System.Windows.Forms.Button();
            this.systemButton = new System.Windows.Forms.Button();
            this.clockButton = new System.Windows.Forms.Button();
            this.sensorButton = new System.Windows.Forms.Button();
            this.dataTab = new System.Windows.Forms.TabPage();
            this.viewButton = new System.Windows.Forms.Button();
            this.realtimeButton = new System.Windows.Forms.Button();
            this.downloaddataButton = new System.Windows.Forms.Button();
            this.fixaLabel11 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.logConsole = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.hideShowCheckbox = new System.Windows.Forms.CheckBox();
            this.mainProgressbar = new System.Windows.Forms.ProgressBar();
            this.masterLoggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.slaveLoggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uSBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rS232CableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rS232CableToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modemToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableCoreSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Main_Menu.SuspendLayout();
            this.mainTab.SuspendLayout();
            this.infoTab.SuspendLayout();
            this.settingTab.SuspendLayout();
            this.dataTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // Main_Menu
            // 
            this.Main_Menu.BackColor = System.Drawing.SystemColors.Control;
            this.Main_Menu.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Main_Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.settingToolStripMenuItem,
            this.openToolStripMenuItem});
            this.Main_Menu.Location = new System.Drawing.Point(0, 0);
            this.Main_Menu.Name = "Main_Menu";
            this.Main_Menu.Padding = new System.Windows.Forms.Padding(0);
            this.Main_Menu.Size = new System.Drawing.Size(806, 24);
            this.Main_Menu.TabIndex = 0;
            this.Main_Menu.Text = "menuStrip1";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectionToolStripMenuItem,
            this.disconnectToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(42, 24);
            this.newToolStripMenuItem.Text = "New";
            // 
            // connectionToolStripMenuItem
            // 
            this.connectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.masterLoggerToolStripMenuItem,
            this.slaveLoggerToolStripMenuItem});
            this.connectionToolStripMenuItem.Name = "connectionToolStripMenuItem";
            this.connectionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.connectionToolStripMenuItem.Text = "Connection";
            // 
            // disconnectToolStripMenuItem
            // 
            this.disconnectToolStripMenuItem.Name = "disconnectToolStripMenuItem";
            this.disconnectToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.disconnectToolStripMenuItem.Text = "Disconnect";
            this.disconnectToolStripMenuItem.Click += new System.EventHandler(this.disconnectToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.softwareLocationToolStripMenuItem,
            this.downloadDataToolStripMenuItem,
            this.realTimeDataToolStripMenuItem});
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(45, 24);
            this.openToolStripMenuItem.Text = "Open";
            // 
            // softwareLocationToolStripMenuItem
            // 
            this.softwareLocationToolStripMenuItem.Name = "softwareLocationToolStripMenuItem";
            this.softwareLocationToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.softwareLocationToolStripMenuItem.Text = "Software Location";
            this.softwareLocationToolStripMenuItem.Click += new System.EventHandler(this.softwareLocationToolStripMenuItem_Click);
            // 
            // downloadDataToolStripMenuItem
            // 
            this.downloadDataToolStripMenuItem.Name = "downloadDataToolStripMenuItem";
            this.downloadDataToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.downloadDataToolStripMenuItem.Text = "Download Data Location";
            this.downloadDataToolStripMenuItem.Click += new System.EventHandler(this.downloadDataToolStripMenuItem_Click);
            // 
            // realTimeDataToolStripMenuItem
            // 
            this.realTimeDataToolStripMenuItem.Name = "realTimeDataToolStripMenuItem";
            this.realTimeDataToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.realTimeDataToolStripMenuItem.Text = "RealTime Data Location";
            this.realTimeDataToolStripMenuItem.Click += new System.EventHandler(this.realTimeDataToolStripMenuItem_Click);
            // 
            // mainTab
            // 
            this.mainTab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainTab.Controls.Add(this.infoTab);
            this.mainTab.Controls.Add(this.settingTab);
            this.mainTab.Controls.Add(this.dataTab);
            this.mainTab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.mainTab.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainTab.Location = new System.Drawing.Point(0, 27);
            this.mainTab.Name = "mainTab";
            this.mainTab.SelectedIndex = 0;
            this.mainTab.Size = new System.Drawing.Size(400, 415);
            this.mainTab.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.mainTab.TabIndex = 1;
            // 
            // infoTab
            // 
            this.infoTab.Controls.Add(this.builddateTextbox);
            this.infoTab.Controls.Add(this.setdateTextbox);
            this.infoTab.Controls.Add(this.checkTextbox);
            this.infoTab.Controls.Add(this.firmwareTextbox);
            this.infoTab.Controls.Add(this.serialTextbox);
            this.infoTab.Controls.Add(this.fixLabel6);
            this.infoTab.Controls.Add(this.fixLabel5);
            this.infoTab.Controls.Add(this.fixLabel4);
            this.infoTab.Controls.Add(this.fixLabel3);
            this.infoTab.Controls.Add(this.fixLabel2);
            this.infoTab.Controls.Add(this.connectioLable);
            this.infoTab.Controls.Add(this.fixLabel1);
            this.infoTab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoTab.Location = new System.Drawing.Point(4, 26);
            this.infoTab.Name = "infoTab";
            this.infoTab.Padding = new System.Windows.Forms.Padding(3);
            this.infoTab.Size = new System.Drawing.Size(392, 385);
            this.infoTab.TabIndex = 0;
            this.infoTab.Text = "Logger Info";
            this.infoTab.UseVisualStyleBackColor = true;
            // 
            // builddateTextbox
            // 
            this.builddateTextbox.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.builddateTextbox.Cursor = System.Windows.Forms.Cursors.Default;
            this.builddateTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.builddateTextbox.Location = new System.Drawing.Point(200, 100);
            this.builddateTextbox.Name = "builddateTextbox";
            this.builddateTextbox.ReadOnly = true;
            this.builddateTextbox.Size = new System.Drawing.Size(160, 26);
            this.builddateTextbox.TabIndex = 0;
            this.builddateTextbox.TabStop = false;
            this.builddateTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // setdateTextbox
            // 
            this.setdateTextbox.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.setdateTextbox.Cursor = System.Windows.Forms.Cursors.Default;
            this.setdateTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setdateTextbox.Location = new System.Drawing.Point(200, 125);
            this.setdateTextbox.Name = "setdateTextbox";
            this.setdateTextbox.ReadOnly = true;
            this.setdateTextbox.Size = new System.Drawing.Size(160, 26);
            this.setdateTextbox.TabIndex = 0;
            this.setdateTextbox.TabStop = false;
            this.setdateTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkTextbox
            // 
            this.checkTextbox.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.checkTextbox.Cursor = System.Windows.Forms.Cursors.Default;
            this.checkTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkTextbox.Location = new System.Drawing.Point(200, 150);
            this.checkTextbox.Name = "checkTextbox";
            this.checkTextbox.ReadOnly = true;
            this.checkTextbox.Size = new System.Drawing.Size(160, 26);
            this.checkTextbox.TabIndex = 0;
            this.checkTextbox.TabStop = false;
            this.checkTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // firmwareTextbox
            // 
            this.firmwareTextbox.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.firmwareTextbox.Cursor = System.Windows.Forms.Cursors.Default;
            this.firmwareTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firmwareTextbox.Location = new System.Drawing.Point(200, 75);
            this.firmwareTextbox.Name = "firmwareTextbox";
            this.firmwareTextbox.ReadOnly = true;
            this.firmwareTextbox.Size = new System.Drawing.Size(160, 26);
            this.firmwareTextbox.TabIndex = 0;
            this.firmwareTextbox.TabStop = false;
            this.firmwareTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // serialTextbox
            // 
            this.serialTextbox.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.serialTextbox.Cursor = System.Windows.Forms.Cursors.Default;
            this.serialTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialTextbox.Location = new System.Drawing.Point(200, 50);
            this.serialTextbox.Name = "serialTextbox";
            this.serialTextbox.ReadOnly = true;
            this.serialTextbox.Size = new System.Drawing.Size(160, 26);
            this.serialTextbox.TabIndex = 0;
            this.serialTextbox.TabStop = false;
            this.serialTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fixLabel6
            // 
            this.fixLabel6.AutoSize = true;
            this.fixLabel6.CausesValidation = false;
            this.fixLabel6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLabel6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLabel6.Location = new System.Drawing.Point(15, 154);
            this.fixLabel6.Name = "fixLabel6";
            this.fixLabel6.Size = new System.Drawing.Size(131, 18);
            this.fixLabel6.TabIndex = 0;
            this.fixLabel6.Text = "Check Point Date";
            // 
            // fixLabel5
            // 
            this.fixLabel5.AutoSize = true;
            this.fixLabel5.CausesValidation = false;
            this.fixLabel5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLabel5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLabel5.Location = new System.Drawing.Point(15, 129);
            this.fixLabel5.Name = "fixLabel5";
            this.fixLabel5.Size = new System.Drawing.Size(129, 18);
            this.fixLabel5.TabIndex = 0;
            this.fixLabel5.Text = "Last Setting Date";
            // 
            // fixLabel4
            // 
            this.fixLabel4.AutoSize = true;
            this.fixLabel4.CausesValidation = false;
            this.fixLabel4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLabel4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLabel4.Location = new System.Drawing.Point(15, 104);
            this.fixLabel4.Name = "fixLabel4";
            this.fixLabel4.Size = new System.Drawing.Size(115, 18);
            this.fixLabel4.TabIndex = 0;
            this.fixLabel4.Text = "Last Build Date";
            // 
            // fixLabel3
            // 
            this.fixLabel3.AutoSize = true;
            this.fixLabel3.CausesValidation = false;
            this.fixLabel3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLabel3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLabel3.Location = new System.Drawing.Point(15, 79);
            this.fixLabel3.Name = "fixLabel3";
            this.fixLabel3.Size = new System.Drawing.Size(131, 18);
            this.fixLabel3.TabIndex = 0;
            this.fixLabel3.Text = "Firmware Version";
            // 
            // fixLabel2
            // 
            this.fixLabel2.AutoSize = true;
            this.fixLabel2.CausesValidation = false;
            this.fixLabel2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLabel2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLabel2.Location = new System.Drawing.Point(15, 54);
            this.fixLabel2.Name = "fixLabel2";
            this.fixLabel2.Size = new System.Drawing.Size(108, 18);
            this.fixLabel2.TabIndex = 0;
            this.fixLabel2.Text = "Serial Number";
            // 
            // connectioLable
            // 
            this.connectioLable.AutoSize = true;
            this.connectioLable.CausesValidation = false;
            this.connectioLable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.connectioLable.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectioLable.ForeColor = System.Drawing.Color.Red;
            this.connectioLable.Location = new System.Drawing.Point(200, 10);
            this.connectioLable.Name = "connectioLable";
            this.connectioLable.Size = new System.Drawing.Size(127, 22);
            this.connectioLable.TabIndex = 0;
            this.connectioLable.Text = "Disconnected";
            // 
            // fixLabel1
            // 
            this.fixLabel1.AutoSize = true;
            this.fixLabel1.CausesValidation = false;
            this.fixLabel1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLabel1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLabel1.Location = new System.Drawing.Point(15, 10);
            this.fixLabel1.Name = "fixLabel1";
            this.fixLabel1.Size = new System.Drawing.Size(120, 22);
            this.fixLabel1.TabIndex = 0;
            this.fixLabel1.Text = "Sigra Logger";
            // 
            // settingTab
            // 
            this.settingTab.Controls.Add(this.radioButton);
            this.settingTab.Controls.Add(this.roleButton);
            this.settingTab.Controls.Add(this.fixLabel9);
            this.settingTab.Controls.Add(this.recordButton);
            this.settingTab.Controls.Add(this.systemButton);
            this.settingTab.Controls.Add(this.clockButton);
            this.settingTab.Controls.Add(this.sensorButton);
            this.settingTab.Location = new System.Drawing.Point(4, 26);
            this.settingTab.Name = "settingTab";
            this.settingTab.Padding = new System.Windows.Forms.Padding(3);
            this.settingTab.Size = new System.Drawing.Size(392, 385);
            this.settingTab.TabIndex = 1;
            this.settingTab.Text = "Settings";
            this.settingTab.UseVisualStyleBackColor = true;
            // 
            // radioButton
            // 
            this.radioButton.BackColor = System.Drawing.Color.Transparent;
            this.radioButton.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton.Location = new System.Drawing.Point(14, 267);
            this.radioButton.Name = "radioButton";
            this.radioButton.Size = new System.Drawing.Size(363, 50);
            this.radioButton.TabIndex = 1;
            this.radioButton.TabStop = false;
            this.radioButton.Text = "Radio";
            this.radioButton.UseVisualStyleBackColor = false;
            this.radioButton.Click += new System.EventHandler(this.Radio_Button_Click);
            // 
            // roleButton
            // 
            this.roleButton.BackColor = System.Drawing.Color.Transparent;
            this.roleButton.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roleButton.Location = new System.Drawing.Point(14, 211);
            this.roleButton.Name = "roleButton";
            this.roleButton.Size = new System.Drawing.Size(363, 50);
            this.roleButton.TabIndex = 0;
            this.roleButton.TabStop = false;
            this.roleButton.Text = "Role";
            this.roleButton.UseVisualStyleBackColor = false;
            this.roleButton.Click += new System.EventHandler(this.Role_Button_Click);
            // 
            // fixLabel9
            // 
            this.fixLabel9.AutoSize = true;
            this.fixLabel9.CausesValidation = false;
            this.fixLabel9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLabel9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLabel9.Location = new System.Drawing.Point(10, 10);
            this.fixLabel9.Name = "fixLabel9";
            this.fixLabel9.Size = new System.Drawing.Size(145, 22);
            this.fixLabel9.TabIndex = 0;
            this.fixLabel9.Text = "Logger Settings";
            // 
            // recordButton
            // 
            this.recordButton.BackColor = System.Drawing.Color.Transparent;
            this.recordButton.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recordButton.Location = new System.Drawing.Point(14, 155);
            this.recordButton.Name = "recordButton";
            this.recordButton.Size = new System.Drawing.Size(363, 50);
            this.recordButton.TabIndex = 0;
            this.recordButton.TabStop = false;
            this.recordButton.Text = "Record";
            this.recordButton.UseVisualStyleBackColor = false;
            this.recordButton.Click += new System.EventHandler(this.Record_Button_Click);
            // 
            // systemButton
            // 
            this.systemButton.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.systemButton.Location = new System.Drawing.Point(14, 323);
            this.systemButton.Name = "systemButton";
            this.systemButton.Size = new System.Drawing.Size(363, 50);
            this.systemButton.TabIndex = 0;
            this.systemButton.TabStop = false;
            this.systemButton.Text = "System";
            this.systemButton.UseVisualStyleBackColor = true;
            this.systemButton.Click += new System.EventHandler(this.SystemButton_Click);
            // 
            // clockButton
            // 
            this.clockButton.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clockButton.Location = new System.Drawing.Point(14, 99);
            this.clockButton.Name = "clockButton";
            this.clockButton.Size = new System.Drawing.Size(363, 50);
            this.clockButton.TabIndex = 0;
            this.clockButton.TabStop = false;
            this.clockButton.Text = "Clock";
            this.clockButton.UseVisualStyleBackColor = true;
            this.clockButton.Click += new System.EventHandler(this.Clock_Button_Click);
            // 
            // sensorButton
            // 
            this.sensorButton.BackColor = System.Drawing.Color.Transparent;
            this.sensorButton.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sensorButton.Location = new System.Drawing.Point(14, 43);
            this.sensorButton.Name = "sensorButton";
            this.sensorButton.Size = new System.Drawing.Size(363, 50);
            this.sensorButton.TabIndex = 0;
            this.sensorButton.TabStop = false;
            this.sensorButton.Text = "Sensors";
            this.sensorButton.UseVisualStyleBackColor = false;
            this.sensorButton.Click += new System.EventHandler(this.Sensor_Button_Click);
            // 
            // dataTab
            // 
            this.dataTab.Controls.Add(this.viewButton);
            this.dataTab.Controls.Add(this.realtimeButton);
            this.dataTab.Controls.Add(this.downloaddataButton);
            this.dataTab.Controls.Add(this.fixaLabel11);
            this.dataTab.Location = new System.Drawing.Point(4, 26);
            this.dataTab.Name = "dataTab";
            this.dataTab.Padding = new System.Windows.Forms.Padding(3);
            this.dataTab.Size = new System.Drawing.Size(392, 385);
            this.dataTab.TabIndex = 2;
            this.dataTab.Text = "Data";
            this.dataTab.UseVisualStyleBackColor = true;
            // 
            // viewButton
            // 
            this.viewButton.BackColor = System.Drawing.Color.Transparent;
            this.viewButton.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewButton.Location = new System.Drawing.Point(14, 171);
            this.viewButton.Name = "viewButton";
            this.viewButton.Size = new System.Drawing.Size(363, 50);
            this.viewButton.TabIndex = 1;
            this.viewButton.TabStop = false;
            this.viewButton.Text = "View";
            this.viewButton.UseVisualStyleBackColor = false;
            this.viewButton.Click += new System.EventHandler(this.viewButton_Click);
            // 
            // realtimeButton
            // 
            this.realtimeButton.BackColor = System.Drawing.Color.Transparent;
            this.realtimeButton.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.realtimeButton.Location = new System.Drawing.Point(14, 115);
            this.realtimeButton.Name = "realtimeButton";
            this.realtimeButton.Size = new System.Drawing.Size(363, 50);
            this.realtimeButton.TabIndex = 0;
            this.realtimeButton.TabStop = false;
            this.realtimeButton.Text = "Real Time";
            this.realtimeButton.UseVisualStyleBackColor = false;
            this.realtimeButton.Click += new System.EventHandler(this.Realtime_button_Click);
            // 
            // downloaddataButton
            // 
            this.downloaddataButton.BackColor = System.Drawing.Color.Transparent;
            this.downloaddataButton.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downloaddataButton.Location = new System.Drawing.Point(14, 59);
            this.downloaddataButton.Name = "downloaddataButton";
            this.downloaddataButton.Size = new System.Drawing.Size(363, 50);
            this.downloaddataButton.TabIndex = 0;
            this.downloaddataButton.TabStop = false;
            this.downloaddataButton.Text = "DownLoad";
            this.downloaddataButton.UseVisualStyleBackColor = false;
            this.downloaddataButton.Click += new System.EventHandler(this.Downloaddata_Button_Click);
            // 
            // fixaLabel11
            // 
            this.fixaLabel11.AutoSize = true;
            this.fixaLabel11.CausesValidation = false;
            this.fixaLabel11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixaLabel11.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixaLabel11.Location = new System.Drawing.Point(15, 10);
            this.fixaLabel11.Name = "fixaLabel11";
            this.fixaLabel11.Size = new System.Drawing.Size(49, 22);
            this.fixaLabel11.TabIndex = 0;
            this.fixaLabel11.Text = "Data";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // logConsole
            // 
            this.logConsole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logConsole.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.logConsole.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logConsole.ForeColor = System.Drawing.SystemColors.Window;
            this.logConsole.Location = new System.Drawing.Point(0, 440);
            this.logConsole.Multiline = true;
            this.logConsole.Name = "logConsole";
            this.logConsole.ReadOnly = true;
            this.logConsole.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logConsole.Size = new System.Drawing.Size(806, 2);
            this.logConsole.TabIndex = 2;
            this.logConsole.TabStop = false;
            this.logConsole.Visible = false;
            // 
            // hideShowCheckbox
            // 
            this.hideShowCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.hideShowCheckbox.AutoSize = true;
            this.hideShowCheckbox.Checked = true;
            this.hideShowCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.hideShowCheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hideShowCheckbox.Location = new System.Drawing.Point(5, 446);
            this.hideShowCheckbox.Name = "hideShowCheckbox";
            this.hideShowCheckbox.Size = new System.Drawing.Size(82, 20);
            this.hideShowCheckbox.TabIndex = 3;
            this.hideShowCheckbox.Text = "Hide Log";
            this.hideShowCheckbox.UseVisualStyleBackColor = true;
            this.hideShowCheckbox.CheckedChanged += new System.EventHandler(this.hideShowCheckbox_CheckedChanged);
            // 
            // mainProgressbar
            // 
            this.mainProgressbar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mainProgressbar.Location = new System.Drawing.Point(610, 442);
            this.mainProgressbar.Name = "mainProgressbar";
            this.mainProgressbar.Size = new System.Drawing.Size(196, 25);
            this.mainProgressbar.TabIndex = 4;
            // 
            // masterLoggerToolStripMenuItem
            // 
            this.masterLoggerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uSBToolStripMenuItem,
            this.rS232CableToolStripMenuItem,
            this.modemToolStripMenuItem,
            this.radioToolStripMenuItem});
            this.masterLoggerToolStripMenuItem.Name = "masterLoggerToolStripMenuItem";
            this.masterLoggerToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.masterLoggerToolStripMenuItem.Text = "Master Logger";
            // 
            // slaveLoggerToolStripMenuItem
            // 
            this.slaveLoggerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rS232CableToolStripMenuItem1,
            this.modemToolStripMenuItem1});
            this.slaveLoggerToolStripMenuItem.Name = "slaveLoggerToolStripMenuItem";
            this.slaveLoggerToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.slaveLoggerToolStripMenuItem.Text = "Slave Logger";
            // 
            // uSBToolStripMenuItem
            // 
            this.uSBToolStripMenuItem.Name = "uSBToolStripMenuItem";
            this.uSBToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.uSBToolStripMenuItem.Text = "USB";
            this.uSBToolStripMenuItem.Click += new System.EventHandler(this.uSBToolStripMenuItem_Click);
            // 
            // rS232CableToolStripMenuItem
            // 
            this.rS232CableToolStripMenuItem.Name = "rS232CableToolStripMenuItem";
            this.rS232CableToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.rS232CableToolStripMenuItem.Text = "RS232 Cable";
            this.rS232CableToolStripMenuItem.Click += new System.EventHandler(this.rS232CableToolStripMenuItem_Click);
            // 
            // modemToolStripMenuItem
            // 
            this.modemToolStripMenuItem.Name = "modemToolStripMenuItem";
            this.modemToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.modemToolStripMenuItem.Text = "Modem";
            this.modemToolStripMenuItem.Click += new System.EventHandler(this.modemToolStripMenuItem_Click);
            // 
            // radioToolStripMenuItem
            // 
            this.radioToolStripMenuItem.Name = "radioToolStripMenuItem";
            this.radioToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.radioToolStripMenuItem.Text = "Radio";
            this.radioToolStripMenuItem.Click += new System.EventHandler(this.radioToolStripMenuItem_Click);
            // 
            // rS232CableToolStripMenuItem1
            // 
            this.rS232CableToolStripMenuItem1.Name = "rS232CableToolStripMenuItem1";
            this.rS232CableToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.rS232CableToolStripMenuItem1.Text = "RS232 Cable";
            this.rS232CableToolStripMenuItem1.Click += new System.EventHandler(this.rS232CableToolStripMenuItem1_Click);
            // 
            // modemToolStripMenuItem1
            // 
            this.modemToolStripMenuItem1.Name = "modemToolStripMenuItem1";
            this.modemToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.modemToolStripMenuItem1.Text = "Modem";
            this.modemToolStripMenuItem1.Click += new System.EventHandler(this.modemToolStripMenuItem1_Click);
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enableCoreSettingsToolStripMenuItem});
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(52, 24);
            this.settingToolStripMenuItem.Text = "Setting";
            // 
            // enableCoreSettingsToolStripMenuItem
            // 
            this.enableCoreSettingsToolStripMenuItem.Name = "enableCoreSettingsToolStripMenuItem";
            this.enableCoreSettingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.enableCoreSettingsToolStripMenuItem.Text = "Enable Core Settings";
            this.enableCoreSettingsToolStripMenuItem.Click += new System.EventHandler(this.enableCoreSettingsToolStripMenuItem_Click);
            // 
            // Main_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(806, 468);
            this.Controls.Add(this.mainProgressbar);
            this.Controls.Add(this.hideShowCheckbox);
            this.Controls.Add(this.logConsole);
            this.Controls.Add(this.mainTab);
            this.Controls.Add(this.Main_Menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.Main_Menu;
            this.Name = "Main_form";
            this.Text = "Sigra Logger Software";
            this.Load += new System.EventHandler(this.Main_form_Load);
            this.Main_Menu.ResumeLayout(false);
            this.Main_Menu.PerformLayout();
            this.mainTab.ResumeLayout(false);
            this.infoTab.ResumeLayout(false);
            this.infoTab.PerformLayout();
            this.settingTab.ResumeLayout(false);
            this.settingTab.PerformLayout();
            this.dataTab.ResumeLayout(false);
            this.dataTab.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.TabControl sensorTab;
        private System.Windows.Forms.TabPage channelTab;
        public System.Windows.Forms.Label channelLabel;
        public System.Windows.Forms.Button channelButton8;
        public System.Windows.Forms.Button channelButton7;
        public System.Windows.Forms.Button channelButton6;
        public System.Windows.Forms.Button channelButton5;
        public System.Windows.Forms.Button channelButton4;
        public System.Windows.Forms.Button channelButton3;
        public System.Windows.Forms.Button channelButton2;
        public System.Windows.Forms.Button channelButton1;
        public System.Windows.Forms.Label settleLabel;
        public System.Windows.Forms.Label sweepLabel;
        public System.Windows.Forms.TextBox settleTextbox;
        public System.Windows.Forms.TextBox sweepTextbox1;
        public System.Windows.Forms.TextBox sweepTextbox2;
        public System.Windows.Forms.Label toLabel;
        public System.Windows.Forms.Label cycleLabel;
        public System.Windows.Forms.TextBox cycleTextbox;
        public System.Windows.Forms.Button sensorPushButton;
        public System.Windows.Forms.Button sensorResetButton;
        public System.Windows.Forms.ComboBox sensorDropdown;
        private System.Windows.Forms.Label fixLabel7;
        public System.Windows.Forms.Label x4Label;
        public System.Windows.Forms.Label x3Label;
        public System.Windows.Forms.Label x2Label;
        public System.Windows.Forms.Label x1Label;
        public System.Windows.Forms.Label x0Label;
        public System.Windows.Forms.Label coefficientLabel;
        public System.Windows.Forms.TextBox unitTextbox;
        public System.Windows.Forms.Label unitLabel;
        public System.Windows.Forms.ComboBox compensatedDropdown;
        public System.Windows.Forms.Label compensatedLabel;
        public System.Windows.Forms.ComboBox equationDropdown;
        public System.Windows.Forms.Label equationLabel;
        public System.Windows.Forms.TextBox x4Textbox;
        public System.Windows.Forms.TextBox x3Textbox;
        public System.Windows.Forms.TextBox x2Textbox;
        public System.Windows.Forms.TextBox x1Textbox;
        public System.Windows.Forms.TextBox x0Textbox;
        public System.Windows.Forms.TextBox compensationTextbox;
        public System.Windows.Forms.Label compensationLabel;
        public System.Windows.Forms.ComboBox counterresetDropdown;
        public System.Windows.Forms.Label counterresetLabel;
        private void InitializeSensorTab()
        {
            this.sensorTab = new System.Windows.Forms.TabControl();
            this.channelTab = new System.Windows.Forms.TabPage();

            this.x4Label = new System.Windows.Forms.Label();
            this.x3Label = new System.Windows.Forms.Label();
            this.x2Label = new System.Windows.Forms.Label();
            this.x1Label = new System.Windows.Forms.Label();
            this.x0Label = new System.Windows.Forms.Label();

            this.x0Textbox = new System.Windows.Forms.TextBox();
            this.x1Textbox = new System.Windows.Forms.TextBox();
            this.x2Textbox = new System.Windows.Forms.TextBox();
            this.x3Textbox = new System.Windows.Forms.TextBox();
            this.x4Textbox = new System.Windows.Forms.TextBox();

            this.coefficientLabel = new System.Windows.Forms.Label();
            this.unitLabel = new System.Windows.Forms.Label();
            this.cycleLabel = new System.Windows.Forms.Label();
            this.toLabel = new System.Windows.Forms.Label();
            this.sweepLabel = new System.Windows.Forms.Label();
            this.settleLabel = new System.Windows.Forms.Label();
            this.compensatedLabel = new System.Windows.Forms.Label();
            this.equationLabel = new System.Windows.Forms.Label();
            this.compensationLabel = new System.Windows.Forms.Label();
            this.counterresetLabel = new System.Windows.Forms.Label();
            this.channelLabel = new System.Windows.Forms.Label();
            this.fixLabel7 = new System.Windows.Forms.Label();

            this.channelButton8 = new System.Windows.Forms.Button();
            this.channelButton7 = new System.Windows.Forms.Button();
            this.channelButton6 = new System.Windows.Forms.Button();
            this.channelButton5 = new System.Windows.Forms.Button();
            this.channelButton4 = new System.Windows.Forms.Button();
            this.channelButton3 = new System.Windows.Forms.Button();
            this.channelButton2 = new System.Windows.Forms.Button();
            this.channelButton1 = new System.Windows.Forms.Button();

            this.unitTextbox = new System.Windows.Forms.TextBox();
            this.cycleTextbox = new System.Windows.Forms.TextBox();
            this.sweepTextbox1 = new System.Windows.Forms.TextBox();
            this.sweepTextbox2 = new System.Windows.Forms.TextBox();
            this.settleTextbox = new System.Windows.Forms.TextBox();
            this.compensationTextbox = new System.Windows.Forms.TextBox();

            this.compensatedDropdown = new System.Windows.Forms.ComboBox();
            this.equationDropdown = new System.Windows.Forms.ComboBox();
            this.sensorDropdown = new System.Windows.Forms.ComboBox();
            this.counterresetDropdown = new System.Windows.Forms.ComboBox();

            this.sensorResetButton = new System.Windows.Forms.Button();
            this.sensorPushButton = new System.Windows.Forms.Button();

            this.sensorTab.SuspendLayout();
            this.channelTab.SuspendLayout();

            // 
            // sensor_tab
            // 
            this.sensorTab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorTab.Controls.Add(this.channelTab);
            this.sensorTab.Font = new System.Drawing.Font("Arial", 11.25F);
            this.sensorTab.ItemSize = new System.Drawing.Size(84, 22);
            this.sensorTab.Location = new System.Drawing.Point(402, 27);
            this.sensorTab.Multiline = true;
            this.sensorTab.Name = "sensor_tab";
            this.sensorTab.SelectedIndex = 0;
            this.sensorTab.Size = new System.Drawing.Size(400, 415);
            this.sensorTab.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.sensorTab.TabIndex = 3;
            // 
            // channel_tab
            // 
            this.channelTab.Controls.Add(this.counterresetDropdown);
            this.channelTab.Controls.Add(this.counterresetLabel);
            this.channelTab.Controls.Add(this.compensationTextbox);
            this.channelTab.Controls.Add(this.compensationLabel);
            this.channelTab.Controls.Add(this.compensatedDropdown);
            this.channelTab.Controls.Add(this.compensatedLabel);
            this.channelTab.Controls.Add(this.equationDropdown);
            this.channelTab.Controls.Add(this.equationLabel);
            this.channelTab.Controls.Add(this.x4Textbox);
            this.channelTab.Controls.Add(this.x3Textbox);
            this.channelTab.Controls.Add(this.x2Textbox);
            this.channelTab.Controls.Add(this.x1Textbox);
            this.channelTab.Controls.Add(this.x0Textbox);
            this.channelTab.Controls.Add(this.x4Label);
            this.channelTab.Controls.Add(this.x3Label);
            this.channelTab.Controls.Add(this.x2Label);
            this.channelTab.Controls.Add(this.x1Label);
            this.channelTab.Controls.Add(this.x0Label);
            this.channelTab.Controls.Add(this.coefficientLabel);
            this.channelTab.Controls.Add(this.unitTextbox);
            this.channelTab.Controls.Add(this.unitLabel);
            this.channelTab.Controls.Add(this.sensorResetButton);
            this.channelTab.Controls.Add(this.sensorPushButton);
            this.channelTab.Controls.Add(this.sweepTextbox2);
            this.channelTab.Controls.Add(this.cycleTextbox);
            this.channelTab.Controls.Add(this.sweepTextbox1);
            this.channelTab.Controls.Add(this.cycleLabel);
            this.channelTab.Controls.Add(this.toLabel);
            this.channelTab.Controls.Add(this.sweepLabel);
            this.channelTab.Controls.Add(this.settleTextbox);
            this.channelTab.Controls.Add(this.settleLabel);
            this.channelTab.Controls.Add(this.channelButton8);
            this.channelTab.Controls.Add(this.channelButton7);
            this.channelTab.Controls.Add(this.channelButton6);
            this.channelTab.Controls.Add(this.channelButton5);
            this.channelTab.Controls.Add(this.channelButton4);
            this.channelTab.Controls.Add(this.channelButton3);
            this.channelTab.Controls.Add(this.channelButton2);
            this.channelTab.Controls.Add(this.sensorDropdown);
            this.channelTab.Controls.Add(this.fixLabel7);
            this.channelTab.Controls.Add(this.channelLabel);
            this.channelTab.Controls.Add(this.channelButton1);
            this.channelTab.Location = new System.Drawing.Point(4, 26);
            this.channelTab.Name = "channel_tab";
            this.channelTab.Padding = new System.Windows.Forms.Padding(3);
            this.channelTab.Size = new System.Drawing.Size(392, 385);
            this.channelTab.TabIndex = 7;
            this.channelTab.Text = "Sensors";
            this.channelTab.UseVisualStyleBackColor = true;
            // 
            // x4_label
            // 
            this.x4Label.AutoSize = true;
            this.x4Label.CausesValidation = false;
            this.x4Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.x4Label.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x4Label.Location = new System.Drawing.Point(90, 223);
            this.x4Label.Name = "x4_label";
            this.x4Label.Size = new System.Drawing.Size(31, 18);
            this.x4Label.TabIndex = 0;
            this.x4Label.Text = "x^4";
            // 
            // x3_label
            // 
            this.x3Label.AutoSize = true;
            this.x3Label.CausesValidation = false;
            this.x3Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.x3Label.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x3Label.Location = new System.Drawing.Point(90, 243);
            this.x3Label.Name = "x3_label";
            this.x3Label.Size = new System.Drawing.Size(31, 18);
            this.x3Label.TabIndex = 0;
            this.x3Label.Text = "x^3";
            // 
            // x2_label
            // 
            this.x2Label.AutoSize = true;
            this.x2Label.CausesValidation = false;
            this.x2Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.x2Label.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x2Label.Location = new System.Drawing.Point(90, 263);
            this.x2Label.Name = "x2_label";
            this.x2Label.Size = new System.Drawing.Size(31, 18);
            this.x2Label.TabIndex = 0;
            this.x2Label.Text = "x^2";
            // 
            // x1_label
            // 
            this.x1Label.AutoSize = true;
            this.x1Label.CausesValidation = false;
            this.x1Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.x1Label.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x1Label.Location = new System.Drawing.Point(90, 283);
            this.x1Label.Name = "x1_label";
            this.x1Label.Size = new System.Drawing.Size(31, 18);
            this.x1Label.TabIndex = 0;
            this.x1Label.Text = "x^1";
            // 
            // x0_label
            // 
            this.x0Label.AutoSize = true;
            this.x0Label.CausesValidation = false;
            this.x0Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.x0Label.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x0Label.Location = new System.Drawing.Point(90, 303);
            this.x0Label.Name = "x0_label";
            this.x0Label.Size = new System.Drawing.Size(31, 18);
            this.x0Label.TabIndex = 0;
            this.x0Label.Text = "x^0";
            // 
            // coefficient_label
            // 
            this.coefficientLabel.AutoSize = true;
            this.coefficientLabel.CausesValidation = false;
            this.coefficientLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.coefficientLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coefficientLabel.Location = new System.Drawing.Point(10, 258);
            this.coefficientLabel.Name = "coefficient_label";
            this.coefficientLabel.Size = new System.Drawing.Size(83, 18);
            this.coefficientLabel.TabIndex = 0;
            this.coefficientLabel.Text = "Coefficient";
            // 
            // unit_textbox
            // 
            this.unitTextbox.Location = new System.Drawing.Point(112, 70);
            this.unitTextbox.Name = "unit_textbox";
            this.unitTextbox.Size = new System.Drawing.Size(127, 25);
            this.unitTextbox.TabIndex = 2;
            this.unitTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // unit_label
            // 
            this.unitLabel.AutoSize = true;
            this.unitLabel.CausesValidation = false;
            this.unitLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.unitLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unitLabel.Location = new System.Drawing.Point(11, 70);
            this.unitLabel.Name = "unit_label";
            this.unitLabel.Size = new System.Drawing.Size(35, 18);
            this.unitLabel.TabIndex = 0;
            this.unitLabel.Text = "Unit";
            // 
            // sensor_reset_button
            // 
            this.sensorResetButton.Location = new System.Drawing.Point(14, 332);
            this.sensorResetButton.Name = "sensor_reset_button";
            this.sensorResetButton.Size = new System.Drawing.Size(126, 37);
            this.sensorResetButton.TabIndex = 30;
            this.sensorResetButton.Text = "Reset All";
            this.sensorResetButton.UseVisualStyleBackColor = true;
            // 
            // sensor_push_button
            // 
            this.sensorPushButton.Location = new System.Drawing.Point(259, 332);
            this.sensorPushButton.Name = "sensor_push_button";
            this.sensorPushButton.Size = new System.Drawing.Size(126, 37);
            this.sensorPushButton.TabIndex = 29;
            this.sensorPushButton.Text = "Push Setting";
            this.sensorPushButton.UseVisualStyleBackColor = true;
            // 
            // sweep_textbox2
            // 
            this.sweepTextbox2.Location = new System.Drawing.Point(184, 160);
            this.sweepTextbox2.Name = "sweep_textbox2";
            this.sweepTextbox2.Size = new System.Drawing.Size(55, 25);
            this.sweepTextbox2.TabIndex = 6;
            this.sweepTextbox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cycle_textbox
            // 
            this.cycleTextbox.Location = new System.Drawing.Point(112, 130);
            this.cycleTextbox.Name = "cycle_textbox";
            this.cycleTextbox.Size = new System.Drawing.Size(127, 25);
            this.cycleTextbox.TabIndex = 4;
            this.cycleTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // sweep_textbox1
            // 
            this.sweepTextbox1.Location = new System.Drawing.Point(112, 160);
            this.sweepTextbox1.Name = "sweep_textbox1";
            this.sweepTextbox1.Size = new System.Drawing.Size(55, 25);
            this.sweepTextbox1.TabIndex = 5;
            this.sweepTextbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cycle_label
            // 
            this.cycleLabel.AutoSize = true;
            this.cycleLabel.CausesValidation = false;
            this.cycleLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cycleLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cycleLabel.Location = new System.Drawing.Point(10, 130);
            this.cycleLabel.Name = "cycle_label";
            this.cycleLabel.Size = new System.Drawing.Size(55, 18);
            this.cycleLabel.TabIndex = 0;
            this.cycleLabel.Text = "Cycles";
            // 
            // to_label
            // 
            this.toLabel.AutoSize = true;
            this.toLabel.CausesValidation = false;
            this.toLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toLabel.Location = new System.Drawing.Point(167, 160);
            this.toLabel.Name = "to_label";
            this.toLabel.Size = new System.Drawing.Size(17, 18);
            this.toLabel.TabIndex = 0;
            this.toLabel.Text = "~";
            // 
            // sweep_label
            // 
            this.sweepLabel.AutoSize = true;
            this.sweepLabel.CausesValidation = false;
            this.sweepLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sweepLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sweepLabel.Location = new System.Drawing.Point(10, 160);
            this.sweepLabel.Name = "sweep_label";
            this.sweepLabel.Size = new System.Drawing.Size(89, 18);
            this.sweepLabel.TabIndex = 0;
            this.sweepLabel.Text = "Sweep (Hz)";
            // 
            // settle_textbox
            // 
            this.settleTextbox.Location = new System.Drawing.Point(112, 100);
            this.settleTextbox.Name = "settle_textbox";
            this.settleTextbox.Size = new System.Drawing.Size(127, 25);
            this.settleTextbox.TabIndex = 3;
            this.settleTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // settle_label
            // 
            this.settleLabel.AutoSize = true;
            this.settleLabel.CausesValidation = false;
            this.settleLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settleLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settleLabel.Location = new System.Drawing.Point(11, 100);
            this.settleLabel.Name = "settle_label";
            this.settleLabel.Size = new System.Drawing.Size(83, 18);
            this.settleLabel.TabIndex = 0;
            this.settleLabel.Text = "Settle (ms)";
            // 
            // channel_button8
            // 
            this.channelButton8.BackColor = System.Drawing.SystemColors.Info;
            this.channelButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.channelButton8.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channelButton8.Location = new System.Drawing.Point(245, 241);
            this.channelButton8.Name = "channel_button8";
            this.channelButton8.Size = new System.Drawing.Size(140, 27);
            this.channelButton8.TabIndex = 21;
            this.channelButton8.Text = "Channel 8";
            this.channelButton8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.channelButton8.UseVisualStyleBackColor = false;
            // 
            // channel_button7
            // 
            this.channelButton7.BackColor = System.Drawing.SystemColors.Info;
            this.channelButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.channelButton7.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channelButton7.Location = new System.Drawing.Point(245, 208);
            this.channelButton7.Name = "channel_button7";
            this.channelButton7.Size = new System.Drawing.Size(140, 27);
            this.channelButton7.TabIndex = 20;
            this.channelButton7.Text = "Channel 7";
            this.channelButton7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.channelButton7.UseVisualStyleBackColor = false;
            // 
            // channel_button6
            // 
            this.channelButton6.BackColor = System.Drawing.SystemColors.Info;
            this.channelButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.channelButton6.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channelButton6.Location = new System.Drawing.Point(245, 175);
            this.channelButton6.Name = "channel_button6";
            this.channelButton6.Size = new System.Drawing.Size(140, 27);
            this.channelButton6.TabIndex = 19;
            this.channelButton6.Text = "Channel 6";
            this.channelButton6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.channelButton6.UseVisualStyleBackColor = false;
            // 
            // channel_button5
            // 
            this.channelButton5.BackColor = System.Drawing.SystemColors.Info;
            this.channelButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.channelButton5.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channelButton5.Location = new System.Drawing.Point(245, 142);
            this.channelButton5.Name = "channel_button5";
            this.channelButton5.Size = new System.Drawing.Size(140, 27);
            this.channelButton5.TabIndex = 18;
            this.channelButton5.Text = "Channel 5";
            this.channelButton5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.channelButton5.UseVisualStyleBackColor = false;
            // 
            // channel_button4
            // 
            this.channelButton4.BackColor = System.Drawing.SystemColors.Info;
            this.channelButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.channelButton4.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channelButton4.Location = new System.Drawing.Point(245, 109);
            this.channelButton4.Name = "channel_button4";
            this.channelButton4.Size = new System.Drawing.Size(140, 27);
            this.channelButton4.TabIndex = 17;
            this.channelButton4.Text = "Channel 4";
            this.channelButton4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.channelButton4.UseVisualStyleBackColor = false;
            // 
            // channel_button3
            // 
            this.channelButton3.BackColor = System.Drawing.SystemColors.Info;
            this.channelButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.channelButton3.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channelButton3.Location = new System.Drawing.Point(245, 76);
            this.channelButton3.Name = "channel_button3";
            this.channelButton3.Size = new System.Drawing.Size(140, 27);
            this.channelButton3.TabIndex = 16;
            this.channelButton3.Text = "Channel 3";
            this.channelButton3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.channelButton3.UseVisualStyleBackColor = false;
            // 
            // channel_button2
            // 
            this.channelButton2.BackColor = System.Drawing.SystemColors.Info;
            this.channelButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.channelButton2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channelButton2.Location = new System.Drawing.Point(245, 43);
            this.channelButton2.Name = "channel_button2";
            this.channelButton2.Size = new System.Drawing.Size(140, 27);
            this.channelButton2.TabIndex = 15;
            this.channelButton2.Text = "Channel 2";
            this.channelButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.channelButton2.UseVisualStyleBackColor = false;
            // 
            // channel_button1
            // 
            this.channelButton1.BackColor = System.Drawing.SystemColors.Info;
            this.channelButton1.Cursor = System.Windows.Forms.Cursors.Default;
            this.channelButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.channelButton1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.channelButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.channelButton1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channelButton1.Location = new System.Drawing.Point(245, 10);
            this.channelButton1.Name = "channel_button1";
            this.channelButton1.Size = new System.Drawing.Size(140, 27);
            this.channelButton1.TabIndex = 14;
            this.channelButton1.Text = "Channel 1";
            this.channelButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.channelButton1.UseVisualStyleBackColor = false;
            // 
            // sensor_dropdown
            // 
            this.sensorDropdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sensorDropdown.FormattingEnabled = true;
            this.sensorDropdown.Items.AddRange(LoggerSoftware.Core.Constant.sensor_types);
            this.sensorDropdown.Location = new System.Drawing.Point(112, 40);
            this.sensorDropdown.Name = "sensor_dropdown";
            this.sensorDropdown.Size = new System.Drawing.Size(127, 25);
            this.sensorDropdown.TabIndex = 1;
            // 
            // fix_label7
            // 
            this.fixLabel7.AutoSize = true;
            this.fixLabel7.CausesValidation = false;
            this.fixLabel7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLabel7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLabel7.Location = new System.Drawing.Point(10, 40);
            this.fixLabel7.Name = "fix_label7";
            this.fixLabel7.Size = new System.Drawing.Size(95, 18);
            this.fixLabel7.TabIndex = 0;
            this.fixLabel7.Text = "Sensor Type";
            // 
            // channel_label
            // 
            this.channelLabel.AutoSize = true;
            this.channelLabel.CausesValidation = false;
            this.channelLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.channelLabel.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channelLabel.Location = new System.Drawing.Point(10, 10);
            this.channelLabel.Name = "channel_label";
            this.channelLabel.Size = new System.Drawing.Size(95, 22);
            this.channelLabel.TabIndex = 0;
            this.channelLabel.Text = "Channel 1";
            // 
            // x0_textbox
            // 
            this.x0Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x0Textbox.Location = new System.Drawing.Point(122, 301);
            this.x0Textbox.Name = "x0_textbox";
            this.x0Textbox.Size = new System.Drawing.Size(116, 22);
            this.x0Textbox.TabIndex = 12;
            this.x0Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // x1_textbox
            // 
            this.x1Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x1Textbox.Location = new System.Drawing.Point(122, 281);
            this.x1Textbox.Name = "x1_textbox";
            this.x1Textbox.Size = new System.Drawing.Size(116, 22);
            this.x1Textbox.TabIndex = 11;
            this.x1Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // x2_textbox
            // 
            this.x2Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x2Textbox.Location = new System.Drawing.Point(122, 261);
            this.x2Textbox.Name = "x2_textbox";
            this.x2Textbox.Size = new System.Drawing.Size(116, 22);
            this.x2Textbox.TabIndex = 10;
            this.x2Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // x3_textbox
            // 
            this.x3Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x3Textbox.Location = new System.Drawing.Point(122, 241);
            this.x3Textbox.Name = "x3_textbox";
            this.x3Textbox.Size = new System.Drawing.Size(116, 22);
            this.x3Textbox.TabIndex = 9;
            this.x3Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // x4_textbox
            // 
            this.x4Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x4Textbox.Location = new System.Drawing.Point(122, 221);
            this.x4Textbox.Name = "x4_textbox";
            this.x4Textbox.Size = new System.Drawing.Size(116, 22);
            this.x4Textbox.TabIndex = 8;
            this.x4Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // equation_label
            // 
            this.equationLabel.AutoSize = true;
            this.equationLabel.CausesValidation = false;
            this.equationLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.equationLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equationLabel.Location = new System.Drawing.Point(10, 192);
            this.equationLabel.Name = "equation_label";
            this.equationLabel.Size = new System.Drawing.Size(107, 18);
            this.equationLabel.TabIndex = 0;
            this.equationLabel.Text = "Equation";
            // 
            // equation_dropdown
            // 
            this.equationDropdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.equationDropdown.FormattingEnabled = true;
            this.equationDropdown.Items.AddRange(LoggerSoftware.Core.Constant.equation_types);
            this.equationDropdown.Location = new System.Drawing.Point(112, 190);
            this.equationDropdown.Name = "equation_dropdown";
            this.equationDropdown.Size = new System.Drawing.Size(127, 25);
            this.equationDropdown.TabIndex = 7;
            // 
            // compensated_label
            // 
            this.compensatedLabel.AutoSize = true;
            this.compensatedLabel.CausesValidation = false;
            this.compensatedLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.compensatedLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.compensatedLabel.Location = new System.Drawing.Point(245, 280);
            this.compensatedLabel.Name = "compensated_label";
            this.compensatedLabel.Size = new System.Drawing.Size(107, 18);
            this.compensatedLabel.TabIndex = 0;
            this.compensatedLabel.Text = "Compensated";
            // 
            // compensated_dropdown
            // 
            this.compensatedDropdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.compensatedDropdown.FormattingEnabled = true;
            this.compensatedDropdown.Items.AddRange(new object[] {
            "No",
            "Yes"});
            this.compensatedDropdown.Location = new System.Drawing.Point(250, 300);
            this.compensatedDropdown.Name = "compensated_dropdown";
            this.compensatedDropdown.Size = new System.Drawing.Size(55, 25);
            this.compensatedDropdown.TabIndex = 13;
            // 
            // compensation_label
            // 
            this.compensationLabel.AutoSize = true;
            this.compensationLabel.CausesValidation = false;
            this.compensationLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.compensationLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.compensationLabel.Location = new System.Drawing.Point(10, 304);
            this.compensationLabel.Name = "compensation_label";
            this.compensationLabel.Size = new System.Drawing.Size(106, 18);
            this.compensationLabel.TabIndex = 0;
            this.compensationLabel.Text = "Compensation  Channel";
            // 
            // compensation_textbox
            // 
            this.compensationTextbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.compensationTextbox.Location = new System.Drawing.Point(188, 302);
            this.compensationTextbox.Name = "compensation_textbox";
            this.compensationTextbox.Size = new System.Drawing.Size(50, 22);
            this.compensationTextbox.TabIndex = 47;
            this.compensationTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // counterreset_label
            // 
            this.counterresetLabel.AutoSize = true;
            this.counterresetLabel.CausesValidation = false;
            this.counterresetLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.counterresetLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.counterresetLabel.Location = new System.Drawing.Point(11, 100);
            this.counterresetLabel.Name = "counterreset_label";
            this.counterresetLabel.Size = new System.Drawing.Size(108, 18);
            this.counterresetLabel.TabIndex = 0;
            this.counterresetLabel.Text = "Counter\nReset On\nRecord";
            // 
            // counterreset_dropdown
            // 
            this.counterresetDropdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.counterresetDropdown.FormattingEnabled = true;
            this.counterresetDropdown.Items.AddRange(new object[] {
            "No",
            "Yes"});
            this.counterresetDropdown.Location = new System.Drawing.Point(112, 120);
            this.counterresetDropdown.Name = "counterreset_dropdown";
            this.counterresetDropdown.Size = new System.Drawing.Size(55, 25);
            this.counterresetDropdown.TabIndex = 49;

            SensorTab_BindEvent();

            this.Controls.Add(this.sensorTab);

            this.sensorTab.Visible = false;

            this.sensorTab.ResumeLayout(false);
            this.channelTab.ResumeLayout(false);
            this.channelTab.PerformLayout();
        }

        private void SensorTab_BindEvent()
        {
            this.channelButton1.Click += new System.EventHandler(Software.sensorTab.Channel_Button_Click);
            this.channelButton2.Click += new System.EventHandler(Software.sensorTab.Channel_Button_Click);
            this.channelButton3.Click += new System.EventHandler(Software.sensorTab.Channel_Button_Click);
            this.channelButton4.Click += new System.EventHandler(Software.sensorTab.Channel_Button_Click);
            this.channelButton5.Click += new System.EventHandler(Software.sensorTab.Channel_Button_Click);
            this.channelButton6.Click += new System.EventHandler(Software.sensorTab.Channel_Button_Click);
            this.channelButton7.Click += new System.EventHandler(Software.sensorTab.Channel_Button_Click);
            this.channelButton8.Click += new System.EventHandler(Software.sensorTab.Channel_Button_Click);

            this.sensorPushButton.Click += new System.EventHandler(Software.sensorTab.sensor_push_button_Click);
            this.sensorResetButton.Click += new System.EventHandler(Software.sensorTab.sensor_reset_button_Click);

            this.sensorDropdown.SelectedIndexChanged += new System.EventHandler(Software.sensorTab.Sensor_Dropdown_SelectedIndexChanged);
            this.compensatedDropdown.SelectedIndexChanged += new System.EventHandler(Software.sensorTab.compensated_dropdown_SelectedIndexChanged);
            this.equationDropdown.SelectedIndexChanged += new System.EventHandler(Software.sensorTab.Equation_Dropdown_SelectedIndexChanged);
        }

        private System.Windows.Forms.TabControl clockTab;
        private System.Windows.Forms.TabPage rtcTab;
        private System.Windows.Forms.Button customtimeButton;
        private System.Windows.Forms.Button systemtimeButton;
        private System.Windows.Forms.Label customtimeLabel;
        private System.Windows.Forms.Label systemtimeLabel;
        private System.Windows.Forms.Label fixLabel10;
        public System.Windows.Forms.MaskedTextBox localtimeTextbox;
        public System.Windows.Forms.MaskedTextBox clockTextbox;
        private void InitializeClockTab()
        {
            this.clockTab = new System.Windows.Forms.TabControl();
            this.rtcTab = new System.Windows.Forms.TabPage();
            this.customtimeButton = new System.Windows.Forms.Button();
            this.systemtimeButton = new System.Windows.Forms.Button();
            this.customtimeLabel = new System.Windows.Forms.Label();
            this.systemtimeLabel = new System.Windows.Forms.Label();
            this.fixLabel10 = new System.Windows.Forms.Label();
            this.localtimeTextbox = new System.Windows.Forms.MaskedTextBox();
            this.clockTextbox = new System.Windows.Forms.MaskedTextBox();

            this.clockTab.SuspendLayout();
            this.rtcTab.SuspendLayout();
            // 
            // clock_tab
            // 
            this.clockTab.Controls.Add(this.rtcTab);
            this.clockTab.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clockTab.ItemSize = new System.Drawing.Size(84, 22);
            this.clockTab.Location = new System.Drawing.Point(402, 27);
            this.clockTab.Name = "clock_tab";
            this.clockTab.SelectedIndex = 0;
            this.clockTab.Size = new System.Drawing.Size(400, 415);
            this.clockTab.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.clockTab.TabIndex = 3;
            this.clockTab.Visible = false;
            // 
            // rtc_tab
            // 
            this.rtcTab.Controls.Add(this.customtimeButton);
            this.rtcTab.Controls.Add(this.systemtimeButton);
            this.rtcTab.Controls.Add(this.customtimeLabel);
            this.rtcTab.Controls.Add(this.systemtimeLabel);
            this.rtcTab.Controls.Add(this.fixLabel10);
            this.rtcTab.Controls.Add(this.localtimeTextbox);
            this.rtcTab.Controls.Add(this.clockTextbox);
            this.rtcTab.Location = new System.Drawing.Point(4, 26);
            this.rtcTab.Name = "rtc_tab";
            this.rtcTab.Padding = new System.Windows.Forms.Padding(3);
            this.rtcTab.Size = new System.Drawing.Size(392, 385);
            this.rtcTab.TabIndex = 0;
            this.rtcTab.Text = "Real Time Clock";
            this.rtcTab.UseVisualStyleBackColor = true;
            // 
            // customtime_button
            // 
            this.customtimeButton.Location = new System.Drawing.Point(232, 189);
            this.customtimeButton.Name = "customtime_button";
            this.customtimeButton.Size = new System.Drawing.Size(154, 29);
            this.customtimeButton.TabIndex = 4;
            this.customtimeButton.Text = "Set To Custom Time";
            this.customtimeButton.UseVisualStyleBackColor = true;
            this.customtimeButton.Click += new System.EventHandler(Software.clockTab.customtime_button_Click);
            // 
            // systemtime_button
            // 
            this.systemtimeButton.Location = new System.Drawing.Point(232, 79);
            this.systemtimeButton.Name = "systemtime_button";
            this.systemtimeButton.Size = new System.Drawing.Size(154, 29);
            this.systemtimeButton.TabIndex = 3;
            this.systemtimeButton.Text = "Set To System Time";
            this.systemtimeButton.UseVisualStyleBackColor = true;
            this.systemtimeButton.Click += new System.EventHandler(Software.clockTab.systemtime_button_Click);
            // 
            // customtime_label
            // 
            this.customtimeLabel.AutoSize = true;
            this.customtimeLabel.CausesValidation = false;
            this.customtimeLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customtimeLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customtimeLabel.Location = new System.Drawing.Point(7, 158);
            this.customtimeLabel.Name = "customtime_label";
            this.customtimeLabel.Size = new System.Drawing.Size(100, 18);
            this.customtimeLabel.TabIndex = 2;
            this.customtimeLabel.Text = "Custom Time";
            // 
            // systemtime_label
            // 
            this.systemtimeLabel.AutoSize = true;
            this.systemtimeLabel.CausesValidation = false;
            this.systemtimeLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.systemtimeLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.systemtimeLabel.Location = new System.Drawing.Point(7, 50);
            this.systemtimeLabel.Name = "systemtime_label";
            this.systemtimeLabel.Size = new System.Drawing.Size(98, 18);
            this.systemtimeLabel.TabIndex = 1;
            this.systemtimeLabel.Text = "System Time";
            // 
            // fix_label10
            // 
            this.fixLabel10.AutoSize = true;
            this.fixLabel10.CausesValidation = false;
            this.fixLabel10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLabel10.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLabel10.Location = new System.Drawing.Point(6, 10);
            this.fixLabel10.Name = "fix_label10";
            this.fixLabel10.Size = new System.Drawing.Size(213, 22);
            this.fixLabel10.TabIndex = 1;
            this.fixLabel10.Text = "Real Time Clock Setting";
            // 
            // localtime_textbox
            // 
            this.localtimeTextbox.Enabled = false;
            this.localtimeTextbox.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.localtimeTextbox.Location = new System.Drawing.Point(10, 79);
            this.localtimeTextbox.Mask = "00/00/0000 00:00:00";
            this.localtimeTextbox.Name = "localtime_textbox";
            this.localtimeTextbox.Size = new System.Drawing.Size(188, 29);
            this.localtimeTextbox.TabIndex = 1;
            // 
            // clock_textbox
            // 
            this.clockTextbox.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clockTextbox.Location = new System.Drawing.Point(10, 189);
            this.clockTextbox.Mask = "00/00/0000 00:00:00";
            this.clockTextbox.Name = "clock_textbox";
            this.clockTextbox.Size = new System.Drawing.Size(188, 29);
            this.clockTextbox.TabIndex = 0;

            this.Controls.Add(this.clockTab);

            this.clockTab.ResumeLayout(false);
            this.rtcTab.ResumeLayout(false);
            this.rtcTab.PerformLayout();
        }

        private System.Windows.Forms.TabControl recordTab;
        private System.Windows.Forms.TabPage recordsettingTab;
        private System.Windows.Forms.Label fixLabel8;
        public System.Windows.Forms.ComboBox enablereadingDropdown;
        private System.Windows.Forms.Label recordintervalLabel;
        private System.Windows.Forms.Label enablereadingLabel;
        private System.Windows.Forms.Label enablesynchronizeLabel;
        public System.Windows.Forms.ComboBox enablesynchronizeDropdown;
        public System.Windows.Forms.TextBox intervalTextbox;
        private System.Windows.Forms.Button recordsettingButton;
        public System.Windows.Forms.CheckBox fastreadCheck;
        private System.Windows.Forms.Label recordonchangeLabel;
        public System.Windows.Forms.TextBox c1Textbox;
        public System.Windows.Forms.TextBox c2Textbox;
        public System.Windows.Forms.TextBox c3Textbox;
        public System.Windows.Forms.TextBox c4Textbox;
        public System.Windows.Forms.TextBox c5Textbox;
        public System.Windows.Forms.TextBox c6Textbox;
        public System.Windows.Forms.TextBox c7Textbox;
        public System.Windows.Forms.TextBox c8Textbox;
        private System.Windows.Forms.Label c1Label;
        private System.Windows.Forms.Label c2Label;
        private System.Windows.Forms.Label c3Label;
        private System.Windows.Forms.Label c4Label;
        private System.Windows.Forms.Label c5Label;
        private System.Windows.Forms.Label c6Label;
        private System.Windows.Forms.Label c7Label;
        private System.Windows.Forms.Label c8Label;
        private void InitializeRecordSettingTab()
        {
            this.recordTab = new System.Windows.Forms.TabControl();
            this.recordsettingTab = new System.Windows.Forms.TabPage();
            this.intervalTextbox = new System.Windows.Forms.TextBox();
            this.enablereadingDropdown = new System.Windows.Forms.ComboBox();
            this.recordintervalLabel = new System.Windows.Forms.Label();
            this.enablereadingLabel = new System.Windows.Forms.Label();
            this.enablesynchronizeLabel = new System.Windows.Forms.Label();
            this.enablesynchronizeDropdown = new System.Windows.Forms.ComboBox();
            this.fastreadCheck = new System.Windows.Forms.CheckBox();
            this.recordonchangeLabel = new System.Windows.Forms.Label();
            this.c1Textbox = new System.Windows.Forms.TextBox();
            this.c2Textbox = new System.Windows.Forms.TextBox();
            this.c3Textbox = new System.Windows.Forms.TextBox();
            this.c4Textbox = new System.Windows.Forms.TextBox();
            this.c5Textbox = new System.Windows.Forms.TextBox();
            this.c6Textbox = new System.Windows.Forms.TextBox();
            this.c7Textbox = new System.Windows.Forms.TextBox();
            this.c8Textbox = new System.Windows.Forms.TextBox();
            this.c1Label = new System.Windows.Forms.Label();
            this.c2Label = new System.Windows.Forms.Label();
            this.c3Label = new System.Windows.Forms.Label();
            this.c4Label = new System.Windows.Forms.Label();
            this.c5Label = new System.Windows.Forms.Label();
            this.c6Label = new System.Windows.Forms.Label();
            this.c7Label = new System.Windows.Forms.Label();
            this.c8Label = new System.Windows.Forms.Label();

            this.recordsettingButton = new System.Windows.Forms.Button();
            this.fixLabel8 = new System.Windows.Forms.Label();
            this.recordTab.SuspendLayout();
            this.recordsettingTab.SuspendLayout();

            // 
            // fix_label8
            // 
            this.fixLabel8.AutoSize = true;
            this.fixLabel8.CausesValidation = false;
            this.fixLabel8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLabel8.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLabel8.Location = new System.Drawing.Point(20, 10);
            this.fixLabel8.Name = "fix_label8";
            this.fixLabel8.Size = new System.Drawing.Size(137, 22);
            this.fixLabel8.TabIndex = 1;
            this.fixLabel8.Text = "Record Setting";
            // 
            // record_tab
            // 
            this.recordTab.Controls.Add(this.recordsettingTab);
            this.recordTab.Font = new System.Drawing.Font("Arial", 11.25F);
            this.recordTab.Location = new System.Drawing.Point(402, 27);
            this.recordTab.Name = "record_tab";
            this.recordTab.SelectedIndex = 0;
            this.recordTab.Size = new System.Drawing.Size(400, 415);
            this.recordTab.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.recordTab.TabIndex = 3;
            this.recordTab.Visible = false;
            // 
            // recordsetting_tab
            // 
            this.recordsettingTab.Controls.Add(this.recordsettingButton);
            this.recordsettingTab.Controls.Add(this.intervalTextbox);
            this.recordsettingTab.Controls.Add(this.enablereadingDropdown);
            this.recordsettingTab.Controls.Add(this.recordintervalLabel);
            this.recordsettingTab.Controls.Add(this.enablereadingLabel);
            this.recordsettingTab.Controls.Add(this.fixLabel8);
            this.recordsettingTab.Controls.Add(this.recordonchangeLabel);
            this.recordsettingTab.Controls.Add(this.enablesynchronizeLabel);
            this.recordsettingTab.Controls.Add(this.fastreadCheck);
            this.recordsettingTab.Controls.Add(this.enablesynchronizeDropdown);
            this.recordsettingTab.Controls.Add(this.c1Textbox);
            this.recordsettingTab.Controls.Add(this.c2Textbox);
            this.recordsettingTab.Controls.Add(this.c3Textbox);
            this.recordsettingTab.Controls.Add(this.c4Textbox);
            this.recordsettingTab.Controls.Add(this.c5Textbox);
            this.recordsettingTab.Controls.Add(this.c6Textbox);
            this.recordsettingTab.Controls.Add(this.c7Textbox);
            this.recordsettingTab.Controls.Add(this.c8Textbox);
            this.recordsettingTab.Controls.Add(this.c1Label);
            this.recordsettingTab.Controls.Add(this.c2Label);
            this.recordsettingTab.Controls.Add(this.c3Label);
            this.recordsettingTab.Controls.Add(this.c4Label);
            this.recordsettingTab.Controls.Add(this.c5Label);
            this.recordsettingTab.Controls.Add(this.c6Label);
            this.recordsettingTab.Controls.Add(this.c7Label);
            this.recordsettingTab.Controls.Add(this.c8Label);
            this.recordsettingTab.Location = new System.Drawing.Point(4, 26);
            this.recordsettingTab.Name = "recordsetting_tab";
            this.recordsettingTab.Padding = new System.Windows.Forms.Padding(3);
            this.recordsettingTab.Size = new System.Drawing.Size(392, 385);
            this.recordsettingTab.TabIndex = 1;
            this.recordsettingTab.Text = "Record Setting";
            this.recordsettingTab.UseVisualStyleBackColor = true;
            // 
            // interval_textbox
            // 
            this.intervalTextbox.Location = new System.Drawing.Point(174, 114);
            this.intervalTextbox.Name = "interval_textbox";
            this.intervalTextbox.Size = new System.Drawing.Size(121, 25);
            this.intervalTextbox.TabIndex = 3;
            // 
            // enablereading_dropdown
            // 
            this.enablereadingDropdown.FormattingEnabled = true;
            this.enablereadingDropdown.Items.AddRange(new object[] {
            "Disable",
            "Enable"});
            this.enablereadingDropdown.Location = new System.Drawing.Point(174, 42);
            this.enablereadingDropdown.Name = "enablereading_dropdown";
            this.enablereadingDropdown.Size = new System.Drawing.Size(121, 25);
            this.enablereadingDropdown.TabIndex = 1;
            // 
            // recordinterval_label
            // 
            this.recordintervalLabel.AutoSize = true;
            this.recordintervalLabel.CausesValidation = false;
            this.recordintervalLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.recordintervalLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recordintervalLabel.Location = new System.Drawing.Point(21, 115);
            this.recordintervalLabel.Name = "recordinterval_label";
            this.recordintervalLabel.Size = new System.Drawing.Size(146, 18);
            this.recordintervalLabel.TabIndex = 0;
            this.recordintervalLabel.Text = "Record Interval (s)";
            // 
            // enablereading_label
            // 
            this.enablereadingLabel.AutoSize = true;
            this.enablereadingLabel.CausesValidation = false;
            this.enablereadingLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.enablereadingLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enablereadingLabel.Location = new System.Drawing.Point(21, 45);
            this.enablereadingLabel.Name = "enablereading_label";
            this.enablereadingLabel.Size = new System.Drawing.Size(133, 18);
            this.enablereadingLabel.TabIndex = 0;
            this.enablereadingLabel.Text = "Enable Recording";
            // 
            // enablesynchronizeLabel
            // 
            this.enablesynchronizeLabel.AutoSize = true;
            this.enablesynchronizeLabel.CausesValidation = false;
            this.enablesynchronizeLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.enablesynchronizeLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enablesynchronizeLabel.Location = new System.Drawing.Point(21, 80);
            this.enablesynchronizeLabel.Name = "enablesynchronize_label";
            this.enablesynchronizeLabel.Size = new System.Drawing.Size(133, 18);
            this.enablesynchronizeLabel.TabIndex = 0;
            this.enablesynchronizeLabel.Text = "Enable Synchronize";
            // 
            // enablesynchronizeDropdown
            // 
            this.enablesynchronizeDropdown.FormattingEnabled = true;
            this.enablesynchronizeDropdown.Items.AddRange(new object[] {
            "Disable",
            "Enable"});
            this.enablesynchronizeDropdown.Location = new System.Drawing.Point(174, 77);
            this.enablesynchronizeDropdown.Name = "enablesynchronize_dropdown";
            this.enablesynchronizeDropdown.Size = new System.Drawing.Size(121, 25);
            this.enablesynchronizeDropdown.TabIndex = 2;
            // 
            // fastreadCheck
            // 
            this.fastreadCheck.AutoSize = true;
            this.fastreadCheck.Location = new System.Drawing.Point(302, 116);
            this.fastreadCheck.Name = "fastreadCheck";
            this.fastreadCheck.Size = new System.Drawing.Size(92, 21);
            this.fastreadCheck.TabIndex = 13;
            this.fastreadCheck.Text = "Fast Read";
            this.fastreadCheck.UseVisualStyleBackColor = true;
            this.fastreadCheck.Click += new System.EventHandler(Software.recordsettingTab.FastRead_CheckBox_Click);
            // 
            // 
            // recordonchange_label
            // 
            this.recordonchangeLabel.AutoSize = true;
            this.recordonchangeLabel.CausesValidation = false;
            this.recordonchangeLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.recordonchangeLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recordonchangeLabel.Location = new System.Drawing.Point(21, 155);
            this.recordonchangeLabel.Name = "recordonchange_label";
            this.recordonchangeLabel.Size = new System.Drawing.Size(133, 18);
            this.recordonchangeLabel.TabIndex = 0;
            this.recordonchangeLabel.Text = "Record On Change Value";
            // 
            // c1_textbox
            // 
            this.c1Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1Textbox.Location = new System.Drawing.Point(80, 200);
            this.c1Textbox.Name = "c1_textbox";
            this.c1Textbox.Size = new System.Drawing.Size(116, 22);
            this.c1Textbox.TabIndex = 39;
            this.c1Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // c2_textbox
            // 
            this.c2Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c2Textbox.Location = new System.Drawing.Point(80, 230);
            this.c2Textbox.Name = "c2_textbox";
            this.c2Textbox.Size = new System.Drawing.Size(116, 22);
            this.c2Textbox.TabIndex = 40;
            this.c2Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // c3_textbox
            // 
            this.c3Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c3Textbox.Location = new System.Drawing.Point(80, 260);
            this.c3Textbox.Name = "c3_textbox";
            this.c3Textbox.Size = new System.Drawing.Size(116, 22);
            this.c3Textbox.TabIndex = 41;
            this.c3Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // c4_textbox
            // 
            this.c4Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c4Textbox.Location = new System.Drawing.Point(80, 290);
            this.c4Textbox.Name = "c4_textbox";
            this.c4Textbox.Size = new System.Drawing.Size(116, 22);
            this.c4Textbox.TabIndex = 42;
            this.c4Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // c5_textbox
            // 
            this.c5Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c5Textbox.Location = new System.Drawing.Point(270, 200);
            this.c5Textbox.Name = "c5_textbox";
            this.c5Textbox.Size = new System.Drawing.Size(116, 22);
            this.c5Textbox.TabIndex = 43;
            this.c5Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // c6_textbox
            // 
            this.c6Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c6Textbox.Location = new System.Drawing.Point(270, 230);
            this.c6Textbox.Name = "c6_textbox";
            this.c6Textbox.Size = new System.Drawing.Size(116, 22);
            this.c6Textbox.TabIndex = 41;
            this.c6Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // c7_textbox
            // 
            this.c7Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c7Textbox.Location = new System.Drawing.Point(270, 260);
            this.c7Textbox.Name = "c7_textbox";
            this.c7Textbox.Size = new System.Drawing.Size(116, 22);
            this.c7Textbox.TabIndex = 42;
            this.c7Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // c8_textbox
            // 
            this.c8Textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c8Textbox.Location = new System.Drawing.Point(270, 290);
            this.c8Textbox.Name = "c8_textbox";
            this.c8Textbox.Size = new System.Drawing.Size(116, 22);
            this.c8Textbox.TabIndex = 43;
            this.c8Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // c1_label
            // 
            this.c1Label.AutoSize = true;
            this.c1Label.CausesValidation = false;
            this.c1Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.c1Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1Label.Location = new System.Drawing.Point(10, 202);
            this.c1Label.Name = "c1_label";
            this.c1Label.Size = new System.Drawing.Size(31, 18);
            this.c1Label.TabIndex = 38;
            this.c1Label.Text = "Channel 1";
            // 
            // c2_label
            // 
            this.c2Label.AutoSize = true;
            this.c2Label.CausesValidation = false;
            this.c2Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.c2Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c2Label.Location = new System.Drawing.Point(10, 232);
            this.c2Label.Name = "c2_label";
            this.c2Label.Size = new System.Drawing.Size(31, 18);
            this.c2Label.TabIndex = 37;
            this.c2Label.Text = "Channel 2";
            // 
            // c3_label
            // 
            this.c3Label.AutoSize = true;
            this.c3Label.CausesValidation = false;
            this.c3Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.c3Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c3Label.Location = new System.Drawing.Point(10, 262);
            this.c3Label.Name = "c3_label";
            this.c3Label.Size = new System.Drawing.Size(31, 18);
            this.c3Label.TabIndex = 36;
            this.c3Label.Text = "Channel 3";
            // 
            // c4_label
            // 
            this.c4Label.AutoSize = true;
            this.c4Label.CausesValidation = false;
            this.c4Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.c4Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c4Label.Location = new System.Drawing.Point(10, 292);
            this.c4Label.Name = "c4_label";
            this.c4Label.Size = new System.Drawing.Size(31, 18);
            this.c4Label.TabIndex = 35;
            this.c4Label.Text = "Channel 4";
            // 
            // c5_label
            // 
            this.c5Label.AutoSize = true;
            this.c5Label.CausesValidation = false;
            this.c5Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.c5Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c5Label.Location = new System.Drawing.Point(200, 202);
            this.c5Label.Name = "c5_label";
            this.c5Label.Size = new System.Drawing.Size(31, 18);
            this.c5Label.TabIndex = 34;
            this.c5Label.Text = "Channel 5";
            // 
            // c6_label
            // 
            this.c6Label.AutoSize = true;
            this.c6Label.CausesValidation = false;
            this.c6Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.c6Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c6Label.Location = new System.Drawing.Point(200, 232);
            this.c6Label.Name = "c6_label";
            this.c6Label.Size = new System.Drawing.Size(31, 18);
            this.c6Label.TabIndex = 34;
            this.c6Label.Text = "Channel 6";
            // 
            // c7_label
            // 
            this.c7Label.AutoSize = true;
            this.c7Label.CausesValidation = false;
            this.c7Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.c7Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c7Label.Location = new System.Drawing.Point(200, 262);
            this.c7Label.Name = "c7_label";
            this.c7Label.Size = new System.Drawing.Size(31, 18);
            this.c7Label.TabIndex = 34;
            this.c7Label.Text = "Channel 7";
            // 
            // c8_label
            // 
            this.c8Label.AutoSize = true;
            this.c8Label.CausesValidation = false;
            this.c8Label.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.c8Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c8Label.Location = new System.Drawing.Point(200, 292);
            this.c8Label.Name = "c8_label";
            this.c8Label.Size = new System.Drawing.Size(31, 18);
            this.c8Label.TabIndex = 34;
            this.c8Label.Text = "Channel 8";
            // 
            // recordsetting_button
            // 
            this.recordsettingButton.Location = new System.Drawing.Point(251, 333);
            this.recordsettingButton.Name = "recordsetting_button";
            this.recordsettingButton.Size = new System.Drawing.Size(126, 37);
            this.recordsettingButton.TabIndex = 5;
            this.recordsettingButton.Text = "Confirm";
            this.recordsettingButton.UseVisualStyleBackColor = true;
            this.recordsettingButton.Click += new System.EventHandler(Software.recordsettingTab.recordsetting_button_Click);

            this.Controls.Add(this.recordTab);

            this.recordTab.Visible = false;

            this.recordTab.ResumeLayout(false);
            this.recordsettingTab.ResumeLayout(false);
            this.recordsettingTab.PerformLayout();
        }

        private System.Windows.Forms.TabControl downloadTab;
        private System.Windows.Forms.TabPage menudownloadTab;
        private System.Windows.Forms.Button menuButton;
        public System.Windows.Forms.Button menuresetButton;
        private System.Windows.Forms.Button checkPointButton;
        private System.Windows.Forms.Label fixLabel12;
        public System.Windows.Forms.CheckBox rawCheck;
        public System.Windows.Forms.CheckBox realCheck;
        public System.Windows.Forms.FlowLayoutPanel menuPanel;
        public System.Windows.Forms.Label progressLabel;
        public System.Windows.Forms.ProgressBar downloadProgressbar;
        private void InitializeDownloadTab()
        {
            this.downloadTab = new System.Windows.Forms.TabControl();
            this.menudownloadTab = new System.Windows.Forms.TabPage();
            this.progressLabel = new System.Windows.Forms.Label();
            this.downloadProgressbar = new System.Windows.Forms.ProgressBar();
            this.menuPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.rawCheck = new System.Windows.Forms.CheckBox();
            this.realCheck = new System.Windows.Forms.CheckBox();
            this.menuButton = new System.Windows.Forms.Button();
            this.checkPointButton = new System.Windows.Forms.Button();
            this.menuresetButton = new System.Windows.Forms.Button();
            this.fixLabel12 = new System.Windows.Forms.Label();
            this.downloadTab.SuspendLayout();
            this.menudownloadTab.SuspendLayout();

            // 
            // downloadTab
            // 
            this.downloadTab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.downloadTab.Controls.Add(this.menudownloadTab);
            this.downloadTab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.downloadTab.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downloadTab.Location = new System.Drawing.Point(402, 27);
            this.downloadTab.Name = "download_tab";
            this.downloadTab.SelectedIndex = 0;
            this.downloadTab.Size = new System.Drawing.Size(400, 415);
            this.downloadTab.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.downloadTab.TabIndex = 3;
            // 
            // menudownloadTab
            // 
            this.menudownloadTab.Controls.Add(this.progressLabel);
            this.menudownloadTab.Controls.Add(this.downloadProgressbar);
            this.menudownloadTab.Controls.Add(this.menuPanel);
            this.menudownloadTab.Controls.Add(this.rawCheck);
            this.menudownloadTab.Controls.Add(this.realCheck);
            this.menudownloadTab.Controls.Add(this.menuButton);
            this.menudownloadTab.Controls.Add(this.checkPointButton);
            this.menudownloadTab.Controls.Add(this.menuresetButton);
            this.menudownloadTab.Controls.Add(this.fixLabel12);
            this.menudownloadTab.Location = new System.Drawing.Point(4, 26);
            this.menudownloadTab.Name = "menudownload_tab";
            this.menudownloadTab.Padding = new System.Windows.Forms.Padding(3);
            this.menudownloadTab.Size = new System.Drawing.Size(392, 385);
            this.menudownloadTab.TabIndex = 2;
            this.menudownloadTab.Text = "DownLoad From Menu";
            this.menudownloadTab.UseVisualStyleBackColor = true;
            // 
            // progressLabel
            // 
            this.progressLabel.AutoSize = true;
            this.progressLabel.Location = new System.Drawing.Point(355, 294);
            this.progressLabel.Name = "progress_label";
            this.progressLabel.Size = new System.Drawing.Size(12, 17);
            this.progressLabel.TabIndex = 16;
            this.progressLabel.Text = " ";
            // 
            // downloadProgressbar
            // 
            this.downloadProgressbar.Location = new System.Drawing.Point(215, 293);
            this.downloadProgressbar.Name = "download_progressbar";
            this.downloadProgressbar.Size = new System.Drawing.Size(138, 22);
            this.downloadProgressbar.TabIndex = 15;
            // 
            // menuPanel
            // 
            this.menuPanel.AutoScroll = true;
            this.menuPanel.Location = new System.Drawing.Point(20, 50);
            this.menuPanel.Name = "menu_panel";
            this.menuPanel.Size = new System.Drawing.Size(380, 239);
            this.menuPanel.TabIndex = 14;
            // 
            // rawCheck
            // 
            this.rawCheck.AutoSize = true;
            this.rawCheck.Location = new System.Drawing.Point(125, 295);
            this.rawCheck.Name = "raw_check";
            this.rawCheck.Size = new System.Drawing.Size(92, 21);
            this.rawCheck.TabIndex = 13;
            this.rawCheck.Text = "Raw Data";
            this.rawCheck.UseVisualStyleBackColor = true;
            this.rawCheck.Click += new System.EventHandler(Software.menudownloadTab.Raw_CheckBox_Click);
            // 
            // realCheck
            // 
            this.realCheck.AutoSize = true;
            this.realCheck.Location = new System.Drawing.Point(20, 295);
            this.realCheck.Name = "realCheck";
            this.realCheck.Size = new System.Drawing.Size(92, 21);
            this.realCheck.TabIndex = 12;
            this.realCheck.Text = "Real Data";
            this.realCheck.UseVisualStyleBackColor = true;
            this.realCheck.Click += new System.EventHandler(Software.menudownloadTab.Real_CheckBox_Click);
            // 
            // menuButton
            // 
            this.menuButton.Location = new System.Drawing.Point(10, 328);
            this.menuButton.Name = "menuButton";
            this.menuButton.Size = new System.Drawing.Size(110, 50);
            this.menuButton.TabIndex = 9;
            this.menuButton.Text = "Get Menu";
            this.menuButton.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuButton.UseVisualStyleBackColor = true;
            this.menuButton.Click += new System.EventHandler(Software.menudownloadTab.Getmenu_Button_Click);
            // 
            // menuresetButton
            // 
            this.menuresetButton.Location = new System.Drawing.Point(140, 328);
            this.menuresetButton.Name = "menuresetButton";
            this.menuresetButton.Size = new System.Drawing.Size(110, 50);
            this.menuresetButton.TabIndex = 10;
            this.menuresetButton.Text = "Clear Memory";
            this.menuresetButton.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuresetButton.UseVisualStyleBackColor = true;
            this.menuresetButton.Click += new System.EventHandler(Software.menudownloadTab.Resetmenu_Button_Click);
            // 
            // checkPointButton
            // 
            this.checkPointButton.Location = new System.Drawing.Point(270, 328);
            this.checkPointButton.Name = "checkPointButton";
            this.checkPointButton.Size = new System.Drawing.Size(110, 50);
            this.checkPointButton.TabIndex = 11;
            this.checkPointButton.Text = "Download from Check Point";
            this.checkPointButton.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkPointButton.UseVisualStyleBackColor = true;
            this.checkPointButton.Click += new System.EventHandler(Software.menudownloadTab.CheckPointbutton_Click);
            // 
            // fixLabel12
            // 
            this.fixLabel12.AutoSize = true;
            this.fixLabel12.CausesValidation = false;
            this.fixLabel12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLabel12.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLabel12.Location = new System.Drawing.Point(16, 10);
            this.fixLabel12.Name = "fix_label12";
            this.fixLabel12.Size = new System.Drawing.Size(202, 22);
            this.fixLabel12.TabIndex = 8;
            this.fixLabel12.Text = "DownLoad From Menu";

            this.downloadTab.Visible = false;

            this.Controls.Add(this.downloadTab);

            this.downloadTab.ResumeLayout(false);
            this.menudownloadTab.ResumeLayout(false);
            this.menudownloadTab.PerformLayout();
        }

        private System.Windows.Forms.TabControl roleTab;
        private System.Windows.Forms.TabPage roleSettingTab;
        private System.Windows.Forms.Label roleLabel;
        public System.Windows.Forms.Label fixLable1;
        public System.Windows.Forms.Label slaveListLabel;
        public System.Windows.Forms.ComboBox roleDropdown;
        public System.Windows.Forms.Label slaveModeLabel;
        public System.Windows.Forms.ComboBox slaveModeDropdown;
        public System.Windows.Forms.Label slaveNumberLabel5;
        public System.Windows.Forms.Label slaveNumberLabel4;
        public System.Windows.Forms.Label slaveNumberLabel3;
        public System.Windows.Forms.Label slaveNumberLabel2;
        public System.Windows.Forms.Label slaveNumberLabel1;
        public System.Windows.Forms.TextBox slaveNumberTextbox5;
        public System.Windows.Forms.TextBox slaveNumberTextbox4;
        public System.Windows.Forms.TextBox slaveNumberTextbox3;
        public System.Windows.Forms.TextBox slaveNumberTextbox2;
        public System.Windows.Forms.TextBox slaveNumberTextbox1;
        public System.Windows.Forms.ComboBox slaveNumberDropdown;
        public System.Windows.Forms.Label slaveNumberLabel;
        public System.Windows.Forms.Button rolePushButton;
        private void InitializeRoleTab()
        {
            this.roleTab = new System.Windows.Forms.TabControl();
            this.roleSettingTab = new System.Windows.Forms.TabPage();
            this.slaveListLabel = new System.Windows.Forms.Label();
            this.roleDropdown = new System.Windows.Forms.ComboBox();
            this.roleLabel = new System.Windows.Forms.Label();
            this.slaveModeDropdown = new System.Windows.Forms.ComboBox();
            this.slaveModeLabel = new System.Windows.Forms.Label();
            this.fixLable1 = new System.Windows.Forms.Label();
            this.slaveNumberLabel1 = new System.Windows.Forms.Label();
            this.slaveNumberLabel2 = new System.Windows.Forms.Label();
            this.slaveNumberLabel3 = new System.Windows.Forms.Label();
            this.slaveNumberLabel4 = new System.Windows.Forms.Label();
            this.slaveNumberLabel5 = new System.Windows.Forms.Label();
            this.slaveNumberTextbox1 = new System.Windows.Forms.TextBox();
            this.slaveNumberTextbox2 = new System.Windows.Forms.TextBox();
            this.slaveNumberTextbox3 = new System.Windows.Forms.TextBox();
            this.slaveNumberTextbox4 = new System.Windows.Forms.TextBox();
            this.slaveNumberTextbox5 = new System.Windows.Forms.TextBox();
            this.slaveNumberLabel = new System.Windows.Forms.Label();
            this.slaveNumberDropdown = new System.Windows.Forms.ComboBox();
            this.rolePushButton = new System.Windows.Forms.Button();

            this.roleTab.SuspendLayout();
            this.roleSettingTab.SuspendLayout();

            // 
            // roleTab
            // 
            this.roleTab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.roleTab.Controls.Add(this.roleSettingTab);
            this.roleTab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.roleTab.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roleTab.Location = new System.Drawing.Point(402, 27);
            this.roleTab.Name = "roleTab";
            this.roleTab.SelectedIndex = 0;
            this.roleTab.Size = new System.Drawing.Size(400, 415);
            this.roleTab.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.roleTab.TabIndex = 5;
            // 
            // roleSettingTab
            // 
            this.roleSettingTab.Controls.Add(this.slaveNumberDropdown);
            this.roleSettingTab.Controls.Add(this.slaveNumberLabel);
            this.roleSettingTab.Controls.Add(this.slaveNumberTextbox5);
            this.roleSettingTab.Controls.Add(this.slaveNumberTextbox4);
            this.roleSettingTab.Controls.Add(this.slaveNumberTextbox3);
            this.roleSettingTab.Controls.Add(this.slaveNumberTextbox2);
            this.roleSettingTab.Controls.Add(this.slaveNumberTextbox1);
            this.roleSettingTab.Controls.Add(this.slaveNumberLabel5);
            this.roleSettingTab.Controls.Add(this.slaveNumberLabel4);
            this.roleSettingTab.Controls.Add(this.slaveNumberLabel3);
            this.roleSettingTab.Controls.Add(this.slaveNumberLabel2);
            this.roleSettingTab.Controls.Add(this.slaveNumberLabel1);
            this.roleSettingTab.Controls.Add(this.slaveListLabel);
            this.roleSettingTab.Controls.Add(this.roleDropdown);
            this.roleSettingTab.Controls.Add(this.roleLabel);
            this.roleSettingTab.Controls.Add(this.slaveModeDropdown);
            this.roleSettingTab.Controls.Add(this.slaveModeLabel);
            this.roleSettingTab.Controls.Add(this.fixLable1);
            this.roleSettingTab.Controls.Add(this.rolePushButton);
            this.roleSettingTab.Location = new System.Drawing.Point(4, 26);
            this.roleSettingTab.Name = "roleSettingTab";
            this.roleSettingTab.Padding = new System.Windows.Forms.Padding(3);
            this.roleSettingTab.Size = new System.Drawing.Size(392, 385);
            this.roleSettingTab.TabIndex = 0;
            this.roleSettingTab.Text = "Role Setting";
            this.roleSettingTab.UseVisualStyleBackColor = true;
            // 
            // slaveListLabel
            // 
            this.slaveListLabel.AutoSize = true;
            this.slaveListLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveListLabel.Location = new System.Drawing.Point(25, 125);
            this.slaveListLabel.Name = "slaveListLabel";
            this.slaveListLabel.Size = new System.Drawing.Size(83, 19);
            this.slaveListLabel.TabIndex = 0;
            this.slaveListLabel.Text = "Slave List";
            // 
            // roleDropdown
            // 
            this.roleDropdown.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roleDropdown.FormattingEnabled = true;
            this.roleDropdown.Items.AddRange(new object[] {
            "Master",
            "Slave"});
            this.roleDropdown.Location = new System.Drawing.Point(138, 45);
            this.roleDropdown.Name = "roleDropdown";
            this.roleDropdown.Size = new System.Drawing.Size(134, 26);
            this.roleDropdown.TabIndex = 1;
            this.roleDropdown.SelectedIndexChanged += new System.EventHandler(Software.roleTab.Role_Dropdown_SelectedIndexChanged);
            // 
            // roleLabel
            // 
            this.roleLabel.AutoSize = true;
            this.roleLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roleLabel.Location = new System.Drawing.Point(26, 48);
            this.roleLabel.Name = "roleLabel";
            this.roleLabel.Size = new System.Drawing.Size(94, 18);
            this.roleLabel.TabIndex = 0;
            this.roleLabel.Text = "Logger Role";
            // 
            // slaveModeDropdown
            // 
            this.slaveModeDropdown.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveModeDropdown.FormattingEnabled = true;
            this.slaveModeDropdown.Items.AddRange(new object[] {
            "RS485",
            "Radio",
            "Disable"});
            this.slaveModeDropdown.Location = new System.Drawing.Point(138, 85);
            this.slaveModeDropdown.Name = "slaveModeDropdown";
            this.slaveModeDropdown.Size = new System.Drawing.Size(134, 26);
            this.slaveModeDropdown.TabIndex = 1;
            this.slaveModeDropdown.SelectedIndexChanged += new System.EventHandler(Software.roleTab.Role_Dropdown_SelectedIndexChanged);
            // 
            // slavemodeLabel
            // 
            this.slaveModeLabel.AutoSize = true;
            this.slaveModeLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveModeLabel.Location = new System.Drawing.Point(26, 88);
            this.slaveModeLabel.Name = "slaveModeLabel";
            this.slaveModeLabel.Size = new System.Drawing.Size(94, 18);
            this.slaveModeLabel.TabIndex = 0;
            this.slaveModeLabel.Text = "Slave Mode";
            // 
            // fixLable1
            // 
            this.fixLable1.AutoSize = true;
            this.fixLable1.CausesValidation = false;
            this.fixLable1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixLable1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixLable1.Location = new System.Drawing.Point(6, 10);
            this.fixLable1.Name = "fixLable1";
            this.fixLable1.Size = new System.Drawing.Size(124, 22);
            this.fixLable1.TabIndex = 0;
            this.fixLable1.Text = "Role Settings";
            // 
            // slaveNumberLabel1
            // 
            this.slaveNumberLabel1.AutoSize = true;
            this.slaveNumberLabel1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberLabel1.Location = new System.Drawing.Point(26, 155);
            this.slaveNumberLabel1.Name = "slaveNumberLabel1";
            this.slaveNumberLabel1.Size = new System.Drawing.Size(119, 18);
            this.slaveNumberLabel1.TabIndex = 0;
            this.slaveNumberLabel1.Text = "Slave Number 1";
            // 
            // slaveNumberLabel2
            // 
            this.slaveNumberLabel2.AutoSize = true;
            this.slaveNumberLabel2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberLabel2.Location = new System.Drawing.Point(26, 185);
            this.slaveNumberLabel2.Name = "slaveNumberLabel2";
            this.slaveNumberLabel2.Size = new System.Drawing.Size(119, 18);
            this.slaveNumberLabel2.TabIndex = 0;
            this.slaveNumberLabel2.Text = "Slave Number 2";
            // 
            // slaveNumberLabel3
            // 
            this.slaveNumberLabel3.AutoSize = true;
            this.slaveNumberLabel3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberLabel3.Location = new System.Drawing.Point(26, 215);
            this.slaveNumberLabel3.Name = "slaveNumberLabel3";
            this.slaveNumberLabel3.Size = new System.Drawing.Size(119, 18);
            this.slaveNumberLabel3.TabIndex = 0;
            this.slaveNumberLabel3.Text = "Slave Number 3";
            // 
            // slaveNumberLabel4
            // 
            this.slaveNumberLabel4.AutoSize = true;
            this.slaveNumberLabel4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberLabel4.Location = new System.Drawing.Point(26, 245);
            this.slaveNumberLabel4.Name = "slaveNumberLabel4";
            this.slaveNumberLabel4.Size = new System.Drawing.Size(119, 18);
            this.slaveNumberLabel4.TabIndex = 0;
            this.slaveNumberLabel4.Text = "Slave Number 4";
            // 
            // slaveNumberLabel5
            // 
            this.slaveNumberLabel5.AutoSize = true;
            this.slaveNumberLabel5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberLabel5.Location = new System.Drawing.Point(26, 275);
            this.slaveNumberLabel5.Name = "slaveNumberLabel5";
            this.slaveNumberLabel5.Size = new System.Drawing.Size(119, 18);
            this.slaveNumberLabel5.TabIndex = 0;
            this.slaveNumberLabel5.Text = "Slave Number 5";
            // 
            // slaveNumberTextbox1
            // 
            this.slaveNumberTextbox1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberTextbox1.Location = new System.Drawing.Point(160, 150);
            this.slaveNumberTextbox1.Name = "slaveNumberTextbox1";
            this.slaveNumberTextbox1.Size = new System.Drawing.Size(100, 26);
            this.slaveNumberTextbox1.TabIndex = 2;
            this.slaveNumberTextbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // slaveNumberTextbox2
            // 
            this.slaveNumberTextbox2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberTextbox2.Location = new System.Drawing.Point(160, 180);
            this.slaveNumberTextbox2.Name = "slaveNumberTextbox2";
            this.slaveNumberTextbox2.Size = new System.Drawing.Size(100, 26);
            this.slaveNumberTextbox2.TabIndex = 3;
            this.slaveNumberTextbox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // slaveNumberTextbox3
            // 
            this.slaveNumberTextbox3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberTextbox3.Location = new System.Drawing.Point(160, 210);
            this.slaveNumberTextbox3.Name = "slaveNumberTextbox3";
            this.slaveNumberTextbox3.Size = new System.Drawing.Size(100, 26);
            this.slaveNumberTextbox3.TabIndex = 4;
            this.slaveNumberTextbox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // slaveNumberTextbox4
            // 
            this.slaveNumberTextbox4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberTextbox4.Location = new System.Drawing.Point(160, 240);
            this.slaveNumberTextbox4.Name = "slaveNumberTextbox4";
            this.slaveNumberTextbox4.Size = new System.Drawing.Size(100, 26);
            this.slaveNumberTextbox4.TabIndex = 5;
            this.slaveNumberTextbox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // slaveNumberTextbox5
            // 
            this.slaveNumberTextbox5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberTextbox5.Location = new System.Drawing.Point(160, 270);
            this.slaveNumberTextbox5.Name = "slaveNumberTextbox5";
            this.slaveNumberTextbox5.Size = new System.Drawing.Size(100, 26);
            this.slaveNumberTextbox5.TabIndex = 6;
            this.slaveNumberTextbox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // slaveNumberLabel
            // 
            this.slaveNumberLabel.AutoSize = true;
            this.slaveNumberLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberLabel.Location = new System.Drawing.Point(26, 116);
            this.slaveNumberLabel.Name = "slaveNumberLabel";
            this.slaveNumberLabel.Size = new System.Drawing.Size(106, 18);
            this.slaveNumberLabel.TabIndex = 0;
            this.slaveNumberLabel.Text = "Slave Number";
            this.slaveNumberLabel.Visible = false;
            // 
            // slaveNumberDropdown
            // 
            this.slaveNumberDropdown.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slaveNumberDropdown.FormattingEnabled = true;
            this.slaveNumberDropdown.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.slaveNumberDropdown.Location = new System.Drawing.Point(138, 113);
            this.slaveNumberDropdown.Name = "slaveNumberDropdown";
            this.slaveNumberDropdown.Size = new System.Drawing.Size(134, 26);
            this.slaveNumberDropdown.TabIndex = 0;
            this.slaveNumberDropdown.Visible = false;
            //
            // rolePushButton
            //
            this.rolePushButton.Location = new System.Drawing.Point(251, 333);
            this.rolePushButton.Name = "rolePushButton";
            this.rolePushButton.Size = new System.Drawing.Size(126, 37);
            this.rolePushButton.TabIndex = 5;
            this.rolePushButton.Text = "Confirm";
            this.rolePushButton.UseVisualStyleBackColor = true;
            this.rolePushButton.Click += new System.EventHandler(Software.roleTab.rolePushButton_Click);

            this.roleTab.Visible = false;

            this.Controls.Add(this.roleTab);

            this.roleTab.ResumeLayout(false);
            this.roleSettingTab.ResumeLayout(false);
            this.roleSettingTab.PerformLayout();
        }

        private System.Windows.Forms.TabPage radioSettingTab;
        public System.Windows.Forms.Button radioPB;
        public System.Windows.Forms.TextBox IDTextbox;
        public System.Windows.Forms.TextBox HPTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TabControl radioTab;
        private void InitializeRadioTab()
        {
            this.radioTab = new System.Windows.Forms.TabControl();
            this.radioSettingTab = new System.Windows.Forms.TabPage();
            this.radioPB = new System.Windows.Forms.Button();
            this.IDTextbox = new System.Windows.Forms.TextBox();
            this.HPTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();

            this.radioTab.SuspendLayout();
            this.radioSettingTab.SuspendLayout();

            // 
            // radioTab
            // 
            this.radioTab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radioTab.Controls.Add(this.radioSettingTab);
            this.radioTab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radioTab.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioTab.Location = new System.Drawing.Point(402, 27);
            this.radioTab.Name = "radioTab";
            this.radioTab.SelectedIndex = 0;
            this.radioTab.Size = new System.Drawing.Size(400, 415);
            this.radioTab.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.radioTab.TabIndex = 5;
            // 
            // radioSettingTab
            // 
            this.radioSettingTab.Controls.Add(this.radioPB);
            this.radioSettingTab.Controls.Add(this.IDTextbox);
            this.radioSettingTab.Controls.Add(this.HPTextbox);
            this.radioSettingTab.Controls.Add(this.label3);
            this.radioSettingTab.Controls.Add(this.label2);
            this.radioSettingTab.Controls.Add(this.label1);
            this.radioSettingTab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioSettingTab.Location = new System.Drawing.Point(4, 26);
            this.radioSettingTab.Name = "radioSettingTab";
            this.radioSettingTab.Padding = new System.Windows.Forms.Padding(3);
            this.radioSettingTab.Size = new System.Drawing.Size(392, 385);
            this.radioSettingTab.TabIndex = 0;
            this.radioSettingTab.Text = "Radio Setting";
            this.radioSettingTab.UseVisualStyleBackColor = true;
            // 
            // radioPB
            // 
            this.radioPB.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioPB.Location = new System.Drawing.Point(259, 339);
            this.radioPB.Name = "radioPB";
            this.radioPB.Size = new System.Drawing.Size(126, 39);
            this.radioPB.TabIndex = 5;
            this.radioPB.Text = "Push";
            this.radioPB.UseVisualStyleBackColor = true;
            this.radioPB.Click += new System.EventHandler(Software.radioTab.radioPB_button_Click);
            // 
            // IDTextbox
            // 
            this.IDTextbox.BackColor = System.Drawing.SystemColors.HighlightText;
            this.IDTextbox.Cursor = System.Windows.Forms.Cursors.Default;
            this.IDTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDTextbox.Location = new System.Drawing.Point(109, 132);
            this.IDTextbox.Name = "IDTextbox";
            this.IDTextbox.Size = new System.Drawing.Size(160, 26);
            this.IDTextbox.TabIndex = 4;
            this.IDTextbox.TabStop = false;
            this.IDTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // HPTextbox
            // 
            this.HPTextbox.BackColor = System.Drawing.SystemColors.HighlightText;
            this.HPTextbox.Cursor = System.Windows.Forms.Cursors.Default;
            this.HPTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPTextbox.Location = new System.Drawing.Point(109, 76);
            this.HPTextbox.Name = "HPTextbox";
            this.HPTextbox.Size = new System.Drawing.Size(160, 26);
            this.HPTextbox.TabIndex = 1;
            this.HPTextbox.TabStop = false;
            this.HPTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.CausesValidation = false;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Network ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.CausesValidation = false;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Preamble ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.CausesValidation = false;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 22);
            this.label1.TabIndex = 2;
            this.label1.Text = "Radio Setting";

            this.radioTab.Visible = false;

            this.Controls.Add(this.radioTab);

            this.radioTab.ResumeLayout(false);
            this.radioSettingTab.ResumeLayout(false);
            this.radioSettingTab.PerformLayout();
        }

        private System.Windows.Forms.TabControl systemTab;
        private System.Windows.Forms.TabPage systemSettingTab;
        private System.Windows.Forms.Label modeLabel;
        private System.Windows.Forms.Label cpLabel;
        private System.Windows.Forms.Label lsLabel;
        private System.Windows.Forms.Label lbLabel;
        private System.Windows.Forms.Label fvLabel;
        private System.Windows.Forms.Label snLabel;
        private System.Windows.Forms.Label systemLabel;
        public System.Windows.Forms.MaskedTextBox cpDateTextbox;
        public System.Windows.Forms.MaskedTextBox lsDateTextbox;
        public System.Windows.Forms.TextBox fvTextbox;
        public System.Windows.Forms.TextBox snTextbox;
        public System.Windows.Forms.ComboBox cmDropdown;
        public System.Windows.Forms.Button systemPushButton;
        private void InitializeSystemTab()
        {
            this.systemTab = new System.Windows.Forms.TabControl();
            this.systemSettingTab = new System.Windows.Forms.TabPage();
            this.systemPushButton = new System.Windows.Forms.Button();
            this.cmDropdown = new System.Windows.Forms.ComboBox();
            this.fvTextbox = new System.Windows.Forms.TextBox();
            this.snTextbox = new System.Windows.Forms.TextBox();
            this.cpDateTextbox = new System.Windows.Forms.MaskedTextBox();
            this.lsDateTextbox = new System.Windows.Forms.MaskedTextBox();
            this.modeLabel = new System.Windows.Forms.Label();
            this.cpLabel = new System.Windows.Forms.Label();
            this.lsLabel = new System.Windows.Forms.Label();
            this.lbLabel = new System.Windows.Forms.Label();
            this.fvLabel = new System.Windows.Forms.Label();
            this.snLabel = new System.Windows.Forms.Label();
            this.systemLabel = new System.Windows.Forms.Label();
            this.lbDateTextbox = new System.Windows.Forms.MaskedTextBox();

            this.systemTab.SuspendLayout();
            this.systemSettingTab.SuspendLayout();
            // 
            // systemTab
            // 
            this.systemTab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.systemTab.Controls.Add(this.systemSettingTab);
            this.systemTab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.systemTab.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.systemTab.Location = new System.Drawing.Point(402, 27);
            this.systemTab.Name = "systemTab";
            this.systemTab.SelectedIndex = 0;
            this.systemTab.Size = new System.Drawing.Size(400, 415);
            this.systemTab.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.systemTab.TabIndex = 5;
            // 
            // systemSettingTab
            // 
            this.systemSettingTab.Controls.Add(this.systemPushButton);
            this.systemSettingTab.Controls.Add(this.cmDropdown);
            this.systemSettingTab.Controls.Add(this.fvTextbox);
            this.systemSettingTab.Controls.Add(this.snTextbox);
            this.systemSettingTab.Controls.Add(this.cpDateTextbox);
            this.systemSettingTab.Controls.Add(this.lsDateTextbox);
            this.systemSettingTab.Controls.Add(this.modeLabel);
            this.systemSettingTab.Controls.Add(this.cpLabel);
            this.systemSettingTab.Controls.Add(this.lsLabel);
            this.systemSettingTab.Controls.Add(this.lbLabel);
            this.systemSettingTab.Controls.Add(this.fvLabel);
            this.systemSettingTab.Controls.Add(this.snLabel);
            this.systemSettingTab.Controls.Add(this.systemLabel);
            this.systemSettingTab.Controls.Add(this.lbDateTextbox);
            this.systemSettingTab.Location = new System.Drawing.Point(4, 26);
            this.systemSettingTab.Name = "systemSettingTab";
            this.systemSettingTab.Padding = new System.Windows.Forms.Padding(3);
            this.systemSettingTab.Size = new System.Drawing.Size(392, 385);
            this.systemSettingTab.TabIndex = 3;
            this.systemSettingTab.Text = "System Settings";
            this.systemSettingTab.UseVisualStyleBackColor = true;
            // 
            // systemPushButton
            // 
            this.systemPushButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.systemPushButton.Location = new System.Drawing.Point(272, 336);
            this.systemPushButton.Name = "systemPushButton";
            this.systemPushButton.Size = new System.Drawing.Size(105, 37);
            this.systemPushButton.TabIndex = 1;
            this.systemPushButton.Text = "Push";
            this.systemPushButton.UseVisualStyleBackColor = true;
            this.systemPushButton.Click += new System.EventHandler(Software.systemTab.systemPushButton_Click);
            // 
            // cmDropdown
            // 
            this.cmDropdown.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmDropdown.FormattingEnabled = true;
            this.cmDropdown.Items.AddRange(new object[] {
            "USB",
            "RS232",
            "RS485",
            "Radio"});
            this.cmDropdown.Location = new System.Drawing.Point(188, 275);
            this.cmDropdown.Name = "cmDropdown";
            this.cmDropdown.Size = new System.Drawing.Size(121, 26);
            this.cmDropdown.TabIndex = 8;
            // 
            // fvTextbox
            // 
            this.fvTextbox.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fvTextbox.Location = new System.Drawing.Point(188, 113);
            this.fvTextbox.Name = "fvTextbox";
            this.fvTextbox.Size = new System.Drawing.Size(188, 29);
            this.fvTextbox.TabIndex = 7;
            // 
            // snTextbox
            // 
            this.snTextbox.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.snTextbox.Location = new System.Drawing.Point(188, 75);
            this.snTextbox.Name = "snTextbox";
            this.snTextbox.Size = new System.Drawing.Size(188, 29);
            this.snTextbox.TabIndex = 6;
            // 
            // cpDateTextbox
            // 
            this.cpDateTextbox.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpDateTextbox.Location = new System.Drawing.Point(188, 234);
            this.cpDateTextbox.Mask = "00/00/0000 00:00:00";
            this.cpDateTextbox.Name = "cpDateTextbox";
            this.cpDateTextbox.Size = new System.Drawing.Size(188, 29);
            this.cpDateTextbox.TabIndex = 5;
            // 
            // lsDateTextbox
            // 
            this.lsDateTextbox.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsDateTextbox.Location = new System.Drawing.Point(188, 194);
            this.lsDateTextbox.Mask = "00/00/0000 00:00:00";
            this.lsDateTextbox.Name = "lsDateTextbox";
            this.lsDateTextbox.Size = new System.Drawing.Size(188, 29);
            this.lsDateTextbox.TabIndex = 4;
            // 
            // modeLabel
            // 
            this.modeLabel.AutoSize = true;
            this.modeLabel.CausesValidation = false;
            this.modeLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.modeLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modeLabel.Location = new System.Drawing.Point(10, 280);
            this.modeLabel.Name = "modeLabel";
            this.modeLabel.Size = new System.Drawing.Size(131, 18);
            this.modeLabel.TabIndex = 3;
            this.modeLabel.Text = "Connection Mode";
            // 
            // cpLabel
            // 
            this.cpLabel.AutoSize = true;
            this.cpLabel.CausesValidation = false;
            this.cpLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpLabel.Location = new System.Drawing.Point(10, 240);
            this.cpLabel.Name = "cpLabel";
            this.cpLabel.Size = new System.Drawing.Size(131, 18);
            this.cpLabel.TabIndex = 1;
            this.cpLabel.Text = "Check Point Date";
            // 
            // lsLabel
            // 
            this.lsLabel.AutoSize = true;
            this.lsLabel.CausesValidation = false;
            this.lsLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lsLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsLabel.Location = new System.Drawing.Point(10, 200);
            this.lsLabel.Name = "lsLabel";
            this.lsLabel.Size = new System.Drawing.Size(129, 18);
            this.lsLabel.TabIndex = 1;
            this.lsLabel.Text = "Last Setting Date";
            // 
            // lbLabel
            // 
            this.lbLabel.AutoSize = true;
            this.lbLabel.CausesValidation = false;
            this.lbLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLabel.Location = new System.Drawing.Point(10, 160);
            this.lbLabel.Name = "lbLabel";
            this.lbLabel.Size = new System.Drawing.Size(115, 18);
            this.lbLabel.TabIndex = 1;
            this.lbLabel.Text = "Last Build Date";
            // 
            // fvLabel
            // 
            this.fvLabel.AutoSize = true;
            this.fvLabel.CausesValidation = false;
            this.fvLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fvLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fvLabel.Location = new System.Drawing.Point(10, 120);
            this.fvLabel.Name = "fvLabel";
            this.fvLabel.Size = new System.Drawing.Size(131, 18);
            this.fvLabel.TabIndex = 1;
            this.fvLabel.Text = "Firmware Version";
            // 
            // snLabel
            // 
            this.snLabel.AutoSize = true;
            this.snLabel.CausesValidation = false;
            this.snLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.snLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.snLabel.Location = new System.Drawing.Point(10, 80);
            this.snLabel.Name = "snLabel";
            this.snLabel.Size = new System.Drawing.Size(108, 18);
            this.snLabel.TabIndex = 1;
            this.snLabel.Text = "Serial Number";
            // 
            // systemLabel
            // 
            this.systemLabel.AutoSize = true;
            this.systemLabel.CausesValidation = false;
            this.systemLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.systemLabel.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.systemLabel.Location = new System.Drawing.Point(6, 10);
            this.systemLabel.Name = "systemLabel";
            this.systemLabel.Size = new System.Drawing.Size(148, 22);
            this.systemLabel.TabIndex = 1;
            this.systemLabel.Text = "System Settings";
            // 
            // lbDateTextbox
            // 
            this.lbDateTextbox.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDateTextbox.Location = new System.Drawing.Point(188, 154);
            this.lbDateTextbox.Mask = "00/00/0000 00:00:00";
            this.lbDateTextbox.Name = "lbDateTextbox";
            this.lbDateTextbox.Size = new System.Drawing.Size(188, 29);
            this.lbDateTextbox.TabIndex = 1;

            this.systemTab.Visible = false;

            this.Controls.Add(this.systemTab);

            this.systemTab.ResumeLayout(false);
            this.systemSettingTab.ResumeLayout(false);
            this.systemSettingTab.PerformLayout();
        }
        #endregion

        private System.Windows.Forms.ToolStripMenuItem downloadDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem realTimeDataToolStripMenuItem;
        private System.Windows.Forms.Button radioButton;
        private System.Windows.Forms.ToolStripMenuItem masterLoggerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uSBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rS232CableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem radioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem slaveLoggerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rS232CableToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem modemToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem enableCoreSettingsToolStripMenuItem;
    }
}

