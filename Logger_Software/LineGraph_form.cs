﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using LoggerSoftware.Core;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace LoggerSoftware
{
    public partial class LineGraph_form : Form
    {
        private int currentX = 0;   // Current X cord

        private int chan = 0;   // Which channel is ploting

         /* ************************************************************
         * Description : Initialize the Line Graph
         * Input : title what is plotting
         *         channel which channel is plotting
         * Output : void
         * ************************************************************
         */
        public LineGraph_form(String title, int channel)
        {
            InitializeComponent();

            chan = channel;
            this.FormClosing += new FormClosingEventHandler(Closing_Window);
            this.Text = title;

            lineChart.Titles.Add(title);
            lineChart.Series[0].Name = "Real Data";
            lineChart.ChartAreas[0].AxisY.IsStartedFromZero = false;
            lineChart.ChartAreas[0].AxisX.LabelStyle.Enabled = false;
            lineChart.ChartAreas[0].AxisX.IsMarginVisible = false;
            lineChart.ChartAreas[0].AxisY.LabelAutoFitStyle = LabelAutoFitStyles.None;
            lineChart.ChartAreas[0].AxisX.LabelStyle.Font = new Font("Arial", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
        }

        /* ************************************************************
         * Description : Add one point in the line graph
         * Input : value record
         * Output : void
         * ************************************************************
         */
        public void Add_Point(double value)
        {
            try
            {
                lineChart.Invoke(new Action(() => lineChart.Series[0].Points.AddXY(currentX, value)));
                readingLabel.Invoke(new Action(() => readingLabel.Text = value.ToString()));
                currentX++;
            }
            catch (Exception)
            {
                return;
            }
        }

        /* ************************************************************
         * Description : Set button click event, set the upper and lower bond of line graph
         * Input : event
         * Output : void
         * ************************************************************
         */
        private void Setbutton_Click(object sender, EventArgs e)
        {
            if (Util.valid_float(upperbond_textbox.Text) && Util.valid_float(lowerbond_textbox.Text))
            {
                lineChart.ChartAreas[0].AxisY.Maximum = Int32.Parse(upperbond_textbox.Text);
                lineChart.ChartAreas[0].AxisY.Minimum = Int32.Parse(lowerbond_textbox.Text);
            }
        }

        /* ************************************************************
         * Description : Auto button click event, auto set the upper and lower bond of line graph
         * Input : event
         * Output : void
         * ************************************************************
         */
        private void Autosetbutton_Click(object sender, EventArgs e)
        {
            lineChart.ChartAreas[0].AxisY.Maximum = Double.NaN;
            lineChart.ChartAreas[0].AxisY.Minimum = Double.NaN;
            lineChart.ChartAreas[0].RecalculateAxesScale();
        }

        /* ************************************************************
         * Description : Line Graph closing event, disable updating
         * Input : event
         * Output : void
         * ************************************************************
         */
        private void Closing_Window(object sender, FormClosingEventArgs e)
        {
            Software.realtimeForm.plotFormType[chan] = 0;
            Software.realtimeForm.linePlotForm[chan] = null;
        }
    }
}
