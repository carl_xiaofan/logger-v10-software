﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using LoggerSoftware.Core;
using LoggerSoftware.SettingTab;
using LoggerSoftware.DataTab;

namespace LoggerSoftware
{
    static class Software
    {
        public static Main_form mainForm { set; get; }

        public static SensorTab sensorTab { set; get; }

        public static ClockTab clockTab { set; get; }

        public static RoleTab roleTab { set; get; }

        public static DownLoadTab menudownloadTab { set; get; }

        public static RecordSettingTab recordsettingTab { set; get; }

        public static RadioTab radioTab { set; get; }

        public static SystemTab systemTab { set; get; }

        public static RealTime_form realtimeForm { set; get; }

        public static SerialPort serialPort { set; get; }

        public static Connection connection { set; get; }

        public static bool enableCoreSettings { set; get; }

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            enableCoreSettings = false;

            sensorTab = new SensorTab();
            clockTab = new ClockTab();
            roleTab = new RoleTab();
            menudownloadTab = new DownLoadTab();
            recordsettingTab = new RecordSettingTab();
            radioTab = new RadioTab();
            systemTab = new SystemTab();

            mainForm = new Main_form();
            Application.Run(mainForm);
        }
    }
}
