﻿namespace LoggerSoftware
{
    partial class Connection_com_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.fix_lable1 = new System.Windows.Forms.Label();
            this.rescan_button = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.serialNumberTextbox = new System.Windows.Forms.TextBox();
            this.serialNumberLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comPanel
            // 
            this.comPanel.AutoScroll = true;
            this.comPanel.Location = new System.Drawing.Point(8, 44);
            this.comPanel.Name = "comPanel";
            this.comPanel.Size = new System.Drawing.Size(289, 118);
            this.comPanel.TabIndex = 2;
            // 
            // fix_lable1
            // 
            this.fix_lable1.AutoSize = true;
            this.fix_lable1.CausesValidation = false;
            this.fix_lable1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fix_lable1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fix_lable1.Location = new System.Drawing.Point(10, 12);
            this.fix_lable1.Name = "fix_lable1";
            this.fix_lable1.Size = new System.Drawing.Size(229, 18);
            this.fix_lable1.TabIndex = 2;
            this.fix_lable1.Text = "Choose a COM Port to Connect";
            // 
            // rescan_button
            // 
            this.rescan_button.BackColor = System.Drawing.SystemColors.Info;
            this.rescan_button.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rescan_button.Location = new System.Drawing.Point(321, 44);
            this.rescan_button.Name = "rescan_button";
            this.rescan_button.Size = new System.Drawing.Size(92, 38);
            this.rescan_button.TabIndex = 1;
            this.rescan_button.Text = "Rescan";
            this.rescan_button.UseVisualStyleBackColor = false;
            this.rescan_button.Click += new System.EventHandler(this.rescan_button_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // serialNumberTextbox
            // 
            this.serialNumberTextbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialNumberTextbox.Location = new System.Drawing.Point(321, 133);
            this.serialNumberTextbox.Name = "serialNumberTextbox";
            this.serialNumberTextbox.Size = new System.Drawing.Size(100, 22);
            this.serialNumberTextbox.TabIndex = 0;
            // 
            // serialNumberLabel
            // 
            this.serialNumberLabel.AutoSize = true;
            this.serialNumberLabel.CausesValidation = false;
            this.serialNumberLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.serialNumberLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialNumberLabel.Location = new System.Drawing.Point(318, 109);
            this.serialNumberLabel.Name = "serialNumberLabel";
            this.serialNumberLabel.Size = new System.Drawing.Size(108, 18);
            this.serialNumberLabel.TabIndex = 4;
            this.serialNumberLabel.Text = "Serial Number";
            // 
            // Connection_com_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(440, 168);
            this.Controls.Add(this.serialNumberLabel);
            this.Controls.Add(this.serialNumberTextbox);
            this.Controls.Add(this.rescan_button);
            this.Controls.Add(this.fix_lable1);
            this.Controls.Add(this.comPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Connection_com_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "COM Ports Connection";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel comPanel;
        private System.Windows.Forms.Label fix_lable1;
        private System.Windows.Forms.Button rescan_button;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox serialNumberTextbox;
        private System.Windows.Forms.Label serialNumberLabel;
    }
}