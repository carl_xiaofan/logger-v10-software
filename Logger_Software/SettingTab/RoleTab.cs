﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LoggerSoftware.Core;

namespace LoggerSoftware.SettingTab
{
    class RoleTab
    {
        private TextBox[] slaveNumberTextboxs;

        private Label[] slaveNumberLabels;

        public RoleTab()
        {

        }

        public bool RoleTab_Init()
        {
            if (Software.connection == null)
            {
                MessageEx.Mesaage_Error("Connection not established", "Please connect to at least one logger");
                return false;
            }

            Software.mainForm.slaveNumberTextbox1.Enabled = Software.enableCoreSettings;
            Software.mainForm.slaveNumberTextbox2.Enabled = Software.enableCoreSettings;
            Software.mainForm.slaveNumberTextbox3.Enabled = Software.enableCoreSettings;
            Software.mainForm.slaveNumberTextbox4.Enabled = Software.enableCoreSettings;
            Software.mainForm.slaveNumberTextbox5.Enabled = Software.enableCoreSettings;
            Software.mainForm.roleDropdown.Enabled = Software.enableCoreSettings;
            Software.mainForm.slaveModeDropdown.Enabled = Software.enableCoreSettings;
            Software.mainForm.rolePushButton.Enabled = Software.enableCoreSettings;

            slaveNumberTextboxs = new TextBox[5]
            {
                Software.mainForm.slaveNumberTextbox1,
                Software.mainForm.slaveNumberTextbox2,
                Software.mainForm.slaveNumberTextbox3,
                Software.mainForm.slaveNumberTextbox4,
                Software.mainForm.slaveNumberTextbox5,
            };

            slaveNumberLabels = new Label[5]
            {
                Software.mainForm.slaveNumberLabel1,
                Software.mainForm.slaveNumberLabel2,
                Software.mainForm.slaveNumberLabel3,
                Software.mainForm.slaveNumberLabel4,
                Software.mainForm.slaveNumberLabel5,
            };

            Software.connection.Get_RoleInfo();

            Software.mainForm.roleDropdown.SelectedIndex = Software.connection.loggerInfo.role;

            if (Software.connection.loggerInfo.slaveMode >= 2)
            {
                Software.mainForm.slaveModeDropdown.SelectedIndex = Software.connection.loggerInfo.slaveMode - 2;
            }
            Software.mainForm.slaveNumberDropdown.SelectedIndex = Software.connection.loggerInfo.slaveNumber;

            for (int i = 0; i < 5; i++)
            {
                slaveNumberTextboxs[i].Text = Software.connection.loggerInfo.slaveList[i].ToString();
            }

            return true;
        }

        public void Role_Dropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (Software.mainForm.roleDropdown.SelectedIndex)
            {
                case 0:
                    for (int i = 0; i < 5; i++)
                    {
                        slaveNumberTextboxs[i].Visible = true;
                        slaveNumberLabels[i].Visible = true;
                    }

                    Software.mainForm.slaveListLabel.Visible = true;
                    Software.mainForm.slaveModeLabel.Visible = true;
                    Software.mainForm.slaveModeDropdown.Visible = true;
                    Software.mainForm.slaveNumberLabel.Visible = false;
                    Software.mainForm.slaveNumberDropdown.Visible = false;
                    break;
                case 1:
                    for (int i = 0; i < 5; i++)
                    {
                        slaveNumberTextboxs[i].Visible = false;
                        slaveNumberLabels[i].Visible = false;
                    }
                    Software.mainForm.slaveListLabel.Visible = false;
                    Software.mainForm.slaveModeLabel.Visible = false;
                    Software.mainForm.slaveModeDropdown.Visible = false;
                    Software.mainForm.slaveNumberLabel.Visible = true;
                    Software.mainForm.slaveNumberDropdown.Visible = true;
                    break;
            }
        }

        public void rolePushButton_Click(object sender, EventArgs e)
        {
            int[] slaveList = new int[5];
            for (int i = 0; i < 5; i++)
            {
                if (Util.valid_int(slaveNumberTextboxs[i].Text, 0, 65535))
                {
                    slaveList[i] = Int32.Parse(slaveNumberTextboxs[i].Text);
                }
                else
                {
                    Software.mainForm.Log_WriteLine(String.Format("Slave Number {0}'s serial number is invalid", i + 1));
                    MessageEx.Mesaage_Error("Slave Number is invalid", String.Format("Slave Number {0}'s serial number is invalid", i + 1));
                    return;
                }
            }

            Software.connection.SendPacket(Packet.SET_RoleInfo_Packet(Software.mainForm.roleDropdown.SelectedIndex,
                Software.mainForm.slaveModeDropdown.SelectedIndex + 2, Software.mainForm.slaveNumberDropdown.SelectedIndex, slaveList));

            Software.mainForm.Log_WriteLine(String.Format("Role Setting Pushed"));
        }
    }
}
