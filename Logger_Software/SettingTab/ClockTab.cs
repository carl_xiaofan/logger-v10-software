﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using LoggerSoftware.Core;

namespace LoggerSoftware.SettingTab
{
    class ClockTab
    {
        private delegate void SafeCallDelegate(string text);
        public ClockTab()
        {
            
        }

        /* ************************************************************ 
         * Description : Init clock tab, start thread to refresh local time
         * Input : void
         * Output : false if no connection established
         * ************************************************************
         */
        public bool ClockTab_Init()
        {
            if (Software.connection == null)
            {
                MessageEx.Mesaage_Error("Connection not established", "Please connect to at least one logger");
                return false;
            }

            Thread update_clock = new Thread(update_systemTime);
            update_clock.Start();
            Software.mainForm.clockTextbox.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

            return true;
        }

        /* ************************************************************ 
         * Description : Update system time thread
         * Input : void
         * Output : void
         * ************************************************************
         */
        private void update_systemTime()
        {
            while (true)
            {
                DateTime now = DateTime.Now;
                setsystemtime(now.ToString("dd/MM/yyyy HH:mm:ss"));
                Thread.Sleep(1000);
            }
        }

        /* ************************************************************ 
         * Description : Set system time
         * Input : text
         * Output : void
         * ************************************************************
         */
        private void setsystemtime(string text)
        {
            if (Software.mainForm.localtimeTextbox.InvokeRequired)
            {
                var d = new SafeCallDelegate(setsystemtime);
                Software.mainForm.localtimeTextbox.Invoke(d, new object[] { text });
            }
            else
            {
                Software.mainForm.localtimeTextbox.Text = text;
            }
        }

        /* ************************************************************ 
         * Description : Push system time to logger
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void systemtime_button_Click(object sender, EventArgs e)
        {
            Software.connection.SendPacket(Packet.SET_RTC_Packet());
            Software.mainForm.Log_WriteLine("Set logger time to local time");
        }

        /* ************************************************************ 
         * Description : Push custom time to logger
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void customtime_button_Click(object sender, EventArgs e)
        {
            DateTime t = Convert.ToDateTime(Software.mainForm.clockTextbox.Text);
            Software.connection.SendPacket(Packet.SET_Time_Packet((int)Constant.Set_flag.RTC, t.Year ,t.Month, t.Day, t.Hour, t.Minute, t.Second));
            Software.mainForm.Log_WriteLine(String.Format("Set logger time to {0}", t));
        }
    }
}
