﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using LoggerSoftware.Core;
using System.Windows.Forms;

namespace LoggerSoftware.SettingTab
{
    class RecordSettingTab
    {
        TextBox[] rocTextboxs;

        public RecordSettingTab()
        {

        }

        public bool RecordSettingTab_Init()
        {
            Cursor.Current = Cursors.WaitCursor;

            if (Software.connection == null)
            {
                MessageEx.Mesaage_Error("Connection not established", "Please connect to at least one logger");
                return false;
            }

            rocTextboxs = new TextBox[8]
            {
                Software.mainForm.c1Textbox,
                Software.mainForm.c2Textbox,
                Software.mainForm.c3Textbox,
                Software.mainForm.c4Textbox,
                Software.mainForm.c5Textbox,
                Software.mainForm.c6Textbox,
                Software.mainForm.c7Textbox,
                Software.mainForm.c8Textbox,
            };

            if (Software.connection.Get_RecordInfo())
            {
                Software.mainForm.enablereadingDropdown.SelectedIndex = Software.connection.loggerInfo.enableRecord;

                Software.mainForm.enablesynchronizeDropdown.SelectedIndex = Software.connection.loggerInfo.enableSynchronize;

                Software.mainForm.intervalTextbox.Text = Software.connection.loggerInfo.measureInterval.ToString();

                Software.mainForm.fastreadCheck.Checked = Software.connection.loggerInfo.enableFastread == 1 ? true : false;

                if (Software.mainForm.fastreadCheck.Checked)
                {
                    Software.mainForm.intervalTextbox.Enabled = false;
                }
                else
                {
                    Software.mainForm.intervalTextbox.Enabled = true;
                }

                for (int i = 0; i < 8; i++)
                {
                    rocTextboxs[i].Text = Software.connection.loggerInfo.rocValue[i].ToString();
                }
            }
            else
            {
                MessageEx.Mesaage_Error("Connection not established", "Please connect to at least one logger");
                return false;
            }

            Cursor.Current = Cursors.Default;

            return true;
        }

        /* ************************************************************ 
         * Description : Push record settings to logger
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void recordsetting_button_Click(object sender, EventArgs e)
        {
            float[] rocValues = new float[8];

            for (int i = 0; i < 8; i++)
            {
                if (Util.valid_float(rocTextboxs[i].Text))
                {
                    rocValues[i] = float.Parse(rocTextboxs[i].Text);
                }
                else
                {
                    Software.mainForm.Log_WriteLine(String.Format("Channel {0} Record On Change is invalid", i + 1));
                    MessageEx.Mesaage_Error("Record On Change is invalid", String.Format("Channel {0} Record On Change is invalid", i + 1));
                    return;
                }
            }

            if (Util.valid_int(Software.mainForm.intervalTextbox.Text, 1, 65535))
            {
                Software.connection.SendPacket(Packet.SET_Record_Packet(Software.mainForm.enablereadingDropdown.SelectedIndex, Software.mainForm.enablesynchronizeDropdown.SelectedIndex, 
                Int32.Parse(Software.mainForm.intervalTextbox.Text), Software.mainForm.fastreadCheck.Checked ? 1 : 0, rocValues));
            }
            else
            {
                MessageEx.Mesaage_Error("Record Interval is invalid", "Record Interval is invalid, can not less than 5");
                return;
            }

            Software.mainForm.Log_WriteLine(String.Format("Record Setting Pushed"));
        }

        /* ************************************************************ 
         * Description : Hnadle fast read check box check event
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void FastRead_CheckBox_Click(object sender, EventArgs e)
        {
            if (Software.mainForm.fastreadCheck.Checked)
            {
                Software.mainForm.intervalTextbox.Enabled = false;
            }
            else
            {
                Software.mainForm.intervalTextbox.Enabled = true;
            }
        }

    }
}
