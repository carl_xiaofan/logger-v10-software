﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using LoggerSoftware.Core;

namespace LoggerSoftware.SettingTab
{
    class SensorTab
    {
        public int selectedChannel;    // Current selecting channel

        private Button[] channelButtons;  // Channel buttons collection

        private TextBox[] cofTextboxs;

        private bool equationEnable;

        public SensorTab()
        {
            selectedChannel = 1;
        }

        public bool SensorTab_Init()
        {
            if(!Init_Tab())
            {
                return false;
            }

            selectedChannel = 1;

            if (!Refresh_Tab(true))
            {
                return false;
            }

            return true;
        }

        private bool Init_Tab()
        {
            if (Software.connection == null)
            {
                MessageEx.Mesaage_Error("Connection not established", "Please connect to at least one logger");
                return false;
            }

            channelButtons = new Button[8]
            {
                Software.mainForm.channelButton1,
                Software.mainForm.channelButton2,
                Software.mainForm.channelButton3,
                Software.mainForm.channelButton4,
                Software.mainForm.channelButton5,
                Software.mainForm.channelButton6,
                Software.mainForm.channelButton7,
                Software.mainForm.channelButton8,
            };

            cofTextboxs = new TextBox[5]
            {
                Software.mainForm.x0Textbox,
                Software.mainForm.x1Textbox,
                Software.mainForm.x2Textbox,
                Software.mainForm.x3Textbox,
                Software.mainForm.x4Textbox,
            };

            Refresh_Buttons();

            return true;

        }

        /* ************************************************************ 
         * Description : Channel button Click Event, refresh interface
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void Channel_Button_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;

            selectedChannel = (int)bt.Text[8] - 48;

            Refresh_Tab(false);
        }

        /* ************************************************************ 
         * Description : Sensor Type dropdown index change event, change interface format
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void Sensor_Dropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Software.mainForm.sensorDropdown.SelectedIndex == 0)  // Unused
            {
                if (selectedChannel % 2 == 1)
                {
                    channelButtons[selectedChannel].Enabled = true;
                }

                Show_VWstuff(false);

                Software.mainForm.settleLabel.Visible = false;
                Software.mainForm.settleTextbox.Visible = false;

                Software.mainForm.unitLabel.Visible = false;
                Software.mainForm.unitTextbox.Visible = false;

                Show_Equation(false);

                Software.mainForm.counterresetLabel.Visible = false;
                Software.mainForm.counterresetDropdown.Visible = false;
            }
            else if (Software.mainForm.sensorDropdown.SelectedIndex == 1)  // Vibrating Wire
            {
                Show_VWstuff(true);

                Software.mainForm.settleLabel.Visible = true;
                Software.mainForm.settleTextbox.Visible = true;

                Software.mainForm.unitLabel.Visible = true;
                Software.mainForm.unitTextbox.Visible = true;

                Software.mainForm.counterresetLabel.Visible = false;
                Software.mainForm.counterresetDropdown.Visible = false;

                Show_Equation(true);
            }
            else if (Software.mainForm.sensorDropdown.SelectedIndex == 5)   // Counter
            {
                if (selectedChannel % 2 == 1)
                {
                    channelButtons[selectedChannel].Enabled = true;
                }

                Show_VWstuff(false);

                Software.mainForm.settleLabel.Visible = false;
                Software.mainForm.settleTextbox.Visible = false;

                Software.mainForm.unitLabel.Visible = true;
                Software.mainForm.unitTextbox.Visible = true;

                Software.mainForm.counterresetLabel.Visible = true;
                Software.mainForm.counterresetDropdown.Visible = true;

                Show_Equation(true);
            }
            else if (Software.mainForm.sensorDropdown.SelectedIndex == 4)   // Frequency
            {
                if (selectedChannel % 2 == 1)
                {
                    channelButtons[selectedChannel].Enabled = true;
                }

                Show_VWstuff(false);

                Software.mainForm.settleLabel.Visible = true;
                Software.mainForm.settleTextbox.Visible = true;

                Software.mainForm.cycleLabel.Visible = true;
                Software.mainForm.cycleTextbox.Visible = true;

                Software.mainForm.unitLabel.Visible = true;
                Software.mainForm.unitTextbox.Visible = true;

                Software.mainForm.counterresetLabel.Visible = false;
                Software.mainForm.counterresetDropdown.Visible = false;

                Show_Equation(true);
            }
            else
            {
                if (selectedChannel % 2 == 1)
                {
                    channelButtons[selectedChannel].Enabled = true;
                }

                Show_VWstuff(false);

                Software.mainForm.settleLabel.Visible = true;
                Software.mainForm.settleTextbox.Visible = true;

                Software.mainForm.unitLabel.Visible = true;
                Software.mainForm.unitTextbox.Visible = true;

                Software.mainForm.counterresetLabel.Visible = false;
                Software.mainForm.counterresetDropdown.Visible = false;

                Show_Equation(true);
            }
        }

        /* ************************************************************ 
         * Description : Compensated dropdown index change event, change interface format
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void compensated_dropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Software.mainForm.compensatedDropdown.Visible == true)
            {
                Show_Compensation(true);
            }

        }

        /* ************************************************************ 
         * Description : Equation dropdown index change event, change interface format
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void Equation_Dropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (equationEnable)
            {
                switch (Software.mainForm.equationDropdown.SelectedIndex)
                {
                    case 0:
                        Software.mainForm.coefficientLabel.Visible = false;
                        Software.mainForm.x0Label.Visible = false;
                        Software.mainForm.x1Label.Visible = false;
                        Software.mainForm.x2Label.Visible = false;
                        Software.mainForm.x3Label.Visible = false;
                        Software.mainForm.x4Label.Visible = false;
                        foreach (TextBox tb in cofTextboxs)
                        {
                            tb.Visible = false;
                        }
                        Show_Compensation(false);
                        break;
                    case 1:
                        Software.mainForm.coefficientLabel.Visible = true;
                        Software.mainForm.x0Label.Visible = true;
                        Software.mainForm.x1Label.Visible = true;
                        Software.mainForm.x2Label.Visible = true;
                        Software.mainForm.x3Label.Visible = true;
                        Software.mainForm.x4Label.Visible = true;
                        foreach (TextBox tb in cofTextboxs)
                        {
                            tb.Visible = true;
                        }
                        Show_Compensation(true);
                        break;
                    case 2:
                        Software.mainForm.coefficientLabel.Visible = true;
                        Software.mainForm.x0Label.Visible = true;
                        Software.mainForm.x1Label.Visible = true;
                        Software.mainForm.x2Label.Visible = true;
                        Software.mainForm.x3Label.Visible = true;
                        Software.mainForm.x4Label.Visible = true;
                        foreach (TextBox tb in cofTextboxs)
                        {
                            tb.Visible = true;
                        }
                        Show_Compensation(false);
                        break;
                }
            }
            else
            {
                Software.mainForm.coefficientLabel.Visible = false;
                Software.mainForm.x0Label.Visible = false;
                Software.mainForm.x1Label.Visible = false;
                Software.mainForm.x2Label.Visible = false;
                Software.mainForm.x3Label.Visible = false;
                Software.mainForm.x4Label.Visible = false;
                foreach (TextBox tb in cofTextboxs)
                {
                    tb.Visible = false;
                }
                Show_Compensation(false);
            }

        }

        /* ************************************************************ 
         * Description : Push button click event, push settings to logger
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void sensor_push_button_Click(object sender, EventArgs e)
        {
            Constant.Sensor_Info set_sensor_infos = new Constant.Sensor_Info();
            set_sensor_infos.Init();

            set_sensor_infos.type = Software.mainForm.sensorDropdown.SelectedIndex;

            // Break when set sensor type to unused
            if (set_sensor_infos.type == 0)
            {
                Software.connection.SendPacket(Packet.SET_Sensor_Type_Packet(selectedChannel - 1,
                    set_sensor_infos.type));
                Software.mainForm.Log_WriteLine(String.Format("Reset channel {0}", selectedChannel));

                Thread.Sleep(Software.connection.delayMs[Software.connection.connectOption]);

                Refresh_Tab(true);

                Refresh_Buttons();

                return;
            }

            set_sensor_infos.unit = Software.mainForm.unitTextbox.Text.PadRight(4, ' ');

            if (set_sensor_infos.type == 1)
            {
                // Cycles
                if (Util.valid_int(Software.mainForm.cycleTextbox.Text, 0, 65535))
                {
                    set_sensor_infos.cycle = Int32.Parse(Software.mainForm.cycleTextbox.Text);
                }
                else
                {

                    MessageEx.Mesaage_Error("Invalid Input Data", "Cycles number not valid");
                    return;
                }

                // Sweep Start
                if (Util.valid_int(Software.mainForm.sweepTextbox1.Text, 0, 65535))
                {
                    set_sensor_infos.excite_start = Int32.Parse(Software.mainForm.sweepTextbox1.Text);
                }
                else
                {
                    MessageEx.Mesaage_Error("Invalid Input Data", "Sweep start not valid");
                    return;
                }

                // Sweep Stop
                if (Util.valid_int(Software.mainForm.sweepTextbox2.Text, 0, 65535))
                {
                    set_sensor_infos.excite_stop = Int32.Parse(Software.mainForm.sweepTextbox2.Text);
                }
                else
                {
                    MessageEx.Mesaage_Error("Invalid Input Data", "Sweep stop not valid");
                    return;
                }
            }

            set_sensor_infos.equation = Software.mainForm.equationDropdown.SelectedIndex;

            if (set_sensor_infos.equation != 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (Util.valid_float(cofTextboxs[i].Text))
                    {
                        set_sensor_infos.cof[i] = float.Parse(cofTextboxs[i].Text);
                    }
                    else
                    {
                        MessageEx.Mesaage_Error("Invalid Input Data", String.Format("Coefficient x^{0} not valid", i));
                        return;
                    }
                }

                // Compensated
                if (Software.mainForm.compensatedDropdown.SelectedIndex == 0)
                {
                    set_sensor_infos.compensation = 0;
                }
                else
                {
                    // Compensation
                    if (Util.valid_int(Software.mainForm.compensationTextbox.Text, 1, 8))
                    {
                        set_sensor_infos.compensation = Int32.Parse(Software.mainForm.compensationTextbox.Text) - 1;
                    }
                    else
                    {
                        MessageEx.Mesaage_Error("Invalid Input Data", "Compensation channel not valid");
                        return;
                    }
                }
            }
            if (set_sensor_infos.type == 4)
            {
                // Cycles
                if (Util.valid_int(Software.mainForm.cycleTextbox.Text, 0, 65535))
                {
                    set_sensor_infos.cycle = Int32.Parse(Software.mainForm.cycleTextbox.Text);
                }
                else
                {

                    MessageEx.Mesaage_Error("Invalid Input Data", "Cycles number not valid");
                    return;
                }
            }

            if (set_sensor_infos.type == 5)
            {
                set_sensor_infos.counter_reset = Software.mainForm.counterresetDropdown.SelectedIndex;
            }

            if (set_sensor_infos.type != 5 && set_sensor_infos.type != 0)
            {
                // Settle time
                if (Util.valid_int(Software.mainForm.settleTextbox.Text, 0, 65535))
                {
                    set_sensor_infos.settle = Int32.Parse(Software.mainForm.settleTextbox.Text);
                }
                else
                {
                    MessageEx.Mesaage_Error("Invalid Input Data", "Settle Time not valid");
                    return;
                }
            }

            Software.connection.SendPacket(Packet.SET_Sensor_Packet(selectedChannel - 1, set_sensor_infos));
            
            Software.mainForm.Log_WriteLine(String.Format("Push sensor settings for channel {0}", selectedChannel));

            Thread.Sleep(Software.connection.delayMs[Software.connection.connectOption]);

            Refresh_Tab(true);

            Refresh_Buttons();
        }

        /* ************************************************************ 
         * Description : Reset button click event, reset all channel settings to default
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void sensor_reset_button_Click(object sender, EventArgs e)
        {
            Software.connection.SendPacket(Packet.SET_RESET_Packet());

            Thread.Sleep(Software.connection.delayMs[Software.connection.connectOption]);

            Refresh_Tab(true);

            for (int i = 0; i < 8; i++)
            {
                Software.connection.loggerInfo.sensorInfo[i].type = 0;
            }

            Refresh_Buttons();
            
            Software.mainForm.Log_WriteLine(String.Format("Reset all sensor settings to default"));
        }

        /* ************************************************************ 
         * Description : Show/hide Vibrating wire section
         * Input : show/hide
         * Output : void
         * ************************************************************
         */
        private void Show_VWstuff(bool enable)
        {
            Software.mainForm.cycleLabel.Visible = enable;
            Software.mainForm.sweepLabel.Visible = enable;
            Software.mainForm.toLabel.Visible = enable;

            Software.mainForm.cycleTextbox.Visible = enable;
            Software.mainForm.sweepTextbox1.Visible = enable;
            Software.mainForm.sweepTextbox2.Visible = enable;
        }

        /* ************************************************************ 
         * Description : Show/hide equation section
         * Input : show/hide
         * Output : void
         * ************************************************************
         */
        private void Show_Equation(bool enable)
        {
            Software.mainForm.equationLabel.Visible = enable;
            Software.mainForm.equationDropdown.Visible = enable;
            equationEnable = enable;
        }

        /* ************************************************************ 
         * Description : Show/hide compensation section
         * Input : show/hide
         * Output : void
         * ************************************************************
         */
        private void Show_Compensation(bool enable)
        {
            if (enable)
            {
                Software.mainForm.compensatedLabel.Visible = true;
                Software.mainForm.compensatedDropdown.Visible = true;

                if (Software.mainForm.compensatedDropdown.SelectedIndex == 0)
                {
                    Software.mainForm.x0Label.Visible = true;
                    Software.mainForm.compensationLabel.Visible = false;
                    Software.mainForm.x0Textbox.Visible = true;
                    Software.mainForm.compensationTextbox.Visible = false;
                }
                else
                {
                    Software.mainForm.x0Label.Visible = false;
                    Software.mainForm.compensationLabel.Visible = true;
                    Software.mainForm.x0Textbox.Visible = false;
                    Software.mainForm.compensationTextbox.Visible = true;
                }
            }
            else
            {
                Software.mainForm.compensatedLabel.Visible = false;
                Software.mainForm.compensatedDropdown.Visible = false;
                Software.mainForm.compensationLabel.Visible = false;
                Software.mainForm.compensationTextbox.Visible = false;
            }
        }

        /* ************************************************************ 
         * Description : Refresh sensor info, will only send package if not got info
         * Input : forceGetInfo force to get sensor info from logger
         * Output : bool
         * ************************************************************
         */
        private bool Refresh_Tab(bool forceGetInfo)
        {
            Cursor.Current = Cursors.WaitCursor;
            Constant.Sensor_Info info;

            if (forceGetInfo)
            {
                info = Software.connection.Get_SensorInfo(selectedChannel - 1);
                Software.connection.loggerInfo.sensorInfo[selectedChannel - 1] = info;
            }
            else
            {
                if (Software.connection.loggerInfo.sensorInfo[selectedChannel - 1].settle != -1)
                {
                    info = Software.connection.loggerInfo.sensorInfo[selectedChannel - 1];
                }
                else
                {
                    info = Software.connection.Get_SensorInfo(selectedChannel - 1);
                    Software.connection.loggerInfo.sensorInfo[selectedChannel - 1] = info;
                }
            }

            if (info.type != -1)
            {
                Software.mainForm.channelLabel.Text = String.Format("Channel {0}", selectedChannel);
                Software.mainForm.counterresetDropdown.SelectedIndex = info.counter_reset;
                Software.mainForm.sensorDropdown.SelectedIndex = info.type;
                Software.mainForm.unitTextbox.Text = info.unit.Replace(" ", "");
                Software.mainForm.settleTextbox.Text = info.settle.ToString();
                Software.mainForm.cycleTextbox.Text = info.cycle.ToString();
                Software.mainForm.sweepTextbox1.Text = info.excite_start.ToString();
                Software.mainForm.sweepTextbox2.Text = info.excite_stop.ToString();
                if (info.compensation == 0)
                {
                    Software.mainForm.compensatedDropdown.SelectedIndex = 0;
                }
                else
                {
                    Software.mainForm.compensatedDropdown.SelectedIndex = 1;
                }
                Software.mainForm.compensationTextbox.Text = info.compensation.ToString();
                for (int i = 0; i < 5; i++)
                {
                    cofTextboxs[i].Text = String.Format("{0:E5}", info.cof[i]);
                }
                Software.mainForm.equationDropdown.SelectedIndex = info.equation;

                channelButtons[selectedChannel - 1].Text = String.Format("Channel {0} {1}", selectedChannel, Type_to_String(info.type));

                Cursor.Current = Cursors.Default;
                return true;
            }
            else
            {
                Cursor.Current = Cursors.Default;
                return false;
            }
        }

        /* ************************************************************ 
         * Description : Refresh button text
         * Input : void
         * Output : void
         * ************************************************************
         */
        private void Refresh_Buttons()
        {
            for (int i = 0; i < 8; i ++)
            {
                channelButtons[i].Text = String.Format("Channel {0} {1}", i + 1, Type_to_String(Software.connection.loggerInfo.sensorInfo[i].type));
            }
        }

        /* ************************************************************ 
         * Description : Convet type to string for button text
         * Input : type
         * Output : text
         * ************************************************************
         */
        private static string Type_to_String(int type)
        {
            switch (type)
            {
                case 0:
                    return "UNUSE";
                case 1:
                    return "VW";
                case 2:
                    return "RESIS";
                case 3:
                    return "VOLT";
                case 4:
                    return "FREQ";
                case 5:
                    return "COUNT";
                default:
                    return "ERROR";
            }
        }
    }
}
