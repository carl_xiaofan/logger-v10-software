﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using LoggerSoftware.Core;

namespace LoggerSoftware.SettingTab
{
    class RadioTab
    {
        private delegate void SafeCallDelegate(string text);
        public RadioTab()
        {
            
        }

        /* ************************************************************ 
         * Description : Init clock tab, start thread to refresh local time
         * Input : void
         * Output : false if no connection established
         * ************************************************************
         */
        public bool RadioTab_Init()
        {
            Cursor.Current = Cursors.WaitCursor;

            if (Software.connection == null)
            {
                MessageEx.Mesaage_Error("Connection not established", "Please connect to at least one logger");
                return false;
            }

            Software.mainForm.HPTextbox.Enabled = Software.enableCoreSettings;
            Software.mainForm.IDTextbox.Enabled = Software.enableCoreSettings;
            Software.mainForm.radioPB.Enabled = Software.enableCoreSettings;

            if (Software.connection.Get_RadioInfo())
            {
                Software.mainForm.HPTextbox.Text = Software.connection.loggerInfo.radioHP.ToString();

                Software.mainForm.IDTextbox.Text = Software.connection.loggerInfo.radioID;
            }

            Cursor.Current = Cursors.Default;

            return true;
        }

        public void radioPB_button_Click(object sender, EventArgs e)
        {
            if (Util.valid_int(Software.mainForm.HPTextbox.Text, 0, 7) & Software.mainForm.IDTextbox.Text.Length == 4)
            {
                Software.connection.SendPacket(Packet.SET_Radio_HPID(Int32.Parse(Software.mainForm.HPTextbox.Text), Software.mainForm.IDTextbox.Text));
            }
            else
            {
                MessageEx.Mesaage_Error("Radio Info is invalid", "Radio Info is invalid, HP need between 0 ~ 7, ID need between 0000 ~ FFFF");
                return;
            }

            Software.mainForm.Log_WriteLine(String.Format("Radio Setting Pushed"));
        }
    }
}
