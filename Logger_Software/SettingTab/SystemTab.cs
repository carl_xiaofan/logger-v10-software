﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerSoftware.Core;

namespace LoggerSoftware.SettingTab
{
    class SystemTab
    {
        public SystemTab()
        {

        }

        public bool SystemTab_Init()
        {
            if (Software.connection == null)
            {
                MessageEx.Mesaage_Error("Connection not established", "Please connect to at least one logger");
                return false;
            }

            Refresh_SystemTab();

            return true;
        }

        private void Refresh_SystemTab()
        {
            Software.mainForm.snTextbox.Text = Software.connection.loggerInfo.serialNum.ToString();
            Software.mainForm.fvTextbox.Text = Software.connection.loggerInfo.firmwareNum.ToString();
            Software.mainForm.lbDateTextbox.Text = Software.connection.loggerInfo.buildDate.ToString("dd/MM/yyyy HH:mm:ss");
            Software.mainForm.lsDateTextbox.Text = Software.connection.loggerInfo.lastSet.ToString("dd/MM/yyyy HH:mm:ss");
            Software.mainForm.cpDateTextbox.Text = Software.connection.loggerInfo.checkPoint.ToString("dd/MM/yyyy HH:mm:ss");
            if (Software.connection.connectOption == 0)
            {
                Software.mainForm.cmDropdown.SelectedIndex = 0;
            }
            else if(Software.connection.connectOption == 1)
            {
                if (Software.connection.loggerInfo.role == 0)
                {
                    Software.mainForm.cmDropdown.SelectedIndex = 1;
                }
                else
                {
                    Software.mainForm.cmDropdown.SelectedIndex = 2;
                }
            }
            else
            {
                Software.mainForm.cmDropdown.SelectedIndex = 3;
            }

            Software.mainForm.snTextbox.Enabled = Software.enableCoreSettings;
            Software.mainForm.fvTextbox.Enabled = Software.enableCoreSettings;
            Software.mainForm.lbDateTextbox.Enabled = Software.enableCoreSettings;
            Software.mainForm.lsDateTextbox.Enabled = Software.enableCoreSettings;
            Software.mainForm.cpDateTextbox.Enabled = Software.enableCoreSettings;
            Software.mainForm.cmDropdown.Enabled = Software.enableCoreSettings;
            Software.mainForm.systemPushButton.Enabled = Software.enableCoreSettings;
        }

        public void systemPushButton_Click(object sender, EventArgs e)
        {
            if (Util.valid_int(Software.mainForm.snTextbox.Text, 1, 65535) && Util.valid_int(Software.mainForm.fvTextbox.Text, 1, 65535))
            {
                DateTime buildDate = Convert.ToDateTime(Software.mainForm.lbDateTextbox.Text);
                DateTime lastSettingDate = Convert.ToDateTime(Software.mainForm.lsDateTextbox.Text);
                DateTime checkPoint = Convert.ToDateTime(Software.mainForm.cpDateTextbox.Text);
                
                Software.connection.SendPacket(Packet.SET_SystemInfo_Packet(Int32.Parse(Software.mainForm.snTextbox.Text),
                Int32.Parse(Software.mainForm.fvTextbox.Text), buildDate, lastSettingDate, checkPoint, Software.mainForm.cmDropdown.SelectedIndex));

                Software.connection.loggerInfo.serialNum = Int32.Parse(Software.mainForm.snTextbox.Text);
                Software.connection.requestSerial = Int32.Parse(Software.mainForm.snTextbox.Text);
                Software.connection.loggerInfo.firmwareNum = Int32.Parse(Software.mainForm.fvTextbox.Text);
                Software.connection.loggerInfo.buildDate = buildDate;
                Software.connection.loggerInfo.lastSet = lastSettingDate;
                Software.connection.loggerInfo.checkPoint = checkPoint;

                Software.connection.Refresh_Info();
            }

            Software.mainForm.Log_WriteLine(String.Format("System Setting Pushed"));
        }

    }
}
