﻿namespace LoggerSoftware
{
    partial class RealTime_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rtmDatagrid = new System.Windows.Forms.DataGridView();
            this.Channel_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Channel_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Channel_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Channel_4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Channel_5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Channel_6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Channel_7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Channel_8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fix_label12 = new System.Windows.Forms.Label();
            this.rtmButton = new System.Windows.Forms.Button();
            this.plot_button = new System.Windows.Forms.Button();
            this.sourcechannel_dropdown = new System.Windows.Forms.ComboBox();
            this.plottype_dropdown = new System.Windows.Forms.ComboBox();
            this.sourcechannel_label = new System.Windows.Forms.Label();
            this.plot_label = new System.Windows.Forms.Label();
            this.browseButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.rtmDatagrid)).BeginInit();
            this.SuspendLayout();
            // 
            // rtmDatagrid
            // 
            this.rtmDatagrid.AllowUserToAddRows = false;
            this.rtmDatagrid.AllowUserToDeleteRows = false;
            this.rtmDatagrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtmDatagrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.rtmDatagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rtmDatagrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Channel_1,
            this.Channel_2,
            this.Channel_3,
            this.Channel_4,
            this.Channel_5,
            this.Channel_6,
            this.Channel_7,
            this.Channel_8});
            this.rtmDatagrid.Location = new System.Drawing.Point(20, 48);
            this.rtmDatagrid.Margin = new System.Windows.Forms.Padding(4);
            this.rtmDatagrid.Name = "rtmDatagrid";
            this.rtmDatagrid.ReadOnly = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.rtmDatagrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.rtmDatagrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.rtmDatagrid.Size = new System.Drawing.Size(667, 355);
            this.rtmDatagrid.TabIndex = 0;
            // 
            // Channel_1
            // 
            this.Channel_1.Frozen = true;
            this.Channel_1.HeaderText = "Channel 1";
            this.Channel_1.Name = "Channel_1";
            this.Channel_1.ReadOnly = true;
            this.Channel_1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Channel_1.Width = 80;
            // 
            // Channel_2
            // 
            this.Channel_2.Frozen = true;
            this.Channel_2.HeaderText = "Channel 2";
            this.Channel_2.Name = "Channel_2";
            this.Channel_2.ReadOnly = true;
            this.Channel_2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Channel_2.Width = 80;
            // 
            // Channel_3
            // 
            this.Channel_3.Frozen = true;
            this.Channel_3.HeaderText = "Channel 3";
            this.Channel_3.Name = "Channel_3";
            this.Channel_3.ReadOnly = true;
            this.Channel_3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Channel_3.Width = 80;
            // 
            // Channel_4
            // 
            this.Channel_4.Frozen = true;
            this.Channel_4.HeaderText = "Channel 4";
            this.Channel_4.Name = "Channel_4";
            this.Channel_4.ReadOnly = true;
            this.Channel_4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Channel_4.Width = 80;
            // 
            // Channel_5
            // 
            this.Channel_5.Frozen = true;
            this.Channel_5.HeaderText = "Channel 5";
            this.Channel_5.Name = "Channel_5";
            this.Channel_5.ReadOnly = true;
            this.Channel_5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Channel_5.Width = 80;
            // 
            // Channel_6
            // 
            this.Channel_6.Frozen = true;
            this.Channel_6.HeaderText = "Channel 6";
            this.Channel_6.Name = "Channel_6";
            this.Channel_6.ReadOnly = true;
            this.Channel_6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Channel_6.Width = 80;
            // 
            // Channel_7
            // 
            this.Channel_7.Frozen = true;
            this.Channel_7.HeaderText = "Channel 7";
            this.Channel_7.Name = "Channel_7";
            this.Channel_7.ReadOnly = true;
            this.Channel_7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Channel_7.Width = 80;
            // 
            // Channel_8
            // 
            this.Channel_8.Frozen = true;
            this.Channel_8.HeaderText = "Channel 8";
            this.Channel_8.Name = "Channel_8";
            this.Channel_8.ReadOnly = true;
            this.Channel_8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Channel_8.Width = 80;
            // 
            // fix_label12
            // 
            this.fix_label12.AutoSize = true;
            this.fix_label12.CausesValidation = false;
            this.fix_label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fix_label12.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fix_label12.Location = new System.Drawing.Point(16, 12);
            this.fix_label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fix_label12.Name = "fix_label12";
            this.fix_label12.Size = new System.Drawing.Size(164, 22);
            this.fix_label12.TabIndex = 9;
            this.fix_label12.Text = "Real Time Record";
            // 
            // rtmButton
            // 
            this.rtmButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rtmButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtmButton.Location = new System.Drawing.Point(695, 12);
            this.rtmButton.Margin = new System.Windows.Forms.Padding(4);
            this.rtmButton.Name = "rtmButton";
            this.rtmButton.Size = new System.Drawing.Size(127, 37);
            this.rtmButton.TabIndex = 10;
            this.rtmButton.Text = "Stop";
            this.rtmButton.UseVisualStyleBackColor = true;
            this.rtmButton.Click += new System.EventHandler(this.RTM_Button_Click);
            // 
            // plot_button
            // 
            this.plot_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.plot_button.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plot_button.Location = new System.Drawing.Point(695, 366);
            this.plot_button.Margin = new System.Windows.Forms.Padding(4);
            this.plot_button.Name = "plot_button";
            this.plot_button.Size = new System.Drawing.Size(127, 37);
            this.plot_button.TabIndex = 11;
            this.plot_button.Text = "Plot";
            this.plot_button.UseVisualStyleBackColor = true;
            this.plot_button.Click += new System.EventHandler(this.plot_button_Click);
            // 
            // sourcechannel_dropdown
            // 
            this.sourcechannel_dropdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sourcechannel_dropdown.FormattingEnabled = true;
            this.sourcechannel_dropdown.Items.AddRange(new object[] {
            "Channel 1",
            "Channel 2",
            "Channel 3",
            "Channel 4",
            "Channel 5",
            "Channel 6",
            "Channel 7",
            "Channel 8"});
            this.sourcechannel_dropdown.Location = new System.Drawing.Point(694, 270);
            this.sourcechannel_dropdown.Name = "sourcechannel_dropdown";
            this.sourcechannel_dropdown.Size = new System.Drawing.Size(127, 25);
            this.sourcechannel_dropdown.TabIndex = 12;
            // 
            // plottype_dropdown
            // 
            this.plottype_dropdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.plottype_dropdown.FormattingEnabled = true;
            this.plottype_dropdown.Items.AddRange(new object[] {
            "Line Chart",
            "Gauge"});
            this.plottype_dropdown.Location = new System.Drawing.Point(694, 325);
            this.plottype_dropdown.Name = "plottype_dropdown";
            this.plottype_dropdown.Size = new System.Drawing.Size(127, 25);
            this.plottype_dropdown.TabIndex = 13;
            // 
            // sourcechannel_label
            // 
            this.sourcechannel_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sourcechannel_label.AutoSize = true;
            this.sourcechannel_label.Location = new System.Drawing.Point(702, 250);
            this.sourcechannel_label.Name = "sourcechannel_label";
            this.sourcechannel_label.Size = new System.Drawing.Size(113, 17);
            this.sourcechannel_label.TabIndex = 14;
            this.sourcechannel_label.Text = "Source Channel";
            // 
            // plot_label
            // 
            this.plot_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.plot_label.AutoSize = true;
            this.plot_label.Location = new System.Drawing.Point(724, 305);
            this.plot_label.Name = "plot_label";
            this.plot_label.Size = new System.Drawing.Size(68, 17);
            this.plot_label.TabIndex = 15;
            this.plot_label.Text = "Plot Type";
            // 
            // browseButton
            // 
            this.browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.browseButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browseButton.Location = new System.Drawing.Point(695, 67);
            this.browseButton.Margin = new System.Windows.Forms.Padding(4);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(127, 37);
            this.browseButton.TabIndex = 16;
            this.browseButton.Text = "Browse History";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // RealTime_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 420);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.plot_label);
            this.Controls.Add(this.sourcechannel_label);
            this.Controls.Add(this.plottype_dropdown);
            this.Controls.Add(this.sourcechannel_dropdown);
            this.Controls.Add(this.plot_button);
            this.Controls.Add(this.rtmButton);
            this.Controls.Add(this.fix_label12);
            this.Controls.Add(this.rtmDatagrid);
            this.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RealTime_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RealTime_form";
            ((System.ComponentModel.ISupportInitialize)(this.rtmDatagrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView rtmDatagrid;
        private System.Windows.Forms.Label fix_label12;
        private System.Windows.Forms.Button rtmButton;
        private System.Windows.Forms.Button plot_button;
        private System.Windows.Forms.ComboBox sourcechannel_dropdown;
        private System.Windows.Forms.ComboBox plottype_dropdown;
        private System.Windows.Forms.Label sourcechannel_label;
        private System.Windows.Forms.Label plot_label;
        private System.Windows.Forms.DataGridViewTextBoxColumn Channel_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Channel_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Channel_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Channel_4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Channel_5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Channel_6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Channel_7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Channel_8;
        private System.Windows.Forms.Button browseButton;
    }
}