﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LoggerSoftware.Core;

namespace LoggerSoftware
{
    public partial class GaugeGraph_form : Form
    {
        private int chan = 0;

        /* ************************************************************
         * Description : Initialize the Gauge Graph
         * Input : title what is plotting
         *         channel which channel is plotting
         * Output : void
         * ************************************************************
         */
        public GaugeGraph_form(String title, int channel)
        {
            InitializeComponent();
            chan = channel;
            this.FormClosing += new FormClosingEventHandler(Closing_Window);
        }

        /* ************************************************************
         * Description : Update the read on gauge graph
         * Input : value record
         * Output : void
         * ************************************************************
         */
        public void Update_Read(double y)
        {
            gaugeGraph.Invoke(new Action(() => gaugeGraph.Value = (float)y));
            readingLabel.Invoke(new Action(() => readingLabel.Text = y.ToString()));
        }

        /* ************************************************************
         * Description : Set button click event, set the max and min value of gauge graph
         * Input : event
         * Output : void
         * ************************************************************
         */
        private void Setbutton_Click(object sender, EventArgs e)
        {
            if (Util.valid_float(maxvalue_textbox.Text) && Util.valid_float(minvalue_textbox.Text))
            {
                gaugeGraph.MaxValue = Int32.Parse(maxvalue_textbox.Text);
                gaugeGraph.MinValue = Int32.Parse(minvalue_textbox.Text);
                float step_size = (Int32.Parse(maxvalue_textbox.Text) - Int32.Parse(minvalue_textbox.Text)) / 10F;
                gaugeGraph.ScaleLinesMajorStepValue = step_size;
            }
        }

        /* ************************************************************
         * Description : Line Graph closing event, disable updating
         * Input : event
         * Output : void
         * ************************************************************
         */
        private void Closing_Window(object sender, FormClosingEventArgs e)
        {
            Software.realtimeForm.plotFormType[chan] = 0;
            Software.realtimeForm.gaugePlotForm[chan] = null;
        }
    }
}
