﻿namespace LoggerSoftware
{
    partial class LineGraph_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.lineChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.currentreading_label = new System.Windows.Forms.Label();
            this.readingLabel = new System.Windows.Forms.Label();
            this.upperbond_textbox = new System.Windows.Forms.TextBox();
            this.lowerbond_textbox = new System.Windows.Forms.TextBox();
            this.upperbond_label = new System.Windows.Forms.Label();
            this.lowerbond_label = new System.Windows.Forms.Label();
            this.set_button = new System.Windows.Forms.Button();
            this.autoset_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.lineChart)).BeginInit();
            this.SuspendLayout();
            // 
            // line_chart
            // 
            this.lineChart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea5.InnerPlotPosition.Auto = false;
            chartArea5.InnerPlotPosition.Height = 85F;
            chartArea5.InnerPlotPosition.Width = 90F;
            chartArea5.InnerPlotPosition.X = 10F;
            chartArea5.InnerPlotPosition.Y = 5F;
            chartArea5.Name = "ChartArea1";
            chartArea5.Position.Auto = false;
            chartArea5.Position.Height = 95F;
            chartArea5.Position.Width = 80F;
            chartArea5.Position.Y = 5F;
            this.lineChart.ChartAreas.Add(chartArea5);
            legend5.BackColor = System.Drawing.Color.Transparent;
            legend5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend5.IsTextAutoFit = false;
            legend5.Name = "Legend1";
            legend5.Position.Auto = false;
            legend5.Position.Height = 10.12658F;
            legend5.Position.Width = 15F;
            legend5.Position.X = 80F;
            legend5.Position.Y = 3F;
            this.lineChart.Legends.Add(legend5);
            this.lineChart.Location = new System.Drawing.Point(3, 0);
            this.lineChart.Name = "line_chart";
            this.lineChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series5.BorderColor = System.Drawing.Color.Transparent;
            series5.BorderWidth = 3;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Color = System.Drawing.Color.Red;
            series5.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series5.LabelForeColor = System.Drawing.Color.White;
            series5.Legend = "Legend1";
            series5.Name = "Chart Series";
            this.lineChart.Series.Add(series5);
            this.lineChart.Size = new System.Drawing.Size(727, 238);
            this.lineChart.TabIndex = 0;
            this.lineChart.Text = "chart1";
            // 
            // currentreading_label
            // 
            this.currentreading_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.currentreading_label.AutoSize = true;
            this.currentreading_label.BackColor = System.Drawing.Color.White;
            this.currentreading_label.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentreading_label.Location = new System.Drawing.Point(596, 33);
            this.currentreading_label.Name = "currentreading_label";
            this.currentreading_label.Size = new System.Drawing.Size(115, 17);
            this.currentreading_label.TabIndex = 1;
            this.currentreading_label.Text = "Current Reading";
            // 
            // reading_label
            // 
            this.readingLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.readingLabel.AutoSize = true;
            this.readingLabel.BackColor = System.Drawing.Color.White;
            this.readingLabel.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.readingLabel.ForeColor = System.Drawing.Color.Blue;
            this.readingLabel.Location = new System.Drawing.Point(599, 55);
            this.readingLabel.Name = "reading_label";
            this.readingLabel.Size = new System.Drawing.Size(15, 22);
            this.readingLabel.TabIndex = 2;
            this.readingLabel.Text = " ";
            // 
            // upperbond_textbox
            // 
            this.upperbond_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.upperbond_textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperbond_textbox.Location = new System.Drawing.Point(599, 106);
            this.upperbond_textbox.Name = "upperbond_textbox";
            this.upperbond_textbox.Size = new System.Drawing.Size(112, 22);
            this.upperbond_textbox.TabIndex = 3;
            // 
            // lowerbond_textbox
            // 
            this.lowerbond_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lowerbond_textbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerbond_textbox.Location = new System.Drawing.Point(599, 154);
            this.lowerbond_textbox.Name = "lowerbond_textbox";
            this.lowerbond_textbox.Size = new System.Drawing.Size(112, 22);
            this.lowerbond_textbox.TabIndex = 4;
            // 
            // upperbond_label
            // 
            this.upperbond_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.upperbond_label.AutoSize = true;
            this.upperbond_label.BackColor = System.Drawing.Color.White;
            this.upperbond_label.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperbond_label.Location = new System.Drawing.Point(609, 86);
            this.upperbond_label.Name = "upperbond_label";
            this.upperbond_label.Size = new System.Drawing.Size(93, 17);
            this.upperbond_label.TabIndex = 5;
            this.upperbond_label.Text = "Upper Bound";
            // 
            // lowerbond_label
            // 
            this.lowerbond_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lowerbond_label.AutoSize = true;
            this.lowerbond_label.BackColor = System.Drawing.Color.White;
            this.lowerbond_label.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerbond_label.Location = new System.Drawing.Point(609, 134);
            this.lowerbond_label.Name = "lowerbond_label";
            this.lowerbond_label.Size = new System.Drawing.Size(94, 17);
            this.lowerbond_label.TabIndex = 6;
            this.lowerbond_label.Text = "Lower Bound";
            // 
            // set_button
            // 
            this.set_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.set_button.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.set_button.Location = new System.Drawing.Point(599, 182);
            this.set_button.Name = "set_button";
            this.set_button.Size = new System.Drawing.Size(51, 30);
            this.set_button.TabIndex = 7;
            this.set_button.Text = "Set";
            this.set_button.UseVisualStyleBackColor = true;
            this.set_button.Click += new System.EventHandler(this.Setbutton_Click);
            // 
            // autoset_button
            // 
            this.autoset_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.autoset_button.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.autoset_button.Location = new System.Drawing.Point(660, 182);
            this.autoset_button.Name = "autoset_button";
            this.autoset_button.Size = new System.Drawing.Size(51, 30);
            this.autoset_button.TabIndex = 8;
            this.autoset_button.Text = "Auto";
            this.autoset_button.UseVisualStyleBackColor = true;
            this.autoset_button.Click += new System.EventHandler(this.Autosetbutton_Click);
            // 
            // LineGraph_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 240);
            this.Controls.Add(this.autoset_button);
            this.Controls.Add(this.set_button);
            this.Controls.Add(this.lowerbond_label);
            this.Controls.Add(this.upperbond_label);
            this.Controls.Add(this.lowerbond_textbox);
            this.Controls.Add(this.upperbond_textbox);
            this.Controls.Add(this.readingLabel);
            this.Controls.Add(this.currentreading_label);
            this.Controls.Add(this.lineChart);
            this.Name = "LineGraph_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Graph";
            ((System.ComponentModel.ISupportInitialize)(this.lineChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart lineChart;
        private System.Windows.Forms.Label currentreading_label;
        private System.Windows.Forms.Label readingLabel;
        private System.Windows.Forms.TextBox upperbond_textbox;
        private System.Windows.Forms.TextBox lowerbond_textbox;
        private System.Windows.Forms.Label upperbond_label;
        private System.Windows.Forms.Label lowerbond_label;
        private System.Windows.Forms.Button set_button;
        private System.Windows.Forms.Button autoset_button;
    }
}