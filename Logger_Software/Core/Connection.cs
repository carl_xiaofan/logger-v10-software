﻿using System;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Forms;
using LoggerSoftware.Core;

namespace LoggerSoftware.Core
{
    class Connection
    {
        public bool connected = false;

        public int connectOption = 0;

        private String portName = null;

        public int requestSerial = 0;  // Current contacting logger's serial

        public Constant.Logger_Info loggerInfo = new Constant.Logger_Info();

        public int[] waitSecond = new int[3] { 3, 10, 5 };

        public int[] delayMs = new int[3] { 100, 1000, 500 };

        public Connection(String portName, int option, int serial_num)
        {
            this.portName = portName;

            requestSerial = serial_num;

            connectOption = option;

            if (Establish_Connection(option))
            {
                connected = true;
                Refresh_Info();
            }
        }

        /* ************************************************************
         * Description : Establish connection with Logger.
         * Input : option Serial mode, 0 for USB, 1 for RS232
         * Output : true if success
         * ************************************************************
         */
        private bool Establish_Connection(int option)
        {
            try
            {
                // USB
                if (option == 0)
                {
                    Software.serialPort = new SerialPort(portName, 9600, Parity.None, 8, StopBits.One);
                }
                // RS232
                else if (option == 1)
                {
                    Software.serialPort = new SerialPort(portName, 115200, Parity.None, 8, StopBits.One);
                }
                else if (option == 2)
                {
                    Software.serialPort = new SerialPort(portName, 115200, Parity.None, 8, StopBits.One);
                }

                Software.serialPort.Handshake = Handshake.None;
                Software.serialPort.ReadBufferSize = 700000;

                Software.serialPort.Open();
            }
            catch (Exception)
            {
                MessageEx.Mesaage_Error("Fail to Establish Connection", "Unable to open serial port");
                return false;
            }

            // Start Hand Shake
            if (!Start_Handshake())
            {
                Software.serialPort.Close();
                return false;
            }

            return true;
        }

        /* ************************************************************
         * Description : Disconnect with logger, close port
         * Input : option Serial mode, 0 for USB, 1 for RS232
         * Output : true if success
         * ************************************************************
         */
        public void Disconnect()
        {
            Software.serialPort.Close();

            Software.mainForm.connectioLable.Text = "Disconnected";
            Software.mainForm.connectioLable.ForeColor = System.Drawing.Color.Red;
            Software.mainForm.serialTextbox.Text = "";
            Software.mainForm.firmwareTextbox.Text = "";
            Software.mainForm.setdateTextbox.Text = "";
            Software.mainForm.builddateTextbox.Text = "";
            Software.mainForm.checkTextbox.Text = "";

            Software.mainForm.Log_WriteLine(String.Format("Disconnect with logger {0}", requestSerial));
        }

        /* ************************************************************ 
         * Description : Hand Shake with Logger, Receive basic logger info
         * Input : void
         * Output : true if success
         * ************************************************************
         */
        private bool Start_Handshake()
        {
            Software.serialPort.DiscardInBuffer();
            Software.serialPort.DiscardOutBuffer();

            // Send SYN Packet
            if (!SendPacket(Packet.SYN_Packet()))
            {
                MessageEx.Mesaage_Error("Packet Send Fail", "Sending SYN Packet Failed");
                return false;
            }

            // Wait to get Packet
            int[] packet = Get_Packet(waitSecond[connectOption]);

            if (packet != null)
            {
                if (packet[4] == (int)Constant.Serial_flag.ACK_SYN)
                {
                    loggerInfo.Init();

                    // Serial Number and Firmware Version
                    loggerInfo.serialNum = Util.uin8_to_uin16(packet[5], packet[6]);
                    loggerInfo.firmwareNum = Util.uin8_to_uin16(packet[7], packet[8]);

                    if (connectOption == 0)
                    {
                        requestSerial = loggerInfo.serialNum;
                    }

                    Software.mainForm.Log_WriteLine(String.Format("Successfully connected to Logger from {0}", portName));

                    // Last Setting Date
                    try
                    {
                        loggerInfo.lastSet = new DateTime(Util.uin8_to_uin16(packet[9], packet[10]), packet[11], packet[12], packet[13], packet[14], packet[15]);
                    }
                    catch (Exception)
                    {
                        Software.mainForm.Log_WriteLine("Last Setting Date not proper");
                    }

                    // Build Date
                    try
                    {
                        loggerInfo.buildDate = new DateTime(Util.uin8_to_uin16(packet[16], packet[17]), packet[18], packet[19], packet[20], packet[21], packet[22]);
                    }
                    catch (Exception)
                    {
                        Software.mainForm.Log_WriteLine("Build Date not proper");
                    }

                    // Check Point Date
                    try
                    {
                        loggerInfo.checkPoint = new DateTime(Util.uin8_to_uin16(packet[23], packet[24]), packet[25], packet[26], packet[27], packet[28], packet[29]);
                    }
                    catch (Exception)
                    {
                        Software.mainForm.Log_WriteLine("Check Point Date not proper");
                    }

                    // All channels Sensor Info
                    for (int i = 0; i < 8; i++)
                    {
                        loggerInfo.sensorInfo[i].type = packet[30 + i];
                    }

                    loggerInfo.role = packet[38];

                    // Role
                    if (loggerInfo.role == 0)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            loggerInfo.slaveList[i] = Util.uin8_to_uin16(packet[39 + i * 2], packet[40 + i * 2]);
                        }
                    }
                    else // Slave Numer for Slave
                    {
                        loggerInfo.slaveNumber = packet[49] + 1;
                    }

                    if (!SendPacket(Packet.ACK_SYN_Packet()))
                    {
                        MessageEx.Mesaage_Error("Packet Send Fail", "Sending ACK_SYN Packet Failed");
                        return false;
                    }
                    return true;
                }
            }

            MessageEx.Mesaage_Error("Fail to Establish Connection", "Fail to HandSake with Logger");
            Software.serialPort.DiscardInBuffer();
            return false;

        }

        /* ************************************************************ 
         * Description : Refresh basic logger info
         * Input : void
         * Output : void
         * ************************************************************
         */
        public void Refresh_Info()
        {

            Software.mainForm.connectioLable.Text = "Connected";
            Software.mainForm.connectioLable.ForeColor = System.Drawing.Color.Green;
            Software.mainForm.connectioLable.Text = "Connected";
            Software.mainForm.serialTextbox.Text = loggerInfo.serialNum.ToString();
            Software.mainForm.firmwareTextbox.Text = loggerInfo.firmwareNum.ToString();
            Software.mainForm.setdateTextbox.Text = loggerInfo.lastSet.ToString("dd/MM/yyyy HH:mm:ss");
            Software.mainForm.builddateTextbox.Text = loggerInfo.buildDate.ToString("dd/MM/yyyy HH:mm:ss");
            Software.mainForm.checkTextbox.Text = loggerInfo.checkPoint.ToString("dd/MM/yyyy HH:mm:ss");
        }

        /* ************************************************************ 
         * Description : Get Menu from logger
         * Input : void
         * Output : List of Date, int[] 0:year 1:month 2:day
         * ************************************************************
         */
        public List<int[]> Receive_Menu()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<int[]> menu = new List<int[]>();
            if (Check_Start(waitSecond[connectOption]))
            {
                Software.serialPort.ReadByte();
                Software.serialPort.ReadByte();
                if (Util.uin8_to_uin16(Software.serialPort.ReadByte(), Software.serialPort.ReadByte()) == requestSerial)
                {
                    if (Software.serialPort.ReadByte() == (int)Constant.Serial_flag.DAT)
                    {
                        while (true)
                        {
                            int[] date = new int[3];
                            int year1 = Software.serialPort.ReadByte();
                            int year2 = Software.serialPort.ReadByte();
                            if (year1 == Constant.STOP_BYTE && year2 == Constant.STOP_BYTE)
                            {
                                break;
                            }
                            date[0] = Util.uin8_to_uin16(year1, year2);
                            date[1] = Software.serialPort.ReadByte();
                            date[2] = Software.serialPort.ReadByte();
                            menu.Add(date);
                        }
                        Cursor.Current = Cursors.Default;
                        Software.mainForm.Log_WriteLine("Get Logger Menu successfully");
                        return menu;
                    }
                }
            }
            Cursor.Current = Cursors.Default;
            MessageEx.Mesaage_Error("Receive Menu Fail", "Fail to receive data menu from Logger");
            return menu;
        }

        /* ************************************************************ 
         * Description : Get Record Info from Logger which include enable_record and interval
         * Input : void
         * Output : true is success
         * ************************************************************
         */
        public bool Get_RecordInfo()
        {
            Cursor.Current = Cursors.WaitCursor;

            Software.serialPort.DiscardInBuffer();
            Software.serialPort.DiscardOutBuffer();

            if (!SendPacket(Packet.DAT_RecordInfo_Packet()))
            {
                MessageEx.Mesaage_Error("Packet Send Fail", "Sending Record Info Packet Failed");
                return false;
            }
            else
            {
                int[] packet = Get_Packet(waitSecond[connectOption]);
                if (packet != null)
                {
                    if (Check_Valid(packet, (int)Constant.Serial_flag.DAT))
                    {
                        loggerInfo.enableRecord = packet[5];
                        loggerInfo.enableSynchronize = packet[6];
                        loggerInfo.measureInterval = Util.uin8_to_uin16(packet[7], packet[8]);
                        loggerInfo.enableFastread = packet[9];

                        for (int i = 0; i < 8; i++)
                        {
                            loggerInfo.rocValue[i] = Util.int_to_float(packet[10 + 4 * i], packet[11 + 4 * i], packet[12 + 4 * i], packet[13 + 4 * i]);
                        }
                        Cursor.Current = Cursors.Default;
                        Software.mainForm.Log_WriteLine("Get Record Info successfully");
                        return true;
                    }
                }
            }
            Cursor.Current = Cursors.Default;
            MessageEx.Mesaage_Error("Record Info Fail", "Fail to get record Info From Logger");
            return false;
        }

        /* ************************************************************ 
         * Description : Get Role Info from Logger which include role, salve number, and slave list
         * Input : void
         * Output : true if success
         * ************************************************************
         */
        public bool Get_RoleInfo()
        {
            Cursor.Current = Cursors.WaitCursor;

            Software.serialPort.DiscardInBuffer();
            Software.serialPort.DiscardOutBuffer();
            if (!SendPacket(Packet.DAT_RoleInfo_Packet()))
            {
                MessageEx.Mesaage_Error("Packet Send Fail", "Sending Role Info Packet Failed");
                return false;
            }
            else
            {
                int[] packet = Get_Packet(10);
                if (packet != null)
                {
                    if (Check_Valid(packet, (int)Constant.Serial_flag.DAT))
                    {
                        loggerInfo.role = packet[5];
                        loggerInfo.slaveNumber = packet[6];
                        loggerInfo.slaveMode = packet[7];
                        for (int i = 0; i < 5; i++)
                        {
                            loggerInfo.slaveList[i] = Util.uin8_to_uin16(packet[8 + 2 * i], packet[9 + 2 * i]);
                        }
                        Software.mainForm.Log_WriteLine("Get Role Info successfully");
                        Cursor.Current = Cursors.Default;
                        return true;
                    }
                }
            }
            Cursor.Current = Cursors.Default;
            MessageEx.Mesaage_Error("Role Info Fail", "Fail to get role Info From Logger");
            return false;
        }

        /* ************************************************************ 
         * Description : Get sensor info for that channel
         * Input : channel which to get
         * Output : Sensor_Info
         * ************************************************************
         */
        public Constant.Sensor_Info Get_SensorInfo(int channel)
        {
            Cursor.Current = Cursors.WaitCursor;

            Constant.Sensor_Info info = new Constant.Sensor_Info();
            info.Init();

            Software.serialPort.DiscardInBuffer();
            Software.serialPort.DiscardOutBuffer();

            if (!SendPacket(Packet.DAT_SensorInfo_Packet(channel)))
            {
                MessageEx.Mesaage_Error("Packet Send Fail", "Sending SensorInfo Failed");
            }
            else
            {
                int[] packet = Get_Packet(10);

                if (packet != null)
                {
                    if (Check_Valid(packet, (int)Constant.Serial_flag.DAT))
                    {
                        info.type = packet[5];
                        info.settle = Util.uin8_to_uin16(packet[6], packet[7]);
                        info.excite_start = Util.uin8_to_uin16(packet[8], packet[9]);
                        info.excite_stop = Util.uin8_to_uin16(packet[10], packet[11]);
                        info.cycle = Util.uin8_to_uin16(packet[12], packet[13]);
                        info.equation = packet[14];
                        for (int i = 0; i < 5; i++)
                        {
                            info.cof[i] = Util.int_to_float(packet[15 + i * 4],
                                packet[16 + i * 4], packet[17 + i * 4], packet[18 + i * 4]);
                        }
                        info.compensation = packet[35];
                        info.record_change = Util.int_to_float(packet[36],
                                packet[37], packet[38], packet[39]);
                        info.counter_reset = (packet[40] & (1 << channel)) > 0 ? 1 : 0;
                        info.unit = String.Format("{0}{1}{2}{3}", (char)packet[41], (char)packet[42], (char)packet[43], (char)packet[44]);

                        Software.mainForm.Log_WriteLine(String.Format("Get Sensor Info Channel {0} successfully", channel + 1));
                        Cursor.Current = Cursors.Default;
                        return info;
                    }
                }
            }
            Cursor.Current = Cursors.Default;
            MessageEx.Mesaage_Error("Sensor Info Fail", "Fail to get Sensor Info From Logger");
            return info;
        }

        /* ************************************************************ 
         * Description : Download raw data into txt file
         * Input : date being download
         * Output : void
         * ************************************************************
         */
        public void Receive_RAW_LOG(int year, int month, int date)
        {
            Software.serialPort.DiscardInBuffer();
            Software.serialPort.DiscardOutBuffer();

            SendPacket(Packet.DAT_RED_Packet(year, month, date));

            if (Check_Start(waitSecond[connectOption]))
            {
                Software.serialPort.ReadByte();
                Software.serialPort.ReadByte();

                if (Util.uin8_to_uin16(Software.serialPort.ReadByte(), Software.serialPort.ReadByte()) == requestSerial)
                {
                    if (Software.serialPort.ReadByte() == (int)Constant.Serial_flag.DAT)
                    {
                        Directory.CreateDirectory(String.Format("DownloadData/{0}", loggerInfo.serialNum.ToString()));
                        using (StreamWriter file = new StreamWriter(Environment.CurrentDirectory + string.Format("/DownloadData/{4}/{0}_{1}-{2}-{3}_raw.txt", loggerInfo.serialNum, year, month, date, loggerInfo.serialNum.ToString())))
                        {
                            int count = Software.serialPort.ReadByte() + Software.serialPort.ReadByte() * 256;
                            int download_count = 1;

                            file.WriteLine("Raw Data | Date: {0}/{1}/{2} | Total number of records: {3}", date, month, year, count);
                            file.WriteLine("                      Channel 1      Channel 2      Channel 3      Channel 4      Channel 5      Channel 6      Channel 7      Channel 8");

                            Software.mainForm.Log_WriteLine(String.Format("Downloading Raw Data on {0}/{1}/{2}...", year, month, date));
                            while (true)
                            {
                                int year1 = Software.serialPort.ReadByte();
                                if (year1 != Constant.STOP_BYTE && year1!= (year & 0x00ff))
                                {
                                    continue;
                                }
                                int year2 = Software.serialPort.ReadByte();

                                if (year1 == Constant.STOP_BYTE && year2 == Constant.STOP_BYTE)
                                {
                                    break;
                                }
                                Software.mainForm.downloadProgressbar.Value = download_count * 100 / count;
                                Software.mainForm.progressLabel.Text = String.Format("{0}%", download_count * 100 / count);

                                int month1 = Software.serialPort.ReadByte();
                                int day1 = Software.serialPort.ReadByte();

                                file.Write("{0:00}/{1:00}/", day1, month1);
                                file.Write("{0:0000} ", Util.uin8_to_uin16(year1, year2));
                                file.Write("{0:00}:{1:00}:{2:00} ", Software.serialPort.ReadByte(), Software.serialPort.ReadByte(), Software.serialPort.ReadByte());
                                Software.serialPort.ReadByte();
                                for (int i = 0; i < 8; i++)
                                {
                                    byte[] floatarray = new byte[4];
                                    floatarray[0] = (byte)Software.serialPort.ReadByte();
                                    floatarray[1] = (byte)Software.serialPort.ReadByte();
                                    floatarray[2] = (byte)Software.serialPort.ReadByte();
                                    floatarray[3] = (byte)Software.serialPort.ReadByte();
                                    if (BitConverter.ToSingle(floatarray, 0) > 0.1)
                                    {
                                        file.Write(" {0: 000000.000;-000000.000} | ", Math.Round(BitConverter.ToSingle(floatarray, 0), 3));
                                    }
                                    else
                                    {
                                        file.Write("             | ");
                                    }
                                }
                                file.WriteLine();
                                download_count++;
                            }
                        }
                        Software.mainForm.Log_WriteLine("DownLoad Done");
                        Software.mainForm.progressLabel.Text = "Done";

                        Software.mainForm.downloadProgressbar.Value = 100;
                        Software.mainForm.progressLabel.Text = String.Format("{0}%", 100);
                        return;
                    }
                }
            }
            MessageEx.Mesaage_Error("Download Fail", "Fail to download raw data from Logger");
        }

        /* ************************************************************ 
         * Description : Download raw data and calculate into real data and store into txt file
         * Input : date being download
         * Output : void
         * ************************************************************
         */
        public void Receive_REAL_LOG(int year, int month, int date)
        {
            List<int> recordingChannel = new List<int>();
            for (int i = 0; i < 8; i++)
            {
                if (Software.connection.loggerInfo.sensorInfo[i].type != 0)
                {
                    recordingChannel.Add(i);
                }
            }

            Software.serialPort.DiscardInBuffer();
            Software.serialPort.DiscardOutBuffer();

            // Get all sensor settings
            Get_AllSensors();

            SendPacket(Packet.DAT_RED_Packet(year, month, date));

            if (Check_Start(10))
            {
                Software.serialPort.ReadByte();
                Software.serialPort.ReadByte();
                if (Util.uin8_to_uin16(Software.serialPort.ReadByte(), Software.serialPort.ReadByte()) == requestSerial)
                {
                    if (Software.serialPort.ReadByte() == (int)Constant.Serial_flag.DAT)
                    {
                        Directory.CreateDirectory(String.Format("DownloadData/{0}", loggerInfo.serialNum.ToString()));
                        using (StreamWriter file = new StreamWriter(Environment.CurrentDirectory + string.Format("/DownloadData/{4}/{0}_{1}-{2}-{3}_real.txt", loggerInfo.serialNum, year, month, date, loggerInfo.serialNum.ToString())))
                        {
                            // Write Sensor Settings at the beginning of the file
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Channel {0}", i + 1).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Type: {0}", Constant.sensor_types[Software.connection.loggerInfo.sensorInfo[i].type]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Unit: {0}", Software.connection.loggerInfo.sensorInfo[i].unit).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Settle Time(ms): {0}", Software.connection.loggerInfo.sensorInfo[i].settle).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Sweep Start(Hz): {0}", Software.connection.loggerInfo.sensorInfo[i].excite_start).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Sweep Stop(Hz): {0}", Software.connection.loggerInfo.sensorInfo[i].excite_stop).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Cycles: {0}", Software.connection.loggerInfo.sensorInfo[i].cycle).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Equation: {0}", Constant.equation_types[Software.connection.loggerInfo.sensorInfo[i].equation]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Coef x^4: {0}", Software.connection.loggerInfo.sensorInfo[i].cof[4]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Coef x^3: {0}", Software.connection.loggerInfo.sensorInfo[i].cof[3]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Coef x^2: {0}", Software.connection.loggerInfo.sensorInfo[i].cof[2]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Coef x^1: {0}", Software.connection.loggerInfo.sensorInfo[i].cof[1]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Coef x^0: {0}", Software.connection.loggerInfo.sensorInfo[i].cof[0]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);

                            int count = Software.serialPort.ReadByte() + Software.serialPort.ReadByte() * 256;
                            int download_count = 0;

                            file.Write(Environment.NewLine);
                            file.WriteLine("Real Data | Date: {0}/{1}/{2} | Total number of records: {3}", date, month, year, count);
                            file.WriteLine("                      Channel 1      Channel 2      Channel 3      Channel 4      Channel 5      Channel 6      Channel 7      Channel 8");

                            Constant.Record[] records = new Constant.Record[count];

                            Software.mainForm.Log_WriteLine(String.Format("Downloading Real Data on {0}/{1}/{2}...", year, month, date));
                            while (true)
                            {
                                int year1 = Software.serialPort.ReadByte();
                                if (year1 != Constant.STOP_BYTE && year1 != (year & 0x00ff))
                                {
                                    continue;
                                }
                                int year2 = Software.serialPort.ReadByte();

                                if (year1 == Constant.STOP_BYTE && year2 == Constant.STOP_BYTE)
                                {
                                    break;
                                }

                                Software.mainForm.downloadProgressbar.Value = (download_count + 1) * 100 / count;
                                Software.mainForm.progressLabel.Text = String.Format("{0}%", (download_count + 1) * 100 / count);

                                int month1 = Software.serialPort.ReadByte();
                                int date1 = Software.serialPort.ReadByte();
                                int hour1 = Software.serialPort.ReadByte();
                                int minute1 = Software.serialPort.ReadByte();
                                int second1 = Software.serialPort.ReadByte();
                                Software.serialPort.ReadByte();
                                
                                float[] r = new float[8];
                                for (int i = 0; i < 8; i++)
                                {
                                    byte[] floatarray = new byte[4];
                                    floatarray[0] = (byte)Software.serialPort.ReadByte();
                                    floatarray[1] = (byte)Software.serialPort.ReadByte();
                                    floatarray[2] = (byte)Software.serialPort.ReadByte();
                                    floatarray[3] = (byte)Software.serialPort.ReadByte();
                                    r[i] = BitConverter.ToSingle(floatarray, 0);
                                }

                                records[download_count] = new Constant.Record(Util.uin8_to_uin16(year1, year2), month1, date1, hour1, minute1, second1, r);

                                download_count++;
                            }

                            for (int i = 0; i < download_count; i++)
                            {
                                file.Write("{0:00}/{1:00}/", records[i].day, records[i].month);
                                file.Write("{0:0000} ", records[i].year);
                                file.Write("{0:00}:{1:00}:{2:00} ", records[i].hour, records[i].minute, records[i].second);
                                float[] real = Calculate_Real_Reading(records[i].readings);
                                for (int j = 0; j < 8; j++)
                                {
                                    if (real[j] > 0.1)
                                    {
                                        file.Write(" {0: 000000.000;-000000.000} | ", Math.Round(real[j], 3));
                                    }
                                    else
                                    {
                                        file.Write("             | ");
                                    }
                                }
                                file.WriteLine();
                            }
                        }
                        Software.mainForm.Log_WriteLine("DownLoad Done");
                        Software.mainForm.progressLabel.Text = "Done";

                        Software.mainForm.downloadProgressbar.Value = 100;
                        Software.mainForm.progressLabel.Text = String.Format("{0}%", 100);
                        return;
                    }
                }
            }
            MessageEx.Mesaage_Error("Download Fail", "Fail to download raw data from Logger");
        }

        /* ************************************************************
         * Description : Get all sensors info, skip if already got
         * Input : void
         * Output : Sensor_info[]
         * ************************************************************
         */
        public bool Get_AllSensors()
        {
            Cursor.Current = Cursors.WaitCursor;

            Software.mainForm.Log_WriteLine("Getting sensors info from all channels");
            for (int i = 0; i < 8; i++)
            {
                if (loggerInfo.sensorInfo[i].type != 0)
                {
                    loggerInfo.sensorInfo[i] = Get_SensorInfo(i);

                    if (loggerInfo.sensorInfo[i].type == -1)
                    {
                        return false;
                    }
                    Software.mainForm.Set_Main_Progressbar((i + 1) * 100 / 8);
                }

            }

            Software.mainForm.Set_Main_Progressbar(100);

            Cursor.Current = Cursors.Default;

            return true;
        }

        public bool Get_RadioInfo()
        {
            Cursor.Current = Cursors.WaitCursor;

            Software.serialPort.DiscardInBuffer();
            Software.serialPort.DiscardOutBuffer();
            if (!SendPacket(Packet.DAT_RadioInfo_Packet()))
            {
                MessageEx.Mesaage_Error("Packet Send Fail", "Sending Radio Info Packet Failed");
                return false;
            }
            else
            {
                int[] packet = Get_Packet(10);
                if (packet != null)
                {
                    if (Check_Valid(packet, (int)Constant.Serial_flag.DAT))
                    {
                        loggerInfo.radioHP = packet[5] - 48;
                        loggerInfo.radioID = string.Format("{0}{1}{2}{3}", (char)packet[6], (char)packet[7], 
                            (char)packet[8], (char)packet[9]);

                        Software.mainForm.Log_WriteLine("Get Radio Info successfully");
                        Cursor.Current = Cursors.Default;
                        return true;
                    }
                }
            }
            Cursor.Current = Cursors.Default;
            MessageEx.Mesaage_Error("Radio Info Fail", "Fail to get radio Info From Logger");
            return false;
        }

        /* ************************************************************
         * Description : Calculate raw data into real data
         * Input : float[] raw data
         * Output : float[] real data
         * ************************************************************
         */
        private float[] Calculate_Real_Reading(float[] rawReading)
        {
            float[] real = new float[8];
            for (int i = 0; i < 8; i++)
            {
                if (loggerInfo.sensorInfo[i].compensation == 0)
                {
                    switch (loggerInfo.sensorInfo[i].equation)
                    {
                        case 0:
                            real[i] = rawReading[i];
                            break;
                        case 1:
                            real[i] = Calculation.Polynomial(loggerInfo.sensorInfo[i].cof, rawReading[i], 0);
                            break;
                        case 2:
                            real[i] = (float)Calculation.Steinhart(loggerInfo.sensorInfo[i].cof, rawReading[i], 0);
                            break;
                    }
                }
            }

            for (int i = 0; i < 8; i++)
            {
                if (loggerInfo.sensorInfo[i].compensation != 0)
                {
                    switch (loggerInfo.sensorInfo[i].equation)
                    {
                        case 0:
                            real[i] = rawReading[i];
                            break;
                        case 1:
                            real[i] = Calculation.Polynomial(loggerInfo.sensorInfo[i].cof, rawReading[i], real[loggerInfo.sensorInfo[i].compensation - 1]);
                            break;
                        case 2:
                            real[i] = (float)Calculation.Steinhart(loggerInfo.sensorInfo[i].cof, rawReading[i], real[loggerInfo.sensorInfo[i].compensation - 1]);
                            break;
                    }
                }
            }

            return real;
        }

        /* ************************************************************
         * Description : Get complete packet from logger
         * Input : void
         * Output : int[] packet
         * ************************************************************
         */
        public int[] Get_Packet(int timeOutSecond)
        {
            int[] current_packet = new int[80];
            int write_pos = 0;

            if (!Check_Start(timeOutSecond))
            {
                return null;
            }

            int current_byte = Software.serialPort.ReadByte();
            int next_byte = -1;

            while (true)
            {
                current_packet[write_pos] = current_byte;
                write_pos++;

                if (next_byte != -1)
                {
                    current_packet[write_pos] = next_byte;
                    write_pos++;
                    next_byte = -1;
                }

                current_byte = Software.serialPort.ReadByte();

                if (current_byte == Constant.STOP_BYTE)
                {
                    next_byte = Software.serialPort.ReadByte();

                    if (next_byte == Constant.STOP_BYTE)
                    {
                        break;
                    }

                }
            }

            return current_packet;
        }

        /* ************************************************************ 
         * Description : Send packet to logger
         * Input : Constant.Packet_Info
         * Output : true if success
         * ************************************************************
         */
        public bool SendPacket(Constant.Packet_Info info)
        {
            if (Software.serialPort != null)
            {
                try
                {
                    byte[] FROM_TO_BYTE = new byte[] { (byte)(requestSerial & 0x00ff), (byte)(requestSerial >> 8 & 0x00ff), 0, 0 };
                    Software.serialPort.Write(Constant.START_BYTE_ARY, 0, 2);
                    Software.serialPort.Write(FROM_TO_BYTE, 0, 4);
                    Software.serialPort.Write(info.data, info.offset, info.count);
                    Software.serialPort.Write(Constant.STOP_BYTE_ARY, 0, 2);
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }
            else
            {
                Software.mainForm.Log_WriteLine("No Connected Port!");
                return false;
            }
        }

        /* ************************************************************
         * Description : Check the start of a packet, timeout 10s
         * Input : void
         * Output : true if successfully receive the head of packet
         * ************************************************************
         */
        private bool Check_Start(int timeOutSecond)
        {
            int timer = 0;

            do
            {
                Thread.Sleep(5);

                if (timeOutSecond > 0)
                {
                    if (timer > 200 * timeOutSecond)
                    {
                        Software.mainForm.Log_WriteLine("Receive Packet Time Out");
                        return false;
                    }

                    timer++;
                }
            } while (Software.serialPort.BytesToRead == 0);

            do
            {
                Thread.Sleep(5);

                if (timeOutSecond > 0)
                {
                    if (timer > 200 * timeOutSecond)
                    {
                        Software.mainForm.Log_WriteLine("Receive Packet Time Out");
                        return false;
                    }
                    timer++;
                }
            } while (Software.serialPort.ReadByte() != Constant.START_BYTE);

            do
            {
                Thread.Sleep(5);

                if (timeOutSecond > 0)
                {
                    if (timer > 200 * timeOutSecond)
                    {
                        Software.mainForm.Log_WriteLine("Receive Packet Time Out");
                        return false;
                    }
                    timer++;
                }
            } while (Software.serialPort.ReadByte() != Constant.START_BYTE);

            return true;
        }

        /* ************************************************************
         * Description : Check packet is valid
         * Input : void
         * Output : true if valid
         * ************************************************************
         */
        public bool Check_Valid(int[] packet, int flag)
        {
            if (packet[0] != 0 || packet[1] != 0)
            {
                return false;
            }
            if (Util.uin8_to_uin16(packet[2], packet[3]) != requestSerial)
            {
                return false;
            }
            if (packet[4] != flag)
            {
                return false;
            }

            return true;
        }
    }

}
