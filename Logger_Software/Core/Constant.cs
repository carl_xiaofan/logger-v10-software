﻿using System;

namespace LoggerSoftware.Core
{
    class Constant
    {
        public static byte START_BYTE = 0xB2; //10101010
        public static byte STOP_BYTE = 0xDC;  //01010101

        public static byte[] START_BYTE_ARY = new byte[] { START_BYTE, START_BYTE };
        public static byte[] STOP_BYTE_ARY = new byte[] { STOP_BYTE, STOP_BYTE };

        public static string[] sensor_types = new string[6] { 
            "Unused", "Vibrating Wire", "Resistance", "Voltage", "Frequency", "Counter" };
        public static string[] equation_types = new string[3] { 
            "Raw Data", "Polynomial", "SteinhartHart" };

        public enum Serial_flag
        {
            FIN = 0x01,
            SYN = 0x02,
            ACK = 0x04,
            ACK_SYN = 0x06,
            SET = 0x08,
            DAT = 0x10,
            FOR = 0x20,
        };

        public enum Set_flag
        {
            SER = 0x01,
            FWV = 0x02,
            RTC = 0x03,
            BUILD_DATE = 0x04,
            CHECK_DATE = 0x05,
            MODE = 0x06,
            ROLE = 0x07,
            SENSOR_TYPE = 0X08,
            SENESOR_SETTLE = 0X09,
            SENSOR_START = 0X0A,
            SENSOR_STOP = 0x0B,
            SENSOR_CYCLES = 0x0C,
            SENSOR_INTERVAL = 0x0D,
            SENSOR_EQUATION = 0x0E,
            SENSOR_COF = 0x0F,
            SENSOR_COMP = 0x10,
            SENSOR_COMPCHAN = 0x11,
            COUNTER_RESET = 0x12,
            SAMPLE_CHANGE = 0x13,
            SENSOR_UNIT = 0x14,
            RESET_SENSOR = 0x15,
            RESET_SD = 0x16,
            SLAVE_LIST = 0x17,
            SLAVE_NUMBER = 0x18,
            SENSOR = 0x19,
            ENABLE_RECORD = 0x1A,
            RECORD = 0x1B,
            ROLE_INFO = 0x1C,
            SYSTEM_INFO = 0x1D,
            RADIO_HPID = 0x1E,
        };

        public enum Dat_flag
        {
            RTM = 0x01,
            RED = 0x02,
            LST = 0x03,
            SENSOR_INFO = 0x04,
            RECORD_INFO = 0x05,
            SLST = 0x06,
            ROLE_INFO = 0x07,
            RADIO_DAT = 0x08,
        };

        public struct Packet_Info
        {
            public byte[] data;
            public int offset, count;

            public Packet_Info(byte[] d, int o, int c)
            {
                data = d;
                offset = o;
                count = c;
            }
        }

        public struct Record
        {
            public int year;
            public int month;
            public int day;
            public int hour;
            public int minute;
            public int second;
            public float[] readings;

            public Record(int y, int m, int d, int h, int mm, int s, float[] record_readings)
            {
                year = y;
                month = m;
                day = d;
                hour = h;
                minute = mm;
                second = s;
                readings = record_readings;
            }
        }

        public struct Logger_Info
        {
            public int serialNum;
            public int firmwareNum;
            public int role;
            public DateTime lastSet;
            public DateTime buildDate;
            public DateTime checkPoint;
            public Sensor_Info[] sensorInfo;
            public int[] slaveList;
            public int slaveNumber;
            public int measureInterval;
            public int enableRecord;
            public int enableSynchronize;
            public int enableFastread;
            public float[] rocValue;
            public int radioHP;
            public string radioID;
            public int slaveMode;

            public Logger_Info(int serial = 0)
            {
                serialNum = serial;
                firmwareNum = 0;
                role = 0;
                lastSet = new DateTime();
                buildDate = new DateTime();
                checkPoint = new DateTime();
                sensorInfo = new Sensor_Info[8];
                slaveList = new int[5];
                slaveNumber = 0;
                measureInterval = 0;
                enableRecord = 0;
                enableSynchronize = 0;
                enableFastread = 0;
                rocValue = new float[8];
                radioHP = 0;
                radioID = "";
                slaveMode = 0;
            }

            public void Init()
            {
                serialNum = -1;
                firmwareNum = -1;
                role = -1;
                lastSet = new DateTime();
                buildDate = new DateTime();
                checkPoint = new DateTime();
                sensorInfo = new Sensor_Info[8];
                for (int i = 0; i < 8; i++)
                {
                    sensorInfo[i].Init();
                }
                slaveList = new int[5];
                slaveNumber = -1;
                measureInterval = -1;
                enableRecord = -1;
                enableSynchronize = -1;
                enableFastread = -1;
                rocValue = new float[8];
                radioHP = -1;
                radioID = "";
                slaveMode = -1;
            }
        }

        public struct Sensor_Info
        {
            public int type;
            public int settle;
            public int excite_start;
            public int excite_stop;
            public int cycle;
            public int equation;
            public string unit;
            public float[] cof;
            public int compensation;
            public float record_change;
            public int counter_reset;

            public Sensor_Info(int t = 0)
            {
                type = 0;
                settle = 0;
                excite_start = 0;
                excite_stop = 0;
                cycle = 0;
                equation = 0;
                compensation = 0;
                record_change = 0;
                counter_reset = 0;
                unit = "";
                cof = new float[5];
            }

            public void Init()
            {
                type = -1;
                settle = 40;
                excite_start = 1600;
                excite_stop = 3200;
                cycle = 250;
                equation = 0;
                compensation = 0;
                record_change = 0;
                counter_reset = 0;
                unit = "Null";
                cof = new float[5];
            }
        }
    }
}
