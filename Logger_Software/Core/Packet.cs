﻿using System;

namespace LoggerSoftware.Core
{
    class Packet
    {
        public static Constant.Packet_Info SYN_Packet()
        {
            byte[] packet = new byte[1];
            packet[0] = (byte)Constant.Serial_flag.SYN;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 1);

            return info;
        }

        public static Constant.Packet_Info ACK_SYN_Packet()
        {
            byte[] packet = new byte[1];
            packet[0] = (byte)Constant.Serial_flag.ACK_SYN;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 1);

            return info;
        }

        public static Constant.Packet_Info SET_Serial_Packet(int serialNum)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SER;
            packet[2] = (byte)(serialNum & 0x00ff);
            packet[3] = (byte)(serialNum >> 8 & 0x00ff);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 4);

            return info;
        }

        public static Constant.Packet_Info SET_FW_Packet(int firmwareVersion)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.FWV;
            packet[2] = (byte)(firmwareVersion & 0x00ff);
            packet[3] = (byte)(firmwareVersion >> 8 & 0x00ff);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 4);

            return info;
        }

        public static Constant.Packet_Info SET_Time_Packet(int option, int year, int month, int day, int hour, int minute, int second)
        {
            DateTime dt = new DateTime(year, month, day, hour, minute, second);
            byte[] packet = new byte[9];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)option;
            packet[2] = (byte)(dt.Year & 0x00ff);
            packet[3] = (byte)(dt.Year >> 8 & 0x00ff);
            packet[4] = (byte)(dt.Month);
            packet[5] = (byte)(dt.Day);
            packet[6] = (byte)(dt.Hour);
            packet[7] = (byte)(dt.Minute);
            packet[8] = (byte)(dt.Second);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 9);

            return info;
        }

        public static Constant.Packet_Info SET_RTC_Packet()
        {
            DateTime dt = DateTime.Now;
            byte[] packet = new byte[13];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.RTC;
            packet[2] = (byte)(dt.Year & 0x00ff);
            packet[3] = (byte)(dt.Year >> 8 & 0x00ff);
            packet[4] = (byte)(dt.Month);
            packet[5] = (byte)(dt.Day);
            packet[6] = (byte)(dt.Hour);
            packet[7] = (byte)(dt.Minute);
            packet[8] = (byte)(dt.Second);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 9);

            return info;
        }

        public static Constant.Packet_Info SET_Sensor_Type_Packet(int channel, int type)
        {
            byte[] packet = new byte[8];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_TYPE;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(type);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 4);

            return info;
        }

        public static Constant.Packet_Info SET_VW_Packet(int flag, int channel, int data)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)flag;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(data & 0x00ff);
            packet[4] = (byte)(data >> 8 & 0x00ff);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 4);

            return info;
        }

        public static Constant.Packet_Info SET_Sensor_Equation_Packet(int channel, int equation)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_EQUATION;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(equation);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 4);

            return info;
        }

        public static Constant.Packet_Info SET_Sensor_COF_Packet(int channel, int cof, float value)
        {
            byte[] f = BitConverter.GetBytes(value);

            byte[] packet = new byte[8];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_COF;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(cof);
            packet[4] = f[0];
            packet[5] = f[1];
            packet[6] = f[2];
            packet[7] = f[3];

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 8);

            return info;
        }

        public static Constant.Packet_Info SET_Interval_Packet(int interval)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_INTERVAL;
            packet[2] = (byte)(interval & 0x00ff);
            packet[3] = (byte)(interval >> 8 & 0x00ff);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 4);

            return info;
        }

        public static Constant.Packet_Info SET_RESET_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.RESET_SENSOR;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 2);

            return info;
        }

        public static Constant.Packet_Info SET_RESETSD_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.RESET_SD;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 2);

            return info;
        }

        public static Constant.Packet_Info SET_MODE_Packet(int mode)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.MODE;
            packet[2] = (byte)mode;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 3);

            return info;
        }

        public static Constant.Packet_Info SET_Role_Packet(int role)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.ROLE;
            packet[2] = (byte)role;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 3);

            return info;
        }

        public static Constant.Packet_Info SET_Counter_Reset_Packet(int channel, int reset)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.COUNTER_RESET;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(reset);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 4);

            return info;
        }

        public static Constant.Packet_Info SET_Sample_Change_Packet(int channel, float value)
        {
            byte[] f = BitConverter.GetBytes(value);

            byte[] packet = new byte[7];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SAMPLE_CHANGE;
            packet[2] = (byte)(channel);
            packet[3] = f[0];
            packet[4] = f[1];
            packet[5] = f[2];
            packet[6] = f[3];

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 7);

            return info;
        }

        public static Constant.Packet_Info SET_Compensated_Packet(int channel, int enable)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_COMP;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(enable);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 4);

            return info;
        }

        public static Constant.Packet_Info SET_Compensation_Packet(int channel, int sourec)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_COMPCHAN;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(sourec);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 4);

            return info;
        }

        public static Constant.Packet_Info SET_Unit_Packet(int channel, string unit)
        {
            byte[] packet = new byte[7];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_UNIT;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(unit[0]);
            packet[4] = (byte)(unit[1]);
            packet[5] = (byte)(unit[2]);
            packet[6] = (byte)(unit[3]);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 7);

            return info;
        }

        public static Constant.Packet_Info SET_Slave_List_Packet(int index, int serial)
        {
            byte[] packet = new byte[5];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SLAVE_LIST;
            packet[2] = (byte)(index);
            packet[3] = (byte)(serial & 0x00ff);
            packet[4] = (byte)(serial >> 8 & 0x00ff);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 5);

            return info;
        }

        public static Constant.Packet_Info SET_Slave_Number_Packet(int slave_number)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SLAVE_NUMBER;
            packet[2] = (byte)slave_number;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 3);

            return info;
        }

        public static Constant.Packet_Info SET_Sensor_Packet(int channel, Constant.Sensor_Info sensor_info)
        {
            byte[] packet = new byte[39];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(sensor_info.type);
            packet[4] = (byte)(sensor_info.unit[0]);
            packet[5] = (byte)(sensor_info.unit[1]);
            packet[6] = (byte)(sensor_info.unit[2]);
            packet[7] = (byte)(sensor_info.unit[3]);
            packet[8] = (byte)(sensor_info.settle & 0x00ff);
            packet[9] = (byte)(sensor_info.settle >> 8 & 0x00ff);
            packet[10] = (byte)(sensor_info.cycle & 0x00ff);
            packet[11] = (byte)(sensor_info.cycle >> 8 & 0x00ff);
            packet[12] = (byte)(sensor_info.excite_start & 0x00ff);
            packet[13] = (byte)(sensor_info.excite_start >> 8 & 0x00ff);
            packet[14] = (byte)(sensor_info.excite_stop & 0x00ff);
            packet[15] = (byte)(sensor_info.excite_stop >> 8 & 0x00ff);
            packet[16] = (byte)(sensor_info.equation);
            for (int i = 0; i < 5; i++)
            {
                byte[] f = BitConverter.GetBytes(sensor_info.cof[i]);
                packet[17 + i * 4] = f[0];
                packet[18 + i * 4] = f[1];
                packet[19 + i * 4] = f[2];
                packet[20 + i * 4] = f[3];
            }
            packet[37] = (byte)(sensor_info.compensation);
            packet[38] = (byte)(sensor_info.counter_reset);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 39);

            return info;
        }

        public static Constant.Packet_Info SET_EnableRecord_Packet(int enable)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.ENABLE_RECORD;
            packet[2] = (byte)enable;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 3);

            return info;
        }

        public static Constant.Packet_Info SET_Record_Packet(int enable_record, int enable_synchronize, int measure_interval, int enableFastread, float[] roc_values)
        {
            byte[] packet = new byte[39];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.RECORD;
            packet[2] = (byte)(enable_record);
            packet[3] = (byte)(enable_synchronize);
            packet[4] = (byte)(measure_interval & 0x00ff);
            packet[5] = (byte)(measure_interval >> 8 & 0x00ff);
            packet[6] = (byte)(enableFastread);
            for (int i = 0; i < 8; i++)
            {
                byte[] f = BitConverter.GetBytes(roc_values[i]);
                packet[7 + i * 4] = f[0];
                packet[8 + i * 4] = f[1];
                packet[9 + i * 4] = f[2];
                packet[10 + i * 4] = f[3];
            }

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 39);

            return info;
        }

        public static Constant.Packet_Info SET_RoleInfo_Packet(int role, int slaveMode, int slaveNumber, int[] slaveList)
        {
            byte[] packet = new byte[15];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.ROLE_INFO;
            packet[2] = (byte)(role);
            packet[3] = (byte)(slaveMode);
            packet[4] = (byte)(slaveNumber);
            for (int i = 0; i < 5; i++)
            {
                packet[5 + i * 2] = (byte)(slaveList[i] & 0x00ff);
                packet[6 + i * 2] = (byte)(slaveList[i] >> 8 & 0x00ff);
            }

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 15);

            return info;
        }

        public static Constant.Packet_Info SET_SystemInfo_Packet(int serialNumber, int firmwareVersion, DateTime buildDate, DateTime lastSetDate, DateTime checkPoint, int mode)
        {
            byte[] packet = new byte[28];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SYSTEM_INFO;
            packet[2] = (byte)(serialNumber & 0x00ff);
            packet[3] = (byte)(serialNumber >> 8 & 0x00ff);
            packet[4] = (byte)(firmwareVersion & 0x00ff);
            packet[5] = (byte)(firmwareVersion >> 8 & 0x00ff);
            packet[6] = (byte)(buildDate.Year & 0x00ff);
            packet[7] = (byte)(buildDate.Year >> 8 & 0x00ff);
            packet[8] = (byte)(buildDate.Month);
            packet[9] = (byte)(buildDate.Day);
            packet[10] = (byte)(buildDate.Hour);
            packet[11] = (byte)(buildDate.Minute);
            packet[12] = (byte)(buildDate.Second);
            packet[13] = (byte)(lastSetDate.Year & 0x00ff);
            packet[14] = (byte)(lastSetDate.Year >> 8 & 0x00ff);
            packet[15] = (byte)(lastSetDate.Month);
            packet[16] = (byte)(lastSetDate.Day);
            packet[17] = (byte)(lastSetDate.Hour);
            packet[18] = (byte)(lastSetDate.Minute);
            packet[19] = (byte)(lastSetDate.Second);
            packet[20] = (byte)(checkPoint.Year & 0x00ff);
            packet[21] = (byte)(checkPoint.Year >> 8 & 0x00ff);
            packet[22] = (byte)(checkPoint.Month);
            packet[23] = (byte)(checkPoint.Day);
            packet[24] = (byte)(checkPoint.Hour);
            packet[25] = (byte)(checkPoint.Minute);
            packet[26] = (byte)(checkPoint.Second);
            packet[27] = (byte)(mode);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 28);

            return info;
        }

        public static Constant.Packet_Info SET_Radio_HPID(int hp, string id)
        {
            byte[] packet = new byte[7];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.RADIO_HPID;
            packet[2] = (byte)(hp);
            packet[3] = (byte)(id[0]);
            packet[4] = (byte)(id[1]);
            packet[5] = (byte)(id[2]);
            packet[6] = (byte)(id[3]);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 7);

            return info;
        }

        public static Constant.Packet_Info DAT_RTM_Packet(int rtm)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.RTM;
            packet[2] = (byte)(rtm);

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 3);

            return info;
        }

        public static Constant.Packet_Info DAT_RED_Packet(int year, int month, int day)
        {
            byte[] packet = new byte[6];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.RED;
            packet[2] = (byte)(year & 0x00ff);
            packet[3] = (byte)(year >> 8 & 0x00ff);
            packet[4] = (byte)month;
            packet[5] = (byte)day;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 6);

            return info;
        }

        public static Constant.Packet_Info DAT_LST_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.LST;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 2);

            return info;
        }

        public static Constant.Packet_Info DAT_SensorInfo_Packet(int channel)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.SENSOR_INFO;
            packet[2] = (byte)channel;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 3);

            return info;
        }

        public static Constant.Packet_Info DAT_SLST_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.SLST;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 2);

            return info;
        }

        public static Constant.Packet_Info DAT_RecordInfo_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.RECORD_INFO;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 2);

            return info;
        }

        public static Constant.Packet_Info DAT_RoleInfo_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.ROLE_INFO;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 2);

            return info;
        }

        public static Constant.Packet_Info DAT_RadioInfo_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.RADIO_DAT;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 2);

            return info;
        }

    }
}
