﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using LoggerSoftware.Core;
using System.Drawing;
using System.Windows.Forms;

namespace LoggerSoftware.DataTab
{
    class DownLoadTab
    {
        public List<int[]> menu;

        public DownLoadTab()
        {

        }

        /* ************************************************************ 
         * Description : Initialize the Download Tab
         * Input : void
         * Output : true if success
         * ************************************************************
         */
        public bool DownLoadTab_Init()
        {
            if (Software.connection == null)
            {
                Software.mainForm.Log_WriteLine(String.Format("Connection not established"));
                return false;
            }

            Software.mainForm.Log_WriteLine(String.Format("Download Tab successfully Initialized"));


            Software.mainForm.progressLabel.Visible = false;
            Software.mainForm.downloadProgressbar.Visible = false;

            if (!Software.mainForm.realCheck.Checked)
            {
                Software.mainForm.rawCheck.Checked = true;
            }

            return true;
        }

        /* ************************************************************ 
         * Description : Get Menu Button Click Event, get menu from logger and display
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void Getmenu_Button_Click(object sender, EventArgs e)
        {
            // Get Menu from Logger
            Software.connection.SendPacket(Packet.DAT_LST_Packet());

            menu = Software.connection.Receive_Menu();

            menu = menu.OrderBy(r => -r[0]).ThenBy(r => -r[1]).ThenBy(r => -r[2]).ToList();

            // Clear panel
            Software.mainForm.menuPanel.Controls.Clear();
            
            // Display in panel
            foreach (int[] date in menu)
            {
                Button button = new Button();
                button.Text = String.Format("{0}/{1}/{2}", date[0], date[1], date[2]);
                button.BackColor = SystemColors.Menu;
                button.Font = new Font("Arial", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                button.Size = new Size(132, 30);
                Software.mainForm.menuPanel.Controls.Add(button);
                button.Click += new EventHandler(Menubutton_Click);
            }

            Software.mainForm.Log_WriteLine(String.Format("Receive menu successfully"));
        }

        public void Resetmenu_Button_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure to clear the whole memory?\n\rData can not be recovered after this.", "Clear Memory", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                // Get Menu from Logger
                Software.connection.SendPacket(Packet.SET_RESETSD_Packet());

                Thread.Sleep(1000);
                // Get Menu from Logger
                Software.connection.SendPacket(Packet.DAT_LST_Packet());
                List<int[]> menu = Software.connection.Receive_Menu();

                menu = menu.OrderBy(r => -r[0]).ThenBy(r => -r[1]).ThenBy(r => -r[2]).ToList();

                // Clear panel
                Software.mainForm.menuPanel.Controls.Clear();

                // Display in panel
                foreach (int[] date in menu)
                {
                    Button button = new Button();
                    button.Text = String.Format("{0}/{1}/{2}", date[0], date[1], date[2]);
                    button.BackColor = SystemColors.Menu;
                    button.Font = new Font("Arial", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                    button.Size = new Size(132, 30);
                    Software.mainForm.menuPanel.Controls.Add(button);
                    button.Click += new EventHandler(Menubutton_Click);
                }

                Software.mainForm.Log_WriteLine(String.Format("Receive menu successfully"));
            }
        }

        /* ************************************************************ 
         * Description : Raw Checkbox Click Event
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void Raw_CheckBox_Click(object sender, EventArgs e)
        {
            Software.mainForm.realCheck.Checked = false;
        }

        /* ************************************************************ 
         * Description : Real Checkbox Click Event
         * Input : event
         * Output : void
         * ************************************************************
         */
        public void Real_CheckBox_Click(object sender, EventArgs e)
        {
            Software.mainForm.rawCheck.Checked = false;
        }

        /* ************************************************************ 
         * Description : Date Button in panel click event, download data for that date
         * Input : event
         * Output : void
         * ************************************************************
         */
        private void Menubutton_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;
            int year = Int32.Parse(bt.Text.Split('/')[0]);
            int month = Int32.Parse(bt.Text.Split('/')[1]);
            int day = Int32.Parse(bt.Text.Split('/')[2]);

            // Raw data download
            if (Software.mainForm.rawCheck.Checked)
            {
                Software.mainForm.progressLabel.Visible = true;
                Software.mainForm.downloadProgressbar.Visible = true;
                Software.connection.Receive_RAW_LOG(year, month, day);
            }
            // Real data download
            if (Software.mainForm.realCheck.Checked)
            {
                Software.mainForm.progressLabel.Visible = true;
                Software.mainForm.downloadProgressbar.Visible = true;
                Software.connection.Receive_REAL_LOG(year, month, day);
            }
        }

        public void CheckPointbutton_Click(object sender, EventArgs e)
        {
            if (menu == null)
            {
                Software.connection.SendPacket(Packet.DAT_LST_Packet());

                menu = Software.connection.Receive_Menu();

                menu = menu.OrderBy(r => -r[0]).ThenBy(r => -r[1]).ThenBy(r => -r[2]).ToList();

                // Clear panel
                Software.mainForm.menuPanel.Controls.Clear();

                // Display in panel
                foreach (int[] date in menu)
                {
                    Button button = new Button();
                    button.Text = String.Format("{0}/{1}/{2}", date[0], date[1], date[2]);
                    button.BackColor = SystemColors.Menu;
                    button.Font = new Font("Arial", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                    button.Size = new Size(132, 30);
                    Software.mainForm.menuPanel.Controls.Add(button);
                    button.Click += new EventHandler(Menubutton_Click);
                }

                Software.mainForm.Log_WriteLine(String.Format("Receive menu successfully"));
            }

            foreach (int[] date in menu)
            {
                DateTime t = new DateTime(date[0], date[1], date[2], 23, 59, 59);
                if (Software.connection.loggerInfo.checkPoint < t)
                {
                    if (Software.mainForm.rawCheck.Checked)
                    {
                        Software.mainForm.progressLabel.Visible = true;
                        Software.mainForm.downloadProgressbar.Visible = true;
                        Software.connection.Receive_RAW_LOG(date[0], date[1], date[2]);
                    }
                    // Real data download
                    if (Software.mainForm.realCheck.Checked)
                    {
                        Software.mainForm.progressLabel.Visible = true;
                        Software.mainForm.downloadProgressbar.Visible = true;
                        Software.connection.Receive_REAL_LOG(date[0], date[1], date[2]);
                    }
                }
                Thread.Sleep(1000);
            }

            DateTime now = DateTime.Now;
            Software.connection.SendPacket(Packet.SET_Time_Packet((int)Constant.Set_flag.CHECK_DATE, now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second));
            Software.mainForm.Log_WriteLine("Check Point Updated");
        }
    }
}
