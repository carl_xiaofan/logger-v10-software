﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO.Ports;
using LoggerSoftware.Core;

namespace LoggerSoftware
{
    public partial class Connection_com_form : Form
    {
        private int connectionOption;

        private int serialNumber;
        public Connection_com_form(int option)
        {
            connectionOption = option;
            InitializeComponent();

            if (connectionOption == 0)
            {
                serialNumberLabel.Visible = false;
                serialNumberTextbox.Visible = false;
                serialNumber = 0;
            }

            Scan_ports();
        }

        private void rescan_button_Click(object sender, EventArgs e)
        {
            Scan_ports();
        }

        private void Scan_ports()
        {
            comPanel.Controls.Clear();
            foreach (String portName in SerialPort.GetPortNames())
            {
                Button button = new Button();
                button.Text = portName;
                button.BackColor = SystemColors.Menu;
                button.Font = new Font("Arial", 11.25F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                button.Size = new Size(132, 38);
                button.Click += new EventHandler(com_button_Click);
                comPanel.Controls.Add(button);
            }
        }

        private void com_button_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            Button button = (sender as Button);
            String portname = button.Text;

            if (connectionOption == 1 || connectionOption == 2)
            {
                if (Util.valid_int(serialNumberTextbox.Text, 0, 65535))
                {
                    serialNumber = Int32.Parse(serialNumberTextbox.Text);
                }
                else
                {
                    MessageEx.Mesaage_Error("Connection Fail", "Please enter a valid serial number");
                    return;
                }
            }

            Software.mainForm.Log_WriteLine("Trying to connect to " + portname);

            Connection connection = new Connection(portname, connectionOption, serialNumber);

            if (connection.connected)
            {
                Software.connection = connection;
                this.Close();
            }

            Cursor.Current = Cursors.Default;
        }
    }
}
