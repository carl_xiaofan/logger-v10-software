﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using LoggerSoftware.Core;
using System.Diagnostics;

namespace LoggerSoftware
{
    public partial class Main_form : Form
    {

        private TabControl currentSubtab;

        public Process virtualProcess;

        public Main_form()
        {
            InitializeSensorTab();
            InitializeClockTab();
            InitializeRoleTab();
            InitializeRecordSettingTab();
            InitializeRadioTab();
            InitializeSystemTab();
            InitializeDownloadTab();
            InitializeComponent();

            this.FormClosing += new FormClosingEventHandler(Closing_Event);
        }

        public void Log_WriteLine(String text)
        {
            logConsole.AppendText(text);
            logConsole.AppendText(Environment.NewLine);
        }

        public void Log_Write(String text)
        {
            logConsole.AppendText(text);
        }

        public void Set_Main_Progressbar(int value)
        {
            mainProgressbar.Value = value;
        }

        private void Main_form_Load(object sender, EventArgs e)
        {

        }

        private void Menu_connection_com_Click(object sender, EventArgs e)
        {
            Form connectionComForm = new Connection_com_form(0);
            connectionComForm.ShowDialog();
        }

        private void Menu_connection_rs232_Click(object sender, EventArgs e)
        {
            Form connectionComForm = new Connection_com_form(1);
            connectionComForm.ShowDialog();
        }

        private void Sensor_Button_Click(object sender, EventArgs e)
        {
            if (currentSubtab == null)
            {
                if (Software.sensorTab.SensorTab_Init())
                {
                    currentSubtab = sensorTab;
                    currentSubtab.Visible = true;
                }
            }
            else
            {
                if (Software.sensorTab.SensorTab_Init())
                {
                    currentSubtab.Visible = false;
                    currentSubtab = sensorTab;
                    currentSubtab.Visible = true;
                }
            }
        }

        private void Clock_Button_Click(object sender, EventArgs e)
        {
            if (currentSubtab == null)
            {
                if (Software.clockTab.ClockTab_Init())
                {
                    currentSubtab = clockTab;
                    currentSubtab.Visible = true;
                }
            }
            else
            {
                if (Software.clockTab.ClockTab_Init())
                {
                    currentSubtab.Visible = false;
                    currentSubtab = clockTab;
                    currentSubtab.Visible = true;
                }
            }
        }

        private void Role_Button_Click(object sender, EventArgs e)
        {
            if (currentSubtab == null)
            {
                if (Software.roleTab.RoleTab_Init())
                {
                    currentSubtab = roleTab;
                    currentSubtab.Visible = true;
                }
            }
            else
            {
                if (Software.roleTab.RoleTab_Init())
                {
                    currentSubtab.Visible = false;
                    currentSubtab = roleTab;
                    currentSubtab.Visible = true;
                }
            }
        }

        private void Record_Button_Click(object sender, EventArgs e)
        {
            if (currentSubtab == null)
            {
                if (Software.recordsettingTab.RecordSettingTab_Init())
                {
                    currentSubtab = recordTab;
                    currentSubtab.Visible = true;
                }
            }
            else
            {
                if (Software.recordsettingTab.RecordSettingTab_Init())
                {
                    currentSubtab.Visible = false;
                    currentSubtab = recordTab;
                    currentSubtab.Visible = true;
                }
            }
        }

        private void Downloaddata_Button_Click(object sender, EventArgs e)
        {
            if (currentSubtab == null)
            {
                if (Software.menudownloadTab.DownLoadTab_Init())
                {
                    currentSubtab = downloadTab;
                    currentSubtab.Visible = true;
                }
            }
            else
            {
                if (Software.menudownloadTab.DownLoadTab_Init())
                {
                    currentSubtab.Visible = false;
                    currentSubtab = downloadTab;
                    currentSubtab.Visible = true;
                }
            }
        }

        private void Radio_Button_Click(object sender, EventArgs e)
        {
            if (currentSubtab == null)
            {
                if (Software.radioTab.RadioTab_Init())
                {
                    currentSubtab = radioTab;
                    currentSubtab.Visible = true;
                }
            }
            else
            {
                if (Software.radioTab.RadioTab_Init())
                {
                    currentSubtab.Visible = false;
                    currentSubtab = radioTab;
                    currentSubtab.Visible = true;
                }
            }
        }


        private void SystemButton_Click(object sender, EventArgs e)
        {
            if (currentSubtab == null)
            {
                if (Software.systemTab.SystemTab_Init())
                {
                    currentSubtab = systemTab;
                    currentSubtab.Visible = true;
                }
            }
            else
            {
                if (Software.systemTab.SystemTab_Init())
                {
                    currentSubtab.Visible = false;
                    currentSubtab = systemTab;
                    currentSubtab.Visible = true;
                }
            }
        }

        private void Realtime_button_Click(object sender, EventArgs e)
        {
            if (Software.connection == null)
            {
                MessageEx.Mesaage_Error("Connection not established", "Please connect to at least one logger");
                return;
            }
            for (int i = 0; i < 8; i ++)
            {
                if (Software.connection.loggerInfo.sensorInfo[i].type != 0)
                {
                    Software.realtimeForm = new RealTime_form();
                    Software.realtimeForm.ShowDialog();
                    return;
                }
            }
            MessageEx.Mesaage_Error("Fail to start Real Time Mode", "None of the channel is in use");
        }

        private void hideShowCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (hideShowCheckbox.Checked)
            {
                logConsole.Visible = false;
                hideShowCheckbox.Text = "Hide Log";
                this.Width = 822;
                this.Height = 507;
            }
            else
            {
                logConsole.Visible = true;
                hideShowCheckbox.Text = "Hide Log";
                this.Width = 822;
                this.Height = 693;
            }
        }

        private void Closing_Event(object sender, FormClosingEventArgs e)
        {
            if (virtualProcess != null)
            {
                virtualProcess.CloseMainWindow();
                virtualProcess.Close();
            }
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form virtualForm = new Virtual_form();
            virtualForm.ShowDialog();
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Software.connection != null)
            {
                Software.connection.Disconnect();
                Software.serialPort = null;
                Software.connection = null;
            }
            
            if (currentSubtab != null)
            {
                currentSubtab.Visible = false;
                currentSubtab = null;
            }

            mainTab.SelectedIndex = 0;
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            if (Software.connection != null)
            {
                string path = Environment.CurrentDirectory + string.Format("\\DownloadData\\{0}", Software.connection.loggerInfo.serialNum);
                if (Directory.Exists(path))
                {
                    Process.Start("explorer.exe", @path);
                }
                else
                {
                    MessageEx.Mesaage_Error("File not found", "No current data for this logger");
                }
            }
            else
            {
                MessageEx.Mesaage_Error("Connection not established", "Please connect to at least one logger");
            }
        }

        private void softwareLocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string path = Environment.CurrentDirectory;
            if (Directory.Exists(path))
            {
                Process.Start("explorer.exe", @path);
            }
        }

        private void downloadDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string path = Environment.CurrentDirectory + "\\DownloadData";
            if (Directory.Exists(path))
            {
                Process.Start("explorer.exe", @path);
            }
            else
            {
                MessageEx.Mesaage_Error("File not found", "No current download data for this logger");
            }
        }

        private void realTimeDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string path = Environment.CurrentDirectory + "\\RealTimeData";
            if (Directory.Exists(path))
            {
                Process.Start("explorer.exe", @path);
            }
            else
            {
                MessageEx.Mesaage_Error("File not found", "No current realtime data for this logger");
            }
        }

        private void Menu_connection_radio_Click(object sender, EventArgs e)
        {
            Form connectionComForm = new Connection_com_form(2);
            connectionComForm.ShowDialog();
        }

        private void uSBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form connectionComForm = new Connection_com_form(0);
            connectionComForm.ShowDialog();
        }

        private void rS232CableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form connectionComForm = new Connection_com_form(1);
            connectionComForm.ShowDialog();
        }

        private void modemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form connectionComForm = new Connection_com_form(1);
            connectionComForm.ShowDialog();
        }

        private void radioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form connectionComForm = new Connection_com_form(2);
            connectionComForm.ShowDialog();
        }

        private void rS232CableToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form connectionComForm = new Connection_com_form(1);
            connectionComForm.ShowDialog();
        }

        private void modemToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form connectionComForm = new Connection_com_form(1);
            connectionComForm.ShowDialog();
        }

        private void enableCoreSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Software.enableCoreSettings == false)
            {
                DialogResult dialogResult = MessageBox.Show("Pushing core settings to logger may cause the connection fail, it is recommended to set under USB connection." +
                    "\n\rDo you stil want to enable core settings?", 
                    "Enable Core Settings", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Software.enableCoreSettings = true;
                    enableCoreSettingsToolStripMenuItem.Text = "Disable Core Settings";
                }
            }
            else
            {
                Software.enableCoreSettings = false;
                enableCoreSettingsToolStripMenuItem.Text = "Enable Core Settings";
            }
        }
    }
}
