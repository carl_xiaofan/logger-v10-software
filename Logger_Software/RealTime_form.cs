﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using LoggerSoftware.Core;
using System.Diagnostics;

namespace LoggerSoftware
{
    public partial class RealTime_form : Form
    {
        private bool rtmEnable = false;

        private int currentRow = -1;     // Current row being printing to

        public LineGraph_form[] linePlotForm = new LineGraph_form[8];

        public GaugeGraph_form[] gaugePlotForm = new GaugeGraph_form[8];

        public int[] plotFormType = new int[8];

        private StreamWriter file;

        private string filePath;

        Thread rtmThread;

        /* ************************************************************ 
         * Description : Initalize the realtime form
         * Input : void
         * Output : void
         * ************************************************************
         */
        public RealTime_form()
        {
            InitializeComponent();
            rtmDatagrid.RowHeadersWidth = 175;
            rtmDatagrid.AllowUserToResizeColumns = true;
            rtmDatagrid.AllowUserToResizeRows = true;
            this.FormClosing += new FormClosingEventHandler(Closing_Event);
            plotFormType = new int[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
            Start_RTM();
        }

        /* ************************************************************ 
         * Description : Start real time mode, initialize the data grid and start thread
         * Input : void
         * Output : void
         * ************************************************************
         */
        private void Start_RTM()
        {
            rtmButton.BackColor = Color.LightSalmon;
            rtmButton.Text = "Stop";
            rtmEnable = true;

            if (Software.connection.Get_AllSensors())
            {
                for (int i = 0; i < 8; i++)
                {
                    if (Software.connection.loggerInfo.sensorInfo[i].type == 0)
                    {
                        rtmDatagrid.Columns[i].Visible = false;
                    }
                    else
                    {
                        rtmDatagrid.Columns[i].HeaderText = String.Format("{0}:{1} ({2})", i + 1,
                            Constant.sensor_types[Software.connection.loggerInfo.sensorInfo[i].type],
                            Software.connection.loggerInfo.sensorInfo[i].unit.Trim());
                    }
                }

                Software.connection.SendPacket(Packet.DAT_RTM_Packet(1));

                rtmThread = new Thread(RTMthread);
                rtmThread.Start();

                Software.mainForm.Log_WriteLine("Real Time Recod Start");
            }
        }

        /* ************************************************************ 
         * Description : Stop real time mode
         * Input : void
         * Output : void
         * ************************************************************
         */
        private void Stop_RTM()
        {
            Software.connection.SendPacket(Packet.DAT_RTM_Packet(0));
            rtmButton.BackColor = Color.LightGreen;
            rtmButton.Text = "Start";
            rtmEnable = false;

            if (rtmThread != null)
            {
                rtmThread.Abort();

                Thread.Sleep(100);

                file.Close();

                DateTime now = DateTime.Now;
                File.Move(filePath, filePath.Substring(0, filePath.Length - 11) + String.Format("{0}.txt", now.ToString("HH-mm-ss")));

                Software.mainForm.Log_WriteLine("Real Time Recod Stop");
            }
        }

        /* ************************************************************ 
         * Description : RTM Button Click event, start/stop rtm
         * Input : event
         * Output : void
         * ************************************************************
         */
        private void RTM_Button_Click(object sender, EventArgs e)
        {
            if (rtmEnable)
            {
                Stop_RTM();
            }
            else
            {
                Start_RTM();
            }
        }

        /* ************************************************************ 
         * Description : RTM Thread, update the data grid ,and write into a file
         * Input : void
         * Output : void
         * ************************************************************
         */
        private void RTMthread()
        {
            List<int> recordingChannel = new List<int>();
            for (int i = 0; i < 8; i++)
            {
                if (Software.connection.loggerInfo.sensorInfo[i].type != 0)
                {
                    recordingChannel.Add(i);
                }
            }

            DateTime now = DateTime.Now;
            filePath = Environment.CurrentDirectory + string.Format("/RealTimeData/{0}/{1}~Writing.txt",
                Software.connection.loggerInfo.serialNum, now.ToString("dd-MM-yyyy_HH-mm-ss"));
            Directory.CreateDirectory(String.Format("RealTimeData/{0}", Software.connection.loggerInfo.serialNum.ToString()));
            using (file = new StreamWriter(filePath))
            {
                // Write Sensor Settings at the beginning of the file
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Channel {0}", i + 1).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Type: {0}", Constant.sensor_types[Software.connection.loggerInfo.sensorInfo[i].type]).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Unit: {0}", Software.connection.loggerInfo.sensorInfo[i].unit).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Settle Time(ms): {0}", Software.connection.loggerInfo.sensorInfo[i].settle).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Sweep Start(Hz): {0}", Software.connection.loggerInfo.sensorInfo[i].excite_start).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Sweep Stop(Hz): {0}", Software.connection.loggerInfo.sensorInfo[i].excite_stop).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Cycles: {0}", Software.connection.loggerInfo.sensorInfo[i].cycle).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Equation: {0}", Constant.equation_types[Software.connection.loggerInfo.sensorInfo[i].equation]).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Coef x^4: {0}", Software.connection.loggerInfo.sensorInfo[i].cof[4]).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Coef x^3: {0}", Software.connection.loggerInfo.sensorInfo[i].cof[3]).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Coef x^2: {0}", Software.connection.loggerInfo.sensorInfo[i].cof[2]).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Coef x^1: {0}", Software.connection.loggerInfo.sensorInfo[i].cof[1]).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);
                foreach (int i in recordingChannel)
                {
                    file.Write(String.Format("Coef x^0: {0}", Software.connection.loggerInfo.sensorInfo[i].cof[0]).PadRight(25, ' '));
                }
                file.Write(Environment.NewLine);

                file.Write(Environment.NewLine);
                file.WriteLine("Real Time Data | Date: {0}/{1}/{2}", now.Year, now.Month, now.Day);

                int previousColoum = 8;

                while (true)
                {
                    int[] data = Software.connection.Get_Packet(0);

                    if (Software.connection.Check_Valid(data, (int)Constant.Serial_flag.DAT))
                    {
                        int currentColoum = data[5];
                        if (currentColoum <= previousColoum)
                        {
                            currentRow++;
                            rtmDatagrid.Invoke(new Action(() => rtmDatagrid.Rows.Add()));
                            rtmDatagrid.Invoke(new Action(() => rtmDatagrid.FirstDisplayedScrollingRowIndex = currentRow));
                            // rtmDatagrid.Invoke(new Action(() => rtmDatagrid.CurrentCell = null));
                            rtmDatagrid.Rows[currentRow].HeaderCell.Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                            file.WriteLine();
                            file.Write(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss "));
                        }
                        float reading = Util.int_to_float(data[6], data[7], data[8], data[9]);

                        rtmDatagrid.Rows[currentRow].Cells[currentColoum].Value = String.Format("{0: 00000.000; -00000.000}", Math.Round(reading, 3));


                        file.Write(" {0: 00000.000; -00000.000} | ", Math.Round(reading, 3));

                        // Update plot if has
                        switch (plotFormType[currentColoum])
                        {
                            case 0:
                                break;
                            case 1:
                                linePlotForm[currentColoum].Add_Point(Math.Round(reading, 2));
                                break;
                            case 2:
                                gaugePlotForm[currentColoum].Update_Read(Math.Round(reading, 2));
                                break;
                        }

                        previousColoum = currentColoum;
                    }
                }
            }
        }

        /* ************************************************************
         * Description : Line Graph closing event, disable updating
         * Input : event
         * Output : void
         * ************************************************************
         */
        private void Closing_Event(object sender, FormClosingEventArgs e)
        {
            if (rtmEnable)
            {
                Stop_RTM();
            }
        }

        /* ************************************************************
         * Description : Plot Button Click, start ploting
         * Input : event
         * Output : void
         * ************************************************************
         */
        private void plot_button_Click(object sender, EventArgs e)
        {
            string title_text;
            if (rtmEnable)
            {
                if (sourcechannel_dropdown.SelectedIndex != -1 && plottype_dropdown.SelectedIndex != -1)
                {
                    switch (plottype_dropdown.SelectedIndex)
                    {
                        case 0:
                            title_text = rtmDatagrid.Columns[sourcechannel_dropdown.SelectedIndex].HeaderText;
                            plotFormType[sourcechannel_dropdown.SelectedIndex] = 1;
                            linePlotForm[sourcechannel_dropdown.SelectedIndex] = new LineGraph_form(String.Format("Channel {0}", title_text), sourcechannel_dropdown.SelectedIndex);
                            linePlotForm[sourcechannel_dropdown.SelectedIndex].Show();

                            if (currentRow > 0)
                            {
                                for (int i = 0; i < currentRow; i++)
                                {
                                    linePlotForm[sourcechannel_dropdown.SelectedIndex].Add_Point(Convert.ToDouble(rtmDatagrid.Rows[i].Cells[sourcechannel_dropdown.SelectedIndex].Value));
                                }
                            }

                            break;
                        case 1:
                            title_text = rtmDatagrid.Columns[sourcechannel_dropdown.SelectedIndex].HeaderText;
                            plotFormType[sourcechannel_dropdown.SelectedIndex] = 2;
                            gaugePlotForm[sourcechannel_dropdown.SelectedIndex] = new GaugeGraph_form(String.Format("Channel {0}", title_text), sourcechannel_dropdown.SelectedIndex);
                            gaugePlotForm[sourcechannel_dropdown.SelectedIndex].Show();

                            if (currentRow > 0)
                            {
                                gaugePlotForm[sourcechannel_dropdown.SelectedIndex].Update_Read(
                                    Math.Round(Convert.ToDouble(rtmDatagrid.Rows[currentRow - 1].Cells[sourcechannel_dropdown.SelectedIndex].Value), 1));
                            }
                            break;
                    }
                }
            }
        }

        /* ************************************************************
         * Description : Open File Browser for storing location
         * Input : event
         * Output : void
         * ************************************************************
         */
        private void browseButton_Click(object sender, EventArgs e)
        {
            string path = Environment.CurrentDirectory + string.Format("\\RealTimeData\\{0}", Software.connection.loggerInfo.serialNum);
            if (Directory.Exists(path))
            {
                Process.Start("explorer.exe", @path);
            }
        }
    }
}
